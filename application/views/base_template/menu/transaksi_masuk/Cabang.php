<li class="nav-item">
	<?php $isActive = ($authorization->activeMenu == MenuConstant::TRANSAKSI_MASUK_CABANG) ? "active" : ""; ?>
	<a href="<?php echo base_url(BaseUrl::DEFAULT_TRANSAKSI_MASUK_CABANG_URL) ?>" class="nav-link <?php echo $isActive ?>">
		<i class="far fa-circle nav-icon"></i>
		<p>Permintaan</p>
	</a>
</li>
