<li class="nav-item">
	<?php $isActive = ($authorization->activeMenu == MenuConstant::TRANSAKSI_MASUK_SUPPLIER) ? "active" : ""; ?>
	<a href="<?php echo base_url(BaseUrl::DEFAULT_TRANSAKSI_MASUK_SUPPLIER_URL) ?>" class="nav-link <?php echo $isActive ?>">
		<i class="far fa-circle nav-icon"></i>
		<p>Supplier</p>
	</a>
</li>
<li class="nav-item">
	<?php $isActive = ($authorization->activeMenu == MenuConstant::TRANSAKSI_MASUK_RETUR) ? "active" : ""; ?>
	<a href="<?php echo base_url(BaseUrl::DEFAULT_TRANSAKSI_MASUK_RETUR_URL) ?>" class="nav-link <?php echo $isActive ?>">
		<i class="far fa-circle nav-icon"></i>
		<p>Retur</p>
	</a>
</li>
