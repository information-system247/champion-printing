<li class="nav-item">
	<?php $isActive = ($authorization->activeMenu == MenuConstant::LAPORAN_TRANSAKSI_MASUK_RETUR) ? "active" : "" ?>
	<a href="<?php echo base_url(BaseUrl::DEFAULT_LAPORAN_TRANSAKSI_MASUK.'/retur') ?>" class="nav-link <?php echo $isActive ?>">
		<i class="far fa-circle nav-icon"></i>
		<p>Retur</p>
	</a>
</li>
