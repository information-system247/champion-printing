<li class="nav-item">
	<?php $isActive = ($authorization->activeMenu == MenuConstant::BERANDA) ? "active" : ""; ?>
	<a href="<?php echo base_url('index') ?>" class="nav-link <?php echo $isActive ?>">
		<i class="nav-icon fas fa-house-user"></i>
		<p>Beranda</p>
	</a>
</li>

<li class="nav-item">
	<?php $isActive = ($authorization->activeMenu == MenuConstant::PERUSAHAAN || $authorization->activeMenu == MenuConstant::USER
		|| $authorization->activeMenu == MenuConstant::SUPPLIER || $authorization->activeMenu == MenuConstant::KARYAWAN
		|| $authorization->activeMenu == MenuConstant::KONSUMEN_CUSTOMER || $authorization->activeMenu == MenuConstant::KONSUMEN_PERUSAHAAN 
		|| $authorization->activeMenu == MenuConstant::KATEGORI_BARANG || $authorization->activeMenu == MenuConstant::TIPE_BARANG
		|| $authorization->activeMenu == MenuConstant::BARANG) ? "active" : ""; ?>
	<a href="#" class="nav-link <?php echo $isActive ?>">
		<i class="nav-icon fas fa-folder"></i>
		<p>Master Data<i class="fas fa-angle-left right"></i></p>
	</a>
	<ul class="nav nav-treeview">
		<li class="nav-item">
			<?php $isActive = ($authorization->activeMenu == MenuConstant::PERUSAHAAN) ? "active" : ""; ?>
			<a href="<?php echo base_url(BaseUrl::DEFAULT_PERUSAHAAN_URL) ?>" class="nav-link <?php echo $isActive ?>">
				<i class="far fa-circle nav-icon"></i>
				<p>Perusahaan / Cabang</p>
			</a>
		</li>
		<li class="nav-item">
			<?php $isActive = ($authorization->activeMenu == MenuConstant::SUPPLIER) ? "active" : ""; ?>
			<a href="<?php echo base_url(BaseUrl::DEFAULT_SUPPLIER_URL) ?>" class="nav-link <?php echo $isActive ?>">
				<i class="far fa-circle nav-icon"></i>
				<p>Supplier</p>
			</a>
		</li>
		<li class="nav-item">
			<?php $isActive = ($authorization->activeMenu == MenuConstant::KARYAWAN || $authorization->activeMenu == MenuConstant::USER) ? "active" : "" ?>
			<a href="#" class="nav-link <?php echo $isActive ?>">
				<i class="nav-icon fas fa-folder"></i>
				<p>Management User<i class="fas fa-angle-left right"></i></p>
			</a>
			<ul class="nav nav-treeview">
				<li class="nav-item">
					<?php $isActive = ($authorization->activeMenu == MenuConstant::KARYAWAN) ? "active" : ""; ?>
					<a href="<?php echo base_url(BaseUrl::DEFAULT_KARYAWAN_URL) ?>" class="nav-link <?php echo $isActive ?>">
						<i class="far fa-circle nav-icon"></i>
						<p>Karyawan</p>
					</a>
				</li>
				<li class="nav-item">
					<?php $isActive = ($authorization->activeMenu == MenuConstant::USER) ? "active" : ""; ?>
					<a href="<?php echo base_url(BaseUrl::DEFAULT_USER_URL) ?>" class="nav-link <?php echo $isActive ?>">
						<i class="far fa-circle nav-icon"></i>
						<p>User</p>
					</a>
				</li>
			</ul>
		</li>
		<li class="nav-item">
			<?php $isActive = ($authorization->activeMenu == MenuConstant::KONSUMEN_CUSTOMER 
				|| $authorization->activeMenu == MenuConstant::KONSUMEN_PERUSAHAAN) ? "active" : "" ?>
			<a href="#" class="nav-link <?php echo $isActive ?>">
				<i class="nav-icon fas fa-folder"></i>
				<p>Konsumen<i class="right fas fa-angle-left"></i></p>
			</a>
			<ul class="nav nav-treeview">
				<li class="nav-item">
					<?php $isActive = ($authorization->activeMenu == MenuConstant::KONSUMEN_CUSTOMER) ? "active" : ""; ?>
					<a href="<?php echo base_url(BaseUrl::DEFAULT_KONSUMEN_CUSTOMER_URL) ?>" class="nav-link <?php echo $isActive ?>">
						<i class="far fa-circle nav-icon"></i>
						<p>Customer</p>
					</a>
				</li>
				<li class="nav-item">
					<?php $isActive = ($authorization->activeMenu == MenuConstant::KONSUMEN_PERUSAHAAN) ? "active" : ""; ?>
					<a href="<?php echo base_url(BaseUrl::DEFAULT_KONSUMEN_PERUSAHAAN_URL) ?>" class="nav-link <?php echo $isActive ?>">
						<i class="far fa-circle nav-icon"></i>
						<p>Perusahaan</p>
					</a>
				</li>
			</ul>
		</li>
		<li class="nav-item">
			<?php $isActive = ($authorization->activeMenu == MenuConstant::KATEGORI_BARANG || $authorization->activeMenu == MenuConstant::TIPE_BARANG
				|| $authorization->activeMenu == MenuConstant::BARANG) ? "active" : "" ?>
			<a href="#" class="nav-link <?php echo $isActive ?>">
				<i class="nav-icon fas fa-folder"></i>
				<p>item<i class="right fas fa-angle-left"></i></p>
			</a>
			<ul class="nav nav-treeview">
				<li class="nav-item">
					<?php $isActive = ($authorization->activeMenu == MenuConstant::KATEGORI_BARANG) ? "active" : ""; ?>
					<a href="<?php echo base_url(BaseUrl::DEFAULT_KATEGORI_BARANG_URL) ?>" class="nav-link <?php echo $isActive ?>">
						<i class="far fa-circle nav-icon"></i>
						<p>Kategori Barang</p>
					</a>
				</li>
				<li class="nav-item">
					<?php $isActive = ($authorization->activeMenu == MenuConstant::TIPE_BARANG) ? "active" : ""; ?>
					<a href="<?php echo base_url(BaseUrl::DEFAULT_TIPE_BARANG_URL) ?>" class="nav-link <?php echo $isActive ?>">
						<i class="far fa-circle nav-icon"></i>
						<p>Tipe Barang</p>
					</a>
				</li>
				<li class="nav-item">
					<?php $isActive = ($authorization->activeMenu == MenuConstant::BARANG) ? "active" : ""; ?>
					<a href="<?php echo base_url(BaseUrl::DEFAULT_BARANG_URL) ?>" class="nav-link <?php echo $isActive ?>">
						<i class="far fa-circle nav-icon"></i>
						<p>barang</p>
					</a>
				</li>
			</ul>
		</li>
	</ul>
</li>

<li class="nav-item">
	<?php $isActive = ($authorization->activeMenu == MenuConstant::TRANSAKSI_MASUK_SUPPLIER 
		|| $authorization->activeMenu == MenuConstant::TRANSAKSI_MASUK_RETUR ) ? "active" : "" ?>
	<a href="#" class="nav-link <?php echo $isActive ?>">
		<i class="nav-icon fas fa-folder"></i>
		<p>Transaksi Masuk<i class="fas fa-angle-left right"></i></p>
	</a>
	<ul class="nav nav-treeview">
		<?php if ($authorization->user->karyawan->perusahaan->level == LevelPerusahaanConstant::PUSAT) {
			$this->load->view('base_template/menu/transaksi_masuk/pusat', $authorization);
		} else {
			$this->load->view('base_template/menu/transaksi_masuk/cabang', $authorization);
		} ?>
	</ul>
</li>

<li class="nav-item">
	<?php $isActive = ($authorization->activeMenu == MenuConstant::TRANSAKSI_KELUAR_PUSAT_CABANG
		|| $authorization->activeMenu == MenuConstant::TRANSAKSI_KELUAR_KONSUMEN ) ? "active" : "" ?>
	<a href="#" class="nav-link <?php echo $isActive ?>">
		<i class="nav-icon fas fa-folder"></i>
		<p>Transaksi Keluar<i class="fas fa-angle-left right"></i></p>
	</a>
	<ul class="nav nav-treeview">
		<?php if ($authorization->user->karyawan->perusahaan->level == LevelPerusahaanConstant::PUSAT) {
			$this->load->view('base_template/menu/transaksi_keluar/pusat', $authorization);
		} else {
			$this->load->view('base_template/menu/transaksi_keluar/cabang', $authorization);
		} ?>
	</ul>
</li>

<li class="nav-item">
	<?php $isActive = ($authorization->activeMenu == MenuConstant::STOK) ? "active" : "" ?>
	<a href="<?php echo base_url(BaseUrl::DEFAULT_STOK_URL) ?>" class="nav-link">
		<i class="nav-icon fas fa-folder"></i>
		<p>Stok</p>
	</a>
</li>

<?php $this->load->view('base_template/menu/laporan', $authorization); ?>
