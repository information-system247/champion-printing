<li class="nav-item">
	<?php $isActive = ($authorization->activeMenu == MenuConstant::LAPORAN_KARYAWAN 
		|| $authorization->activeMenu == MenuConstant::LAPORAN_SUPPLIER || $authorization->activeMenu == MenuConstant::LAPORAN_KONSUMEN
		|| $authorization->activeMenu == MenuConstant::LAPORAN_STOK || $authorization->activeMenu == MenuConstant::LAPORAN_TRANSAKSI_MASUK_SUPPLIER 
		|| $authorization->activeMenu == MenuConstant::LAPORAN_TRANSAKSI_MASUK_RETUR || $authorization->activeMenu == MenuConstant::LAPORAN_TRANSAKSI_MASUK_PUSAT 
		|| $authorization->activeMenu == MenuConstant::LAPORAN_TRANSAKSI_KELUAR_CABANG || $authorization->activeMenu == MenuConstant::LAPORAN_TRANSAKSI_KELUAR_KONSUMEN) ? "active" : "" ?>
	<a href="#" class="nav-link <?php echo $isActive ?>">
		<i class="nav-icon fas fa-book"></i>
		<p>
			Laporan
			<i class="fas fa-angle-left right"></i>
		</p>
	</a>
	<ul class="nav nav-treeview">
		<li class="nav-item">
			<?php $isActive = ($authorization->activeMenu == MenuConstant::LAPORAN_KARYAWAN) ? "active" : "" ?>
			<a href="<?php echo base_url(BaseUrl::DEFAULT_LAPORAN_KARYAWAN) ?>" class="nav-link <?php  echo $isActive?>">
				<i class="far fa-circle nav-icon"></i>
				<p>karyawan</p>
			</a>
		</li>
		<li class="nav-item">
			<?php $isActive = ($authorization->activeMenu == MenuConstant::LAPORAN_SUPPLIER) ? "active" : "" ?>
			<a href="<?php echo base_url(BaseUrl::DEFAULT_LAPORAN_SUPPLIER) ?>" class="nav-link <?php echo $isActive ?>">
				<i class="far fa-circle nav-icon"></i>
				<p>Supplier</p>
			</a>
		</li>
		<li class="nav-item">
			<?php $isActive = ($authorization->activeMenu == MenuConstant::LAPORAN_KONSUMEN) ? "active" : "" ?>
			<a href="<?php echo base_url(BaseUrl::DEFAULT_LAPORAN_KONSUMEN) ?>" class="nav-link <?php echo $isActive ?>">
				<i class="far fa-circle nav-icon"></i>
				<p>Konsumen</p>
			</a>
		</li>
		<li class="nav-item">
			<?php $isActive = ($authorization->activeMenu == MenuConstant::LAPORAN_TRANSAKSI_MASUK_SUPPLIER 
				|| $authorization->activeMenu == MenuConstant::LAPORAN_TRANSAKSI_MASUK_RETUR 
				|| $authorization->activeMenu == MenuConstant::LAPORAN_TRANSAKSI_MASUK_PUSAT) ? "active" : "" ?>
			<a href="#" class="nav-link <?php echo $isActive ?>">
				<i class="nav-icon fas fa-book"></i>
				<p>Transaksi Masuk<i class="fas fa-angle-left right"></i></p>
			</a>
			<ul class="nav nav-treeview">
				<li class="nav-item">
					<?php $isActive = ($authorization->activeMenu == MenuConstant::LAPORAN_TRANSAKSI_MASUK_SUPPLIER) ? "active" : "" ?>
					<a href="<?php echo base_url(BaseUrl::DEFAULT_LAPORAN_TRANSAKSI_MASUK.'/supplier') ?>" class="nav-link <?php echo $isActive ?>">
						<i class="far fa-circle nav-icon"></i>
						<p>Supplier</p>
					</a>
				</li>
				<?php if ($authorization->user->karyawan->perusahaan->level == LevelPerusahaanConstant::PUSAT) {
					$this->load->view('base_template/menu/laporan_transaksi_masuk/pusat', $authorization);
				} else {
					$this->load->view('base_template/menu/laporan_transaksi_masuk/cabang', $authorization);
				} ?>
			</ul>
		</li>
		<li class="nav-item">
			<?php $isActive = ($authorization->activeMenu == MenuConstant::LAPORAN_TRANSAKSI_KELUAR_CABANG || $authorization->activeMenu == MenuConstant::LAPORAN_TRANSAKSI_KELUAR_KONSUMEN) ? "active" : "" ?>
			<a href="#" class="nav-link <?php echo $isActive ?>">
				<i class="nav-icon fas fa-book"></i>
				<p>Transaksi Keluar<i class="fas fa-angle-left right"></i></p>
			</a>
			<ul class="nav nav-treeview">
				<li class="nav-item">
					<?php $isActive = ($authorization->activeMenu == MenuConstant::LAPORAN_TRANSAKSI_KELUAR_KONSUMEN) ? "active" : "" ?>
					<a href="<?php echo base_url(BaseUrl::DEFAULT_LAPORAN_TRANSAKSI_KELUAR.'/konsumen') ?>" class="nav-link <?php echo $isActive ?>">
						<i class="far fa-circle nav-icon"></i>
						<p>Konsumen</p>
					</a>
				</li>
				<?php if ($authorization->user->karyawan->perusahaan->level == LevelPerusahaanConstant::PUSAT) {
					$this->load->view('base_template/menu/laporan_transaksi_keluar/pusat', $authorization);
				} ?>
			</ul>
		</li>
		<li class="nav-item">
			<?php $isActive = ($authorization->activeMenu == MenuConstant::LAPORAN_STOK) ? "active" : "" ?>
			<a href="<?php echo base_url(BaseUrl::DEFAULT_LAPORAN_STOK) ?>" class="nav-link <?php echo $isActive ?>">
				<i class="far fa-circle nav-icon"></i>
				<p>Stok Barang</p>
			</a>
		</li>
	</ul>
</li>
