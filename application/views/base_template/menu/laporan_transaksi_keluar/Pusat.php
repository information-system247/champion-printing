<li class="nav-item">
	<?php $isActive = ($authorization->activeMenu == MenuConstant::LAPORAN_TRANSAKSI_KELUAR_CABANG) ? "active" : "" ?>
	<a href="<?php echo base_url(BaseUrl::DEFAULT_LAPORAN_TRANSAKSI_KELUAR.'/cabang') ?>" class="nav-link <?php echo $isActive ?>">
		<i class="far fa-circle nav-icon"></i>
		<p>Cabang</p>
	</a>
</li>
