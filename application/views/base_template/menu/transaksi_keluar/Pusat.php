<li class="nav-item">
	<?php $isActive = ($authorization->activeMenu == MenuConstant::TRANSAKSI_KELUAR_PUSAT_CABANG) ? "active" : ""; ?>
	<a href="<?php echo base_url(BaseUrl::DEFAULT_TRANSAKSI_KELUAR_CABANG_URL) ?>" class="nav-link <?php echo $isActive ?>">
		<i class="far fa-circle nav-icon"></i>
		<p>Cabang</p>
	</a>
</li>
<li class="nav-item">
	<?php $isActive = ($authorization->activeMenu == MenuConstant::TRANSAKSI_KELUAR_KONSUMEN) ? "active" : ""; ?>
	<a href="<?php echo base_url(BaseUrl::DEFAULT_TRANSAKSI_KELUAR_KONSUMEN_URL) ?>" class="nav-link <?php echo $isActive ?>">
		<i class="far fa-circle nav-icon"></i>
		<p>Konsumen</p>
	</a>
</li>
