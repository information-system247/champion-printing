<table id="dataTable" class="table table-bordered table-hover">
	<thead>
		<tr>
			<th>No</th>
			<th>Nama Supplier</th>
			<th>Alamat</th>
			<th>No Hp</th>
			<th width="13%">Aksi</th>
		</tr>
	</thead>
	<tbody><?php $no = 1;
		if (count($supplierList) == 0) { ?>
			<tr>
				<td colspan="5" align="center">Data Masih Kosong ...</td>
			</tr><?php
		} else {
			foreach ($supplierList as $supplier) {
				$data = array(
					'data' => $supplier
				); ?>
				<tr>
					<td><?php echo $no++; ?></td>
					<td><?php echo $supplier->nama_supplier ?></td>
					<td><?php echo $supplier->alamat ?></td>
					<td><?php echo $supplier->no_hp ?></td>
					<td>
						<div class="row">
							<div class="col-6">
							<button type="button" class="btn btn-block bg-gradient-warning btn btn-sm" data-toggle="modal" data-target="#modalUpdate<?php echo $supplier->id ?>">Ubah</button>
							</div>
							<div class="col-6">
								<button type="button" class="btn btn-block bg-gradient-danger btn btn-sm" data-toggle="modal" data-target="#modalDelete<?php echo $supplier->id ?>">Hapus</button>
							</div>
						</div>
					</td>
				</tr>
				<?php $this->load->view('supplier/delete', $data);
				$this->load->view('supplier/edit', $data);
			}
		} ?>
	</tbody>
</table>
