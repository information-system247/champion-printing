<div class="modal fade" id="modalDetail">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header bg-info">
        <h4 class="modal-title">Tambah Barang</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
				<div class="card-body">
					<table id="dataTable1" class="table table-bordered table-hover">
						<thead>
							<tr>
								<th style="width: 30px; text-align: center;">No</th>
								<th style="text-align: center;">Nama Barang</th>
								<th style="text-align: center;">QTY</th>
							</tr>
						</thead>
						<tbody id="showDataDetail">
						</tbody>
					</table>
				</div>
			</div>
			<!-- /.card-body -->
			<div class="modal-footer justify-content-between">
				<form id="formSubmit" method="post" action="<?php echo base_url(BaseUrl::DEFAULT_TRANSAKSI_URL.'/update') ?>">
					<input type="hidden" id="id" name="id">
					<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
					<button type="submit" class="btn btn-primary">Proses</button>
				</form>
			</div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<script type="text/javascript">
	$(document).ready(function(){
		getDetailTransaksi();
		function getDetailTransaksi(){
			var transaksiId = $('#id').val();
			$.ajax({
				type  : 'POST',
				url   : '<?php echo base_url('transaksi/histori/getdetailhistoritransaksi')?>',
				async : true,
				dataType : 'json',
				data : {
					transaksiId: transaksiId,
				},
				success : function(data){
					var html = '';
					var i;
					if (data.length > 0) {
						var index = 1;
						for(i=0; i<data.length; i++){
							html += '<tr>'+
								'<td>'+ (index++) +'</td>'+
								'<td>'+ data[i].barang.nama_barang +'</td>'+
								'<td>'+ data[i].qty +'</td>'+
							'</tr>';
						}
					}
					
					$('#showDataDetail').html(html);
				}
			});
		}

	});
</script>
