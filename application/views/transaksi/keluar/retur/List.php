<?php 
  $this->load->view('base_template/head');
  $this->load->view('base_template/sidebar', $authorization);
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Transaksi Keluar Retur</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Master</a></li>
              <li class="breadcrumb-item active">Transaksi Keluar</li>
							<li class="breadcrumb-item active">Retur</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-1">
            <div class="card">
							<?php if ($authorization->user->karyawan->posisi->nama == 'Kasir') {
								?> <a href="<?php echo base_url(BaseUrl::DEFAULT_TRANSAKSI_KELUAR_RETUR_URL.'/add') ?>" type="button" class="btn btn-block btn-primary">Tambah</a> <?php
							} ?>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Transaksi Keluar</h3>
              </div>
							<input type="hidden" id="idPerusahaan" name="idPerusahaan" value="<?php echo $authorization->user->karyawan->perusahaan->id ?>">
							<input type="hidden" class="form-control" id="levelPerusahaan" name="levelPerusahaan" value="<?php echo $authorization->user->karyawan->perusahaan->level ?>" />
							<input type="hidden" class="form-control" id="userId" name="userId" value="<?php echo $authorization->user->id ?>" />
              <!-- /.card-header -->
              <div class="card-body">
								<table id="dataTable" class="table table-bordered table-hover">
									<thead>
										<tr>
											<th style="text-align: center;">No</th>
											<th style="text-align: center;">Tanggal Transaksi</th>
											<th style="text-align: center;">Nama Perusahaan</th>
											<th style="text-align: center;">Keterangan</th>
											<th style="text-align: center;">Status</th>
											<th style="width: 50px; text-align: center;">Aksi</th>
										</tr>
									</thead>
									<tbody id="showData"></tbody>
								</table>
							</div>
            </div>
          </div>
        </div>
      </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <button type="submit" class="btn btn-primary swalDefaultSuccess" style="visibility: hidden">success</button>
  <button type="submit" class="btn btn-primary swalDefaultError" style="visibility: hidden">failed</button>
  <button type="submit" class="btn btn-primary swalDefaultWarning" style="visibility: hidden">warning</button>

<?php
  $this->load->view('base_template/footer');
  $this->load->view('base_template/script');

  if ($this->session->flashdata('success')) {
    echo $this->session->flashdata('success');
    unset($_SESSION['success']);
  }
  
  if ($this->session->flashdata('failed')) {
    echo $this->session->flashdata('failed');
    unset($_SESSION['failed']);
  }
  
  if ($this->session->flashdata('warning')) {
    echo $this->session->flashdata('warning');
    unset($_SESSION['warning']);
  }
?>
<script type="text/javascript">
	$(document).ready(function(){
		showTempList();
		function showTempList(){
			var perusahaanId = $('#idPerusahaan').val();
			var level = $('#levelPerusahaan').val();
			var userId = $('#userId').val();
			var jenisTransaksi = "<?php echo JenisTransaksiConstant::RETUR ?>"

			$.ajax({
				type  : 'POST',
				url   : '<?php echo base_url('transaksi/histori/gethistoritransaksi')?>',
				async : true,
				dataType : 'json',
				data : {
					perusahaanId: perusahaanId,
					status: null,
					level: level,
					userId: userId,
					jenisTransaksi: jenisTransaksi
				},
				success : function(data){
					var html = '';
					var i;
					var transaksiList = data.historiTransaksiList;
					if (transaksiList.length > 0) {
						var index = 1;
						for(i=0; i<transaksiList.length; i++){
							html += '<tr>'+
								'<td>'+ (index++) +'</td>'+
								'<td>'+ transaksiList[i].tgl_transaksi +'</td>'+
								'<td>'+ transaksiList[i].id_perusahaan +'</td>'+
								'<td>'+ transaksiList[i].keterangan +'</td>'+
								'<td>'+ transaksiList[i].status +'</td>'+
								'<td>'+
									'<div class="row">'+
										'<a href="<?php echo base_url(BaseUrl::DEFAULT_TRANSAKSI_KELUAR_RETUR_URL.'/detailtransaksi').'/' ?>'+ transaksiList[i].id +'" type="button" class="btn btn-block bg-gradient-warning btn btn-sm">Detail</a>'+
									'</div>'+
								'</td>'+
							'</tr>';
						}
					}
					
					$('#showData').html(html);
				}
			});
		}

		$('#dataTable').on('click','.proses',function(){
			var id = $(this).data('id');
			$.ajax({
				type : "POST",
				url  : "<?php echo base_url(BaseUrl::DEFAULT_TRANSAKSI_URL.'/update') ?>",
				data : {
					id: id
				},
				dataType : 'json',
				success: function(data){
					showTempList();
				}
			});
		});
		
	});
</script>
