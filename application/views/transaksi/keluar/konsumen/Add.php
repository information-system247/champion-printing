<?php 
  $this->load->view('base_template/head');
  $this->load->view('base_template/sidebar', $authorization);
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1>Transaksi Keluar</h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="#">Beranda</a></li>
						<li class="breadcrumb-item active">Transaksi Keluar</li>
						<li class="breadcrumb-item active">Konsumen</li>
					</ol>
				</div>
			</div>
		</div><!-- /.container-fluid -->
	</section>

	<!-- Main content -->
	<section class="content">

		<!-- Default box -->
		<div class="card">
			<div class="card-header">
				<h3 class="card-title">Tambah Transaksi Keluar</h3>
			</div>
			<div class="card-body">
				<form id="formDetail" method="post" action="<?php echo base_url(BaseUrl::DEFAULT_TRANSAKSI_KELUAR_KONSUMEN_URL."/storetemp") ?>">
					<div class="row">
						<div class="col-md-6">
							<input type="hidden" name="userId" id="userId" value="<?php echo $authorization->user->id ?>" />
							<input type="hidden" name="kategoriTransaksi" id="kategoriTransaksi" value="<?php echo KategoriTransaksiConstant::KELUAR ?>" />
							<div class="form-group">
								<label for="tipeTransaksi">Tipe Transaksi</label>
								<input type="text" class="form-control" name="tipeTransaksi" id="tipeTransaksi" value="<?php echo tipeTransaksiKeluarConstant::TRANS_KELUAR_PUSAT_KE_KONSUMEN ?>" disabled>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label for="idPerusahaan">Nama Konsumen</label>
								<select class="form-control" name="idKonsumen" id="idKonsumen" required>
									<?php foreach ($konsumenList as $konsumen) { ?>
										<option value="<?php echo $konsumen->id ?>"><?php echo $konsumen->nama_konsumen ?></option><?php
									} ?>
								</select>
							</div>
						</div>
					</div>
					<!-- /.rows -->
					<div class="row">
						<div class="col-md-4">
							<div class="form-group">
								<label for="namaBarang">Nama Barang</label>
								<select class="form-control" name="idBarang" id="idBarang" required>
									<?php if (!empty($barangList)) { 
										foreach ($barangList as $barang) { ?>
											<option value="<?php echo $barang->id ?>"><?php echo $barang->nama_barang ?></option><?php
										}
									} ?>
								</select>
							</div>
							<div class="form-group">
								<label for="harga">Harga</label>
								<input type="text" class="form-control" name="harga" id="harga" disabled>
							</div>
							<div class="form-group">
								<label for="qty">QTY</label>
								<input type="text" class="form-control" name="qty" id="qty" placeholder="Masukkan jumlah qty" onkeyup="getSubTotal();" required>
							</div>
							<div class="form-group">
								<label for="subTotal">Sub Total</label>
								<input type="text" class="form-control" name="subTotal" id="subTotal" placeholder="masukkan harga Beli" disabled>
							</div>
							<div class="form-group text-right">
								<button class="btn btn-primary" type="button" id="addTemp">Tambah</button>
							</div>
						</div>
						<div class="col-md-8">
							<?php $this->load->view('transaksi/keluar/konsumen/tabledetail', $tempList); ?>
						</div>
					</div>
					<!-- /.rows -->
					<div class="form-group">
						<label for="keterangan">Keterangan</label>
						<input type="text" class="form-control" name="keterangan" id="keterangan">
					</div>
					<div class="form-group">
						<label for="total">Total</label>
						<input type="text" class="form-control" name="total" id="total" value="<?php echo $total ?>" disabled>
					</div>
					<div class="form-group">
						<label for="total">Bayar</label>
						<input type="text" class="form-control" name="bayar" id="bayar" onkeyup="getKembalian();" required>
					</div>
					<div class="form-group">
						<label for="total">Kembali</label>
						<input type="text" class="form-control" name="kembali" id="kembali" disabled>
					</div>
				</form>
			</div>
			<!-- /.card-body -->
			<form id="formSubmit" method="post" action="<?php echo base_url(BaseUrl::DEFAULT_TRANSAKSI_KELUAR_KONSUMEN_URL."/store") ?>">
				<div class="modal-footer justify-content-between">
					<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
					<button type="submit" class="btn btn-primary" id="simpan">Simpan</button>
				</div>
			</form>
		</div>
		<!-- /.card -->

	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->
<?php
  $this->load->view('base_template/footer');
  $this->load->view('base_template/script');
?>
<script type="text/javascript">
	$(document).ready(function(){
		showTempList();
		getHargaBarang();
		function showTempList(){
			var userId = $('#userId').val();
			var kategoriTransaksi = $('#kategoriTransaksi').val();
			var tipeTransaksi = $('#tipeTransaksi').val();
			$('#total').val('');

			$.ajax({
				type  : 'POST',
				url   : '<?php echo base_url('transaksi/temp/gettempbyuserid')?>',
				async : true,
				dataType : 'json',
				data : {
					userId: userId,
					kategoriTransaksi: kategoriTransaksi,
					tipeTransaksi: tipeTransaksi
				},
				success : function(data){
					var html = '';
					var i;
					var tempList = data.tempList;
					if (tempList.length > 0) {
						var index = 1;
						for(i=0; i<tempList.length; i++){
							html += '<tr>'+
								'<td>'+ (index++) +'</td>'+
								'<td>'+ tempList[i].barang.nama_barang +'</td>'+
								'<td>'+ tempList[i].qty +'</td>'+
								'<td>'+ tempList[i].sub_total +'</td>'+
								'<td>'+
									'<div class="row">'+
										'<button type="button" class="btn btn-block bg-gradient-danger btn btn-sm deleteTemp" id="'+ tempList[i].id +'">Hapus</button>'+
									'</div>'+
								'</td>'+
							'</tr>';
						}
					}
					
					$('#total').val(data.total);
					$('#showData').html(html);
				}
			});
		}

		function getHargaBarang() {
			var idBarang = $('#idBarang').val();
			$.ajax({
				type  : 'POST',
				url   : '<?php echo base_url('master/barang/getbarangbyid')?>',
				async : false,
				dataType : 'json',
				data : {
					id: idBarang
				}
			}).then(data => {
				$('#harga').val(data.harga_jual);
				$('#qty').val("");
					$('#subTotal').val("");
			}).fail((jqXHR,textStatus,errorThrown) => console.log(errorThrown, 'error'));
		}

		$('#idBarang').change(function(){
			var idBarang = $(this).val();
			$.ajax({
				type  : 'POST',
				url   : '<?php echo base_url('master/barang/getbarangbyid')?>',
				async : false,
				dataType : 'json',
				data : {
					id: idBarang
				}
			}).then(data => {
				$('#harga').val(data.harga_jual);
			}).fail((jqXHR,textStatus,errorThrown) => console.log(errorThrown, 'error'));
		});

		$('#addTemp').on('click', function() {
			var idBarang = $('#idBarang').val();
			var qty = $('#qty').val();
			var subTotal = $('#subTotal').val();
			var keterangan = $('#keterangan').val();
			var tipeTransaksi = $('#tipeTransaksi').val();
			var total = $('#total').val();
			var kategoriTransaksi = $('#kategoriTransaksi').val();
			var konsumenId = $('#idKonsumen').val();
			$.ajax({
				type : "POST",
				url  : "<?php echo base_url('transaksi/temp/storetemp') ?>",
				data : {
					idBarang: idBarang,
					qty: qty,
					subTotal: subTotal,
					keterangan: keterangan,
					idSupplier: null,
					total: total,
					tipeTransaksi: tipeTransaksi,
					kategoriTransaksi: kategoriTransaksi,
					idPerusahaan: null,
					idKonsumen: konsumenId
				},
				dataType : 'json',
				success: function(data) {
					$('#qty').val("");
					$('#subTotal').val("");
					$('#keterangan').val("");
					showTempList();
				}
			});
		});

		$("#showData").on('click','.deleteTemp',function(){
			var id = $(this).attr('id');
			$.ajax({
				url: '<?php echo base_url('transaksi/temp/delete'); ?>',
				type: 'POST',
				data: {
					id: id
				},
				success: function(response){
					showTempList();
				}
			});
		});

	});

	function getSubTotal() {
		var qty = document.getElementById('qty').value;
		var harga = document.getElementById('harga').value;
		var subTotal = parseInt(qty) * parseInt(harga);
		if (!isNaN(subTotal)) {
			document.getElementById('subTotal').value = subTotal;
		} else {
			document.getElementById('subTotal').value = "";
		}
	}

	function getKembalian() {
		var total = document.getElementById('total').value;
		var bayar = document.getElementById('bayar').value;
		var kembali = bayar - total;
		if (kembali <= 0) {
			document.querySelector('#simpan').disabled = true;
		} else {
			document.querySelector('#simpan').disabled = false;
		}
		document.getElementById('kembali').value = kembali;
	}
</script>
