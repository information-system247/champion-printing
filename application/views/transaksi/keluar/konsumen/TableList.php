<table id="dataTable" class="table table-bordered table-hover">
	<thead>
		<tr>
			<th style="text-align: center;">No</th>
			<th style="text-align: center;">Tanggal Transaksi</th>
			<th style="text-align: center;">Nama Konsumen</th>
			<th style="text-align: center;">Keterangan</th>
			<th style="text-align: center;">Status</th>
			<th style="width: 60px; text-align: center;">Aksi</th>
		</tr>
	</thead>
	<tbody> <?php $no = 1;
		if (count($transactionHistoryList) == 0) { ?>
			<tr>
				<td colspan="10" style="text-align: center;">Data Masih Kosong ...</td>
			</tr><?php
		} else {
			foreach ($transactionHistoryList as $transactionHistory) { 
				$data = array(
					'data' => $transactionHistory
				); ?>
				<tr>
					<td><?php echo $no++; ?></td>
					<td><?php echo $transactionHistory->tgl_transaksi ?></td>
					<td><?php echo $transactionHistory->konsumen->nama_konsumen ?></td>
					<td><?php echo $transactionHistory->keterangan ?></td>
					<td><?php echo $transactionHistory->status ?></td>
					<td>
						<a href="<?php echo base_url(BaseUrl::DEFAULT_TRANSAKSI_KELUAR_KONSUMEN_URL."/detailtransaksi")."/".$transactionHistory->id ?>" type="button" class="btn btn-block bg-gradient-warning btn btn-sm">Detail</a>
					</td>
				</tr><?php 
			}
		} ?>
	</tbody>
</table>
