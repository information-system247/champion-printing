<?php 
  $this->load->view('base_template/head');
  $this->load->view('base_template/sidebar', $authorization);
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1>Transaksi Masuk</h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="#">Beranda</a></li>
						<li class="breadcrumb-item active">Transaksi Masuk</li>
						<li class="breadcrumb-item active">Supplier</li>
					</ol>
				</div>
			</div>
		</div><!-- /.container-fluid -->
	</section>

	<!-- Main content -->
	<section class="content">

		<!-- Default box -->
		<div class="card">
			<div class="card-header">
				<h3 class="card-title">Tambah Transaksi Masuk</h3>
			</div>
			<div class="card-body">
				<form id="formDetail" method="post" action="<?php echo base_url(BaseUrl::DEFAULT_TRANSAKSI_MASUK_SUPPLIER_URL."/storetemp") ?>">
					<div class="row">
						<div class="col-md-6">
							<input type="hidden" name="userId" id="userId" value="<?php echo $authorization->user->id ?>" />
							<input type="hidden" name="kategoriTransaksi" id="kategoriTransaksi" value="<?php echo KategoriTransaksiConstant::MASUK ?>" />
							<div class="form-group">
								<label for="tipeTransaksi">Tipe Transaksi</label>
								<?php if (LevelPerusahaanConstant::PUSAT == $authorization->user->karyawan->perusahaan->level) { ?>
									<select class="form-control" name="tipeTransaksi" id="tipeTransaksi" required>
										<option value="<?php echo TipeTransaksiMasukConstant::TRANS_MASUK_PUSAT_DARI_SUPPLIER ?>" selected>Supplier</option>
									</select>
									<?php
								} else {
									?><input type="text" class="form-control" name="tipeTransaksi" id="tipeTransaksi" value="<?php echo TipeTransaksiMasukConstant::TRANS_MASUK_CABANG_DARI_PUSAT ?>" disabled><?php
								} ?>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label for="idSupplier">Nama Supplier</label>
								<select class="form-control" name="idSupplier" id="idSupplier" required>
									<?php foreach ($supplierList as $supplier) { ?>
										<option value="<?php echo $supplier->id ?>"><?php echo $supplier->nama_supplier ?></option><?php
									} ?>
								</select>
							</div>
						</div>
					</div>
					<!-- /.rows -->
					<div class="row">
						<div class="col-md-4">
							<div class="form-group">
								<label for="namaBarang">Nama Barang</label>
								<select class="form-control" name="idBarang" id="idBarang" required>
									<?php if (!empty($barangList)) { 
										foreach ($barangList as $barang) { ?>
											<option value="<?php echo $barang->id ?>"><?php echo $barang->nama_barang ?></option><?php
										}
									} ?>
								</select>
							</div>
							<div class="form-group">
								<label for="qty">QTY</label>
								<input type="number" class="form-control" name="qty" id="qty" placeholder="Masukkan jumlah qty" required>
							</div>
							<div class="form-group">
								<label for="subTotal">Sub Total</label>
								<input type="number" class="form-control" name="subTotal" id="subTotal" placeholder="masukkan harga Beli" required>
							</div>
							
							<div class="form-group text-right">
								<button class="btn btn-primary" type="button" id="addTemp">Tambah</button>
							</div>
						</div>
						<div class="col-md-8">
							<?php $this->load->view('transaksi/masuk/pusat/supplier/tabledetail', $tempList); ?>
						</div>
					</div>
					<!-- /.rows -->
					<div class="form-group">
						<label for="keterangan">Keterangan</label>
						<input type="text" class="form-control" name="keterangan" id="keterangan">
					</div>
					<div class="form-group">
						<label for="total">Total</label>
						<input type="text" class="form-control" name="total" id="total" value="<?php echo $total ?>" disabled>
					</div>
				</form>
			</div>
			<!-- /.card-body -->
			<form id="formSubmit" method="post" action="<?php echo base_url(BaseUrl::DEFAULT_TRANSAKSI_MASUK_SUPPLIER_URL."/store") ?>">
				<div class="modal-footer justify-content-between">
					<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
					<button type="submit" class="btn btn-primary">Simpan</button>
				</div>
			</form>
		</div>
		<!-- /.card -->

	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->
<?php
  $this->load->view('base_template/footer');
  $this->load->view('base_template/script');
?>
<script type="text/javascript">
	$(document).ready(function(){
		showTempList();
		function showTempList(){
			var userId = $('#userId').val();
			var kategoriTransaksi = $('#kategoriTransaksi').val();
			var tipeTransaksi = $('#tipeTransaksi').val();
			$('#total').val('');

			$.ajax({
				type  : 'POST',
				url   : '<?php echo base_url('transaksi/temp/gettempbyuserid')?>',
				async : true,
				dataType : 'json',
				data : {
					userId: userId,
					kategoriTransaksi: kategoriTransaksi,
					tipeTransaksi: tipeTransaksi
				},
				success : function(data){
					var html = '';
					var i;
					var tempList = data.tempList;
					if (tempList.length > 0) {
						var index = 1;
						for(i=0; i<tempList.length; i++){
							html += '<tr>'+
								'<td>'+ (index++) +'</td>'+
								'<td>'+ tempList[i].barang.nama_barang +'</td>'+
								'<td>'+ tempList[i].qty +'</td>'+
								'<td>'+ tempList[i].sub_total +'</td>'+
								'<td>'+
									'<div class="row">'+
										'<button type="button" class="btn btn-block bg-gradient-danger btn btn-sm deleteTemp" id="'+ tempList[i].id +'">Hapus</button>'+
									'</div>'+
								'</td>'+
							'</tr>';
						}
					}
					
					$('#total').val(data.total);
					$('#showData').html(html);
				}
			});
		}

		$('#idSupplier').change(function(){ 
				var id=$(this).val();
				$.ajax({
						url : "<?php echo base_url('master/barang/getbarangbysupplier') ?>",
						method : "POST",
						data : {
							id: id
						},
						async : true,
						dataType : 'json',
						success: function(data){
							var html = '';
							var i;
							for(i=0; i<data.length; i++){
								html += '<option value='+data[i].id+'>'+data[i].nama_barang+'</option>';
							}
							$('#idBarang').html(html);
						}
				});
				return false;
		});

		showBarangList();
		function showBarangList() {
			var id = $('#idSupplier').val();
			$.ajax({
				type: 'POST',
				url: '<?php echo base_url('master/barang/getbarangbysupplier') ?>',
				async: true,
				dataType : 'json',
				data : {
					id: id
				},
				success: function(data) {
					var html = '';
					var i;
					for(i = 0; i < data.length; i++){
						html += '<option value='+ data[i].id +'>'+ data[i].nama_barang +'</option>';
					}
					$('#idBarang').html(html);
				}
			});
		}

		$('#addTemp').on('click', function() {
			var idSupplier = $('#idSupplier').val();
			var idBarang = $('#idBarang').val();
			var qty = $('#qty').val();
			var subTotal = $('#subTotal').val();
			var keterangan = $('#keterangan').val();
			var tipeTransaksi = $('#tipeTransaksi').val();
			var total = $('#total').val();
			var kategoriTransaksi = $('#kategoriTransaksi').val();
			$.ajax({
				type : "POST",
				url  : "<?php echo base_url('transaksi/temp/storetemp') ?>",
				data : {
					idBarang: idBarang,
					qty: qty,
					subTotal: subTotal,
					keterangan: keterangan,
					idSupplier: idSupplier,
					total: total,
					tipeTransaksi: tipeTransaksi,
					kategoriTransaksi: kategoriTransaksi,
					idPerusahaan: null,
					idKonsumen: null
				},
				dataType : 'json',
				success: function(data){
					$('#qty').val("");
					$('#subTotal').val("");
					$('#keterangan').val("");
					showTempList();
				}
			});
		});

		$("#showData").on('click','.deleteTemp',function(){
			var id = $(this).attr('id');
			$.ajax({
				url: '<?php echo base_url('transaksi/temp/delete'); ?>',
				type: 'POST',
				data: {
					id: id
				},
				success: function(response){
					showTempList();
				}
			});
		});

	});
</script>
