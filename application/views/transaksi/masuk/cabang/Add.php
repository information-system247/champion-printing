<?php 
  $this->load->view('base_template/head');
  $this->load->view('base_template/sidebar', $authorization);
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1>Transaksi Keluar</h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="#">Beranda</a></li>
						<li class="breadcrumb-item active">Transaksi Keluar</li>
						<li class="breadcrumb-item active">Cabang</li>
					</ol>
				</div>
			</div>
		</div><!-- /.container-fluid -->
	</section>

	<!-- Main content -->
	<section class="content">

		<!-- Default box -->
		<div class="card">
			<div class="card-header">
				<h3 class="card-title">Tambah Transaksi Keluar</h3>
			</div>
			<div class="card-body">
				<div class="row">
					<div class="col-md-6">
						<input type="hidden" name="userId" id="userId" value="<?php echo $authorization->user->id ?>" />
						<input type="hidden" name="kategoriTransaksi" id="kategoriTransaksi" value="<?php echo KategoriTransaksiConstant::MASUK ?>" />
						<input type="hidden" name="perusahaanId" id="perusahaanId" value="<?php echo $authorization->user->karyawan->perusahaan->id ?>" />
						<div class="form-group">
							<label for="tipeTransaksi">Tipe Transaksi</label>
							<input type="text" class="form-control" name="tipeTransaksi" id="tipeTransaksi" value="<?php echo TipeTransaksiMasukConstant::TRANS_MASUK_CABANG_DARI_PUSAT ?>" disabled>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label for="idPerusahaan">Nama Cabang</label>
							<select class="form-control" name="idPerusahaan" id="idPerusahaan" required></select>
						</div>
					</div>
				</div>
				<!-- /.rows -->
				<div class="row">
					<div class="col-md-4">
						<div class="form-group">
							<label for="namaBarang">Nama Barang</label>
							<select class="form-control" name="idBarang" id="idBarang" required></select>
						</div>
						<div class="form-group">
							<label for="qty">QTY</label>
							<input type="text" class="form-control" name="qty" id="qty" placeholder="Masukkan jumlah qty" required>
						</div>
						<div class="form-group text-right">
							<button class="btn btn-primary" type="button" id="addTemp">Tambah</button>
						</div>
					</div>
					<div class="col-md-8">
						<?php $this->load->view('transaksi/masuk/cabang/tabledetail'); ?>
					</div>
				</div>
				<!-- /.rows -->
				<div class="form-group">
					<label for="keterangan">Keterangan</label>
					<input type="text" class="form-control" name="keterangan" id="keterangan">
				</div>
			</div>
			<!-- /.card-body -->
			<form id="formSubmit" method="post" action="<?php echo base_url(BaseUrl::DEFAULT_TRANSAKSI_URL."/store") ?>">
				<div class="modal-footer justify-content-between">
					<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
					<button type="submit" class="btn btn-primary">Simpan</button>
				</div>
			</form>
		</div>
		<!-- /.card -->

	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->
<?php
  $this->load->view('base_template/footer');
  $this->load->view('base_template/script');
?>
<script type="text/javascript">
	$(document).ready(function(){
		showPerusahaanPusat();
		function showPerusahaanPusat() {
			$.ajax({
				type: 'POST',
				url: '<?php echo base_url(BaseUrl::DEFAULT_PERUSAHAAN_URL.'/getperusahaanpusat') ?>',
				async: true,
				dataType : 'json',
				success: function(data) {
					var html = '';
					var i;
					for(i = 0; i < data.length; i++){
						html += '<option value='+ data[i].id +'>'+ data[i].nama + ' ('+ data[i].level +')'+'</option>';
					}
					$('#idPerusahaan').html(html);
				}
			});
		}

		showBarangList();
		function showBarangList() {
			$.ajax({
				type: 'POST',
				url: '<?php echo base_url(BaseUrl::DEFAULT_BARANG_URL.'/getbaranglist') ?>',
				async: true,
				dataType : 'json',
				success: function(data) {
					var html = '';
					var i;
					var barangList = data.barangList;
					for(i = 0; i < barangList.length; i++){
						html += '<option value='+ barangList[i].id +'>'+ barangList[i].nama_barang +'</option>';
					}
					$('#idBarang').html(html);
				}
			});
		}

		showTempList();
		function showTempList(){
			var userId = $('#userId').val();
			var kategoriTransaksi = $('#kategoriTransaksi').val();
			var tipeTransaksi = $('#tipeTransaksi').val();
			$('#total').val('');

			$.ajax({
				type  : 'POST',
				url   : '<?php echo base_url('transaksi/temp/gettempbyuserid')?>',
				async : true,
				dataType : 'json',
				data : {
					userId: userId,
					kategoriTransaksi: kategoriTransaksi,
					tipeTransaksi: tipeTransaksi
				},
				success : function(data){
					var html = '';
					var i;
					var tempList = data.tempList;
					if (tempList.length > 0) {
						var index = 1;
						for(i=0; i<tempList.length; i++){
							html += '<tr>'+
								'<td>'+ (index++) +'</td>'+
								'<td>'+ tempList[i].barang.nama_barang +'</td>'+
								'<td>'+ tempList[i].qty +'</td>'+
								'<td>'+
									'<div class="row">'+
										'<button type="button" class="btn btn-block bg-gradient-danger btn btn-sm deleteTemp" id="'+ tempList[i].id +'">Hapus</button>'+
									'</div>'+
								'</td>'+
							'</tr>';
						}
					}
					
					$('#showData').html(html);
				}
			});
		}

		$('#addTemp').on('click', function() {
			var idPerusahaan = $('#idPerusahaan').val();
			var idBarang = $('#idBarang').val();
			var qty = $('#qty').val();
			var tipeTransaksi = $('#tipeTransaksi').val();
			var kategoriTransaksi = $('#kategoriTransaksi').val();
			$.ajax({
				type : "POST",
				url  : "<?php echo base_url('transaksi/temp/storetemp') ?>",
				data : {
					idBarang: idBarang,
					qty: qty,
					subTotal: null,
					keterangan: null,
					idSupplier: null,
					total: null,
					tipeTransaksi: tipeTransaksi,
					kategoriTransaksi: kategoriTransaksi,
					idPerusahaan: idPerusahaan,
					idKonsumen: null
				},
				dataType : 'json',
				success: function(data){
					$('#qty').val("");
					$('#subTotal').val("");
					$('#keterangan').val("");
					showTempList();
				}
			});
		});

	});
</script>
