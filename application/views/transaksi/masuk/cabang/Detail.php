<?php 
  $this->load->view('base_template/head');
  $this->load->view('base_template/sidebar', $authorization);
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Transaksi Masuk</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Master</a></li>
              <li class="breadcrumb-item active">Transaksi Masuk</li>
							<li class="breadcrumb-item active">Cabang</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Detail Transaksi Masuk Retur</h3>
								<input type="hidden" id="transaksiId" value="<?php echo $transaksiOptional[0]->id ?>" />
              </div>
							<div class="card-body">
							<table id="dataTable1" class="table table-bordered table-hover">
								<thead>
									<tr>
										<th style="width: 30px; text-align: center;">No</th>
										<th style="text-align: center;">Nama Barang</th>
										<th style="text-align: center;">QTY</th>
									</tr>
								</thead>
								<tbody id="showData">
								</tbody>
								</table>
							</div>
            </div>
          </div>
        </div>
      </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php
  $this->load->view('base_template/footer');
  $this->load->view('base_template/script');
?>
<script type="text/javascript">
	$(document).ready(function(){
		showDetailList();
		function showDetailList(){
			var transaksiId = $('#transaksiId').val();

			$.ajax({
				type  : 'POST',
				url   : '<?php echo base_url('transaksi/histori/getdetailhistoritransaksi')?>',
				async : true,
				dataType : 'json',
				data : {
					transaksiId: transaksiId,
				},
				success : function(data){
					var html = '';
					var i;
					var transId = data[0].id_transaksi;
					if (data.length > 0) {
						var index = 1;
						for(i=0; i<data.length; i++){
							html += '<tr>'+
								'<td>'+ (index++) +'</td>'+
								'<td>'+ data[i].barang.nama_barang +'</td>'+
								'<td>'+ data[i].qty +'</td>'+
							'</tr>';
						}
					}
					
					$('#showData').html(html);
				}
			});
		}

	});
</script>
