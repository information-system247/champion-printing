<!-- modal Pengaturan -->
<div class="modal fade" id="modalAdd">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header bg-info">
        <h4 class="modal-title">Tambah Karyawan</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form id="formKaryawan" method="post" action="<?php echo base_url('master/karyawan/store') ?>">
        <div class="modal-body">
          <input type="text" id="id" name="id" hidden>
          <div class="form-group">
            <label for="namaLengkap">Nama lengkap</label>
            <input type="text" class="form-control" id="namaLengkap" name="namaLengkap" placeholder="masukan nama lengkap" onkeypress="return event.charCode >= 65 && event.charCode <= 90 || event.charCode >= 97 && event.charCode <= 122 || event.charCode == 46 || event.charCode == 39 || event.charCode == 32 || event.charCode == 96" maxlength="50" required>
          </div>
          <div class="form-group">
            <label for="alamat">Alamat</label>
            <textarea class="form-control" rows="3" name="alamat" id="alamat" placeholder="Masukan alamat" required></textarea>
          </div>
          <div class="form-group">
            <label for="telepon">Telepon</label>
            <input type="text" class="form-control" id="telepon" name="telepon" placeholder="masukan nomor telepon" minlength="12" maxlength="15" onkeypress="return event.charCode >= 48 && event.charCode <=57" required>
          </div>
          <div class="form-group">
            <label for="idPerusahaan">Perusahaan</label>
            <select class="form-control" id="idPerusahaan" name="idPerusahaan" required><?php 
              foreach ($perusahaanList as $data) { ?>
                <option value="<?php echo $data->id ?>"><?php echo $data->nama ?></option><?php
              } ?>
            </select>
          </div>
          <div class="form-group">
            <label for="idPosisi">Posisi Karyawan</label>
            <select class="form-control" name="idPosisi" id="idPosisi" required>
              <?php foreach ($posisiList as $posisi) { ?>
                <option value="<?php echo $posisi->id ?>"><?php echo $posisi->nama ?></option><?php
              }?>
            </select>
          </div>
        </div>
        <!-- /.card-body -->
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
          <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
      </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->