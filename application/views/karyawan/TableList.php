<table id="dataTable" class="table table-bordered table-hover">
	<thead>
		<tr>
			<th style="text-align: center;">No</th>
			<th style="text-align: center;">Nama Lengkap</th>
			<th style="text-align: center;">Alamat</th>
			<th style="text-align: center;">Perusahaan</th>
			<th style="text-align: center;">Telepon</th>
			<th style="text-align: center;">Posisi</th>
			<th style="width: 110px; text-align: center;">Aksi</th>
		</tr>
	</thead>
	<tbody><?php $no = 1;
		if (count($karyawanList) == 0) { ?>
			<tr>
				<td colspan="6" style="text-align: center;">Data Masih Kosong ...</td>
			</tr><?php
		} else {
			foreach ($karyawanList as $karyawan) {
				$data = array(
					'data' => $karyawan
				);?>
				<tr>
					<td><?php echo $no++; ?></td>
					<td><?php echo $karyawan->nama ?></td>
					<td><?php echo $karyawan->alamat ?></td>
					<td><?php echo $karyawan->perusahaan->nama ?></td>
					<td><?php echo $karyawan->telepon ?></td>
					<td><?php echo $karyawan->posisi->nama ?></td>
					<td>
						<div class="row">
							<div class="col-6">
								<button type="button" class="btn btn-block bg-gradient-warning btn btn-sm" data-toggle="modal" data-target="#modalUpdate<?php echo $karyawan->id ?>">Edit</button>
							</div>
							<div class="col-6">
								<button type="button" class="btn btn-block bg-gradient-danger btn btn-sm" data-toggle="modal" data-target="#modalDelete<?php echo $karyawan->id ?>">Hapus</button>
							</div>
						</div>
					</td>
				</tr>
				<?php $this->load->view('karyawan/delete', $data);
				$this->load->view('karyawan/edit', $data);
			}
		} ?>
	</tbody>
</table>
