<?php 
  $this->load->view('base_template/head');
  $this->load->view('base_template/sidebar', $authorization);
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Stok</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Stok</a></li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Stok</h3>
								<input type="hidden" name="idPerusahaan" id="idPerusahaan" value="<?php echo $authorization->user->karyawan->perusahaan->id ?>" />
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <?php $this->load->view("stok/tableList") ?>
              </div>
            </div>
          </div>
        </div>
      </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <button type="submit" class="btn btn-primary swalDefaultSuccess" style="visibility: hidden">success</button>
  <button type="submit" class="btn btn-primary swalDefaultError" style="visibility: hidden">failed</button>
  <button type="submit" class="btn btn-primary swalDefaultWarning" style="visibility: hidden">warning</button>

<?php
  $this->load->view('base_template/footer');
  $this->load->view('base_template/script');

  if ($this->session->flashdata('success')) {
    echo $this->session->flashdata('success');
    unset($_SESSION['success']);
  }
  
  if ($this->session->flashdata('failed')) {
    echo $this->session->flashdata('failed');
    unset($_SESSION['failed']);
  }
  
  if ($this->session->flashdata('warning')) {
    echo $this->session->flashdata('warning');
    unset($_SESSION['warning']);
  }
?>
<script type="text/javascript">
	$(document).ready(function(){
		showTempList();
		function showTempList(){
			var idPerusahaan = $("#idPerusahaan").val();
			$.ajax({
				type  : 'POST',
				url   : '<?php echo base_url(BaseUrl::DEFAULT_STOK_URL.'/getstok')?>',
				async : true,
				dataType : 'json',
				data : {
					idPerusahaan: idPerusahaan,
				},
				success : function(data){
					var html = '';
					var i;
					var stokList = data.stokList;
					if (stokList.length > 0) {
						var index = 1;
						for(i=0; i<stokList.length; i++){
							html += '<tr>'+
								'<td>'+ (index++) +'</td>'+
								'<td>'+ stokList[i].barang.nama_barang +'</td>'+
								'<td>'+ stokList[i].qty +'</td>'+
								'<td>'+ stokList[i].barang.satuan +'</td>'+
							'</tr>';
						}
					}

					$('#showData').html(html);
				}
			});
		}
		
	});
</script>
