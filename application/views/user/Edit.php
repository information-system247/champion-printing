<!-- modal Pengaturan -->
<div class="modal fade" id="modalUpdate<?php echo $data->id ?>">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header bg-info">
        <h4 class="modal-title">Ubah Karyawan</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form id="formPerusashaan" method="post" action="<?php echo base_url('master/user/update') ?>">
        <div class="modal-body">
          <input type="text" name="id" id="id" value="<?php echo $data->id ?>" hidden>
          <div class="form-group">
            <label for="idKaryawan">Nama Karyawan</label>
            <select class="form-control" id="idKaryawan" name="idKaryawan" required>
              <?php foreach ($karyawanList as $karyawan) { 
                if ($data->id == $karyawan->id) { ?>
                  <option value="<?php echo $karyawan->id; ?>" selected><?php echo $karyawan->nama.' - '.$karyawan->perusahaan->nama; ?></option><?php
                } else { ?>
                  <option value="<?php echo $karyawan->id; ?>"><?php echo $karyawan->nama.' - '.$karyawan->perusahaan->nama; ?></option><?php
                }
              } ?>
            </select>
          </div>
          <div class="form-group">
            <label for="username">username</label>
            <input class="form-control" name="username" id="username" value="<?php echo $data->username ?>" placeholder="Masukan username" required>
          </div>
          <div class="form-group">
            <label for="password">password</label>
            <input type="password" class="form-control" id="password" name="password" value="<?php echo $this->encryption->decrypt($data->password); ?>" placeholder="masukan password" required>
          </div>
        </div>
        <!-- /.card-body -->
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
          <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
      </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
