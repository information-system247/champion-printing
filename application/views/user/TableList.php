<table id="dataTable" class="table table-bordered table-hover">
	<thead>
		<tr>
			<th>No</th>
			<th>Nama Karyawan</th>
			<th>Nama Perusahaan</th>
			<th>username</th>
			<th>Password</th>
			<th>Posisi</th>
			<th width="13%">Aksi</th>
		</tr>
	</thead>
	<tbody><?php $no = 1;
		if (count($userList) == 0) { ?>
			<tr>
				<td colspan="6" align="center">Data Masih Kosong ...</td>
			</tr><?php
		} else {
			foreach ($userList as $user) {
				$data = array(
					'data' => $user
				); ?>
				<tr>
					<td><?php echo $no++; ?></td>
					<td><?php echo $user->karyawan->nama ?></td>
					<td><?php echo $user->karyawan->perusahaan->nama ?></td>
					<td><?php echo $user->username ?></td>
					<td><?php echo ChampionPrintingConstant::maskingPassword($this->encryption->decrypt($user->password)) ?></td>
					<td><?php echo $user->karyawan->posisi->nama ?></td>
					<td>
						<div class="row">
							<div class="col-6">
							<button type="button" class="btn btn-block bg-gradient-warning btn btn-sm" data-toggle="modal" data-target="#modalUpdate<?php echo $user->id ?>">Ubah</button>
							</div>
							<div class="col-6">
								<button type="button" class="btn btn-block bg-gradient-danger btn btn-sm" data-toggle="modal" data-target="#modalDelete<?php echo $user->id ?>">Hapus</button>
							</div>
						</div>
					</td>
				</tr>
				<?php $this->load->view('user/delete', $data);
				$this->load->view('user/edit', $data);
			}
		} ?>
	</tbody>
</table>
