<!-- modal Pengaturan -->
<div class="modal fade" id="modalAdd">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header bg-info">
        <h4 class="modal-title">Tambah User</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form id="formPerusashaan" method="post" action="<?php echo base_url('master/user/store') ?>">
        <div class="modal-body">
          <input type="text" id="id" name="id" hidden>
          <div class="form-group">
            <label for="idKaryawan">Nama Karyawan</label>
            <select class="form-control" id="idKaryawan" name="idKaryawan" required>
              <?php foreach ($karyawanList as $karyawan) { ?>
                <option value="<?php echo $karyawan->id; ?>"><?php echo $karyawan->nama.' - '.$karyawan->perusahaan->nama; ?></option><?php
              } ?>
            </select>
          </div>
          <div class="form-group">
            <label for="username">username</label>
            <input class="form-control" name="username" id="username" placeholder="Masukan username" required>
          </div>
          <div class="form-group">
            <label for="password">password</label>
            <input type="password" class="form-control" id="password" name="password" placeholder="masukan password" required>
          </div>
        </div>
        <!-- /.card-body -->
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
          <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
      </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
