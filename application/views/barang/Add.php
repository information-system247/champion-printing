<!-- modal Pengaturan -->
<div class="modal fade" id="modalAdd">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header bg-info">
        <h4 class="modal-title">Tambah Barang</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form id="formbarang" method="post" action="<?php echo base_url('master/barang/store') ?>">
        <div class="modal-body">
          <input type="text" id="id" name="id" hidden>
          <div class="form-group">
            <label for="namaBarang">Nama Barang</label>
            <input type="text" class="form-control" id="namaBarang" name="namaBarang" placeholder="masukan nama barang" onkeypress="return event.charCode >= 65 && event.charCode <= 90 || event.charCode >= 97 && event.charCode <= 122 || event.charCode == 46 || event.charCode == 39 || event.charCode == 32 || event.charCode == 96" maxlength="50" required>
          </div>
          <div class="form-group">
            <label for="idKategori">Nama Kategori</label>
            <select class="form-control" name="idKategori" id="idKategori" required>
              <?php foreach ($kategoriList as $kategori) { ?>
                <option value="<?php echo $kategori->id ?>"><?php echo $kategori->nama_kategori ?></option><?php
              } ?>
            </select>
          </div>
          <div class="form-group">
            <label for="idTipe">Nama Tipe</label>
            <select class="form-control" name="idTipe" id="idTipe" required>
              <?php foreach ($tipeList as $tipe) { ?>
                <option value="<?php echo $tipe->id ?>"><?php echo $tipe->nama_tipe ?></option><?php
              } ?>
            </select>
          </div>
					<textarea class="form-control" name="skDiskon" id="skDiskon" rows="3" placeholder="Masukkan syarat dan ketentuan diskon" hidden></textarea>
					<input type="hidden" class="form-control" name="diskon" id="diskon" placeholder="Masukkan diskon" onkeypress="return event.charCode >= 48 && event.charCode <= 57 || event.charCode == 46">
          <!-- <div class="form-group">
            <label for="skDiskon">Syarat dan Ketentuan Diskon</label>
            
          </div> -->
          <!-- <div class="form-group">
            <label for="diskon">Diskon</label>
            
          </div> -->
          <div class="form-group">
            <label for="hargaJual">harga Jual</label>
            <input type="text" class="form-control" name="hargaJual" id="hargaJual" placeholder="masukkan harga jual" onkeypress="return event.charCode >= 48 && event.charCode <= 57" required>
          </div>
          <div class="form-group">
            <label for="idSupplier">Nama Supplier</label>
            <select class="form-control" name="idSupplier" id="idSupplier" required>
              <?php foreach ($supplierList as $supplier) { ?>
                <option class="form-control" value="<?php echo $supplier->id ?>"><?php echo $supplier->nama_supplier ?></option><?php
              } ?>
            </select>
          </div>
          <div class="form-group">
            <label for="satuan">Satuan</label>
            <input type="text" class="form-control" name="satuan" id="satuan" required>
          </div>
        </div>
        <!-- /.card-body -->
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
          <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
      </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
