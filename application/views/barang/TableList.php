<table id="dataTable" class="table table-bordered table-hover">
	<thead>
		<tr>
			<th style="text-align: center;">No</th>
			<th style="text-align: center;">Nama Barang</th>
			<th style="text-align: center;">Nama Kategori</th>
			<th style="text-align: center;">Nama Tipe</th>
			<!-- <th style="text-align: center;">SK Diskon</th>
			<th style="text-align: center;">Diskon</th> -->
			<th style="text-align: center;">harga Jual</th>
			<th style="text-align: center;">Nama Supplier</th>
			<th style="text-align: center;">Satuan</th>
			<th style="width: 110px; text-align: center;">Aksi</th>
		</tr>
	</thead>
	<tbody><?php $no = 1;
		if (count($barangList) == 0) { ?>
			<tr>
				<td colspan="10" style="text-align: center;">Data Masih Kosong ...</td>
			</tr><?php
		} else {
			foreach ($barangList as $barang) {
				$data = array(
					'data' => $barang
				); ?>
				<tr>
					<td><?php echo $no++; ?></td>
					<td><?php echo $barang->nama_barang ?></td>
					<td><?php echo $barang->kategori->nama_kategori ?></td>
					<td><?php echo $barang->tipe->nama_tipe ?></td>
					<!-- <td><?php echo empty($barang->sk_diskon) ? "-" : $barang->sk_diskon ?></td>
					<td><?php echo $barang->diskon ?></td> -->
					<td><?php echo $barang->harga_jual ?></td>
					<td><?php echo $barang->supplier->nama_supplier ?></td>
					<td><?php echo $barang->satuan ?></td>
					<td>
						<div class="row">
							<div class="col-6">
								<button type="button" class="btn btn-block bg-gradient-warning btn btn-sm" data-toggle="modal" data-target="#modalUpdate<?php echo $barang->id ?>">Edit</button>
							</div>
							<div class="col-6">
								<button type="button" class="btn btn-block bg-gradient-danger btn btn-sm" data-toggle="modal" data-target="#modalDelete<?php echo $barang->id ?>">Hapus</button>
							</div>
						</div>
					</td>
				</tr>
				<?php $this->load->view('barang/delete', $data);
				$this->load->view('barang/edit', $data);
			}
		} ?>
	</tbody>
</table>
