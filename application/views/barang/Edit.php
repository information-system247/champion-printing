<!-- modal Pengaturan -->
<div class="modal fade" id="modalUpdate<?php echo $data->id ?>">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header bg-info">
        <h4 class="modal-title">Ubah Barang</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form id="formBarang" method="post" action="<?php echo base_url('master/barang/update') ?>">
        <div class="modal-body">
          <input type="text" name="id" id="id" value="<?php echo $data->id ?>" hidden>
          <div class="form-group">
            <label for="namaBarang">Nama Barang</label>
            <input type="text" class="form-control" id="namaBarang" name="namaBarang" value="<?php echo $data->nama_barang ?>" placeholder="masukan nama barang" onkeypress="return event.charCode >= 65 && event.charCode <= 90 || event.charCode >= 97 && event.charCode <= 122 || event.charCode == 46 || event.charCode == 39 || event.charCode == 32 || event.charCode == 96" maxlength="50" required>
          </div>
          <div class="form-group">
            <label for="idKategori">Nama Kategori</label>
            <select class="form-control" name="idKategori" id="idKategori">
              <?php foreach ($kategoriList as $kategori) {
                if ($data->kategori->id == $kategori->id) { ?>
                  <option value="<?php echo $kategori->id ?>" selected><?php echo $kategori->nama_kategori ?></option><?php
                } else { ?>
                  <option value="<?php echo $kategori->id ?>"><?php echo $kategori->nama_kategori ?></option><?php
                }
              } ?>
            </select>
          </div>
          <div class="form-group">
            <label class="form-label" for="idTipe">Nama Tipe</label>
            <select class="form-control" name="idTipe" id="idTipe">
              <?php foreach ($tipeList as $tipe) {
                if ($data->tipe->id == $tipe->id) { ?>
                  <option value="<?php echo $tipe->id ?>" selected><?php echo $tipe->nama_tipe ?></option><?php
                } else { ?>
                  <option value="<?php echo $tipe->id ?>"><?php echo $tipe->nama_tipe ?></option><?php
                }
              }?>
            </select>
          </div>
					<textarea class="form-control" name="skDiskon" id="skDiskon" rows="3" hidden><?php echo $data->sk_diskon ?></textarea>
					<input type="hidden" class="form-control" name="diskon" id="diskon" value="<?php echo $data->diskon ?>" placeholder="Masukkan diskon" onkeypress="return event.charCode >= 48 && event.charCode <= 57 || event.charCode == 46">
          <!-- <div class="form-group">
            <label for="skDiskon">Syarat dan Ketentuan Diskon</label>
            
          </div> -->
          <!-- <div class="form-group">
            <label for="diskon">Diskon</label>
            
          </div> -->
          <div class="form-group">
            <label for="hargaJual">Harga Jual</label>
            <input type="text" class="form-control" name="hargaJual" id="hargaJual" value="<?php echo $data->harga_jual ?>" placeholder="Masukkan harga jual barang" onkeypress="return event.charCode >= 48 && event.charCode <= 57" required>
          </div>
          <div class="form-group">
            <label for="idSupplier">Nama Supplier</label>
            <select class="form-control" name="idSupplier" id="idSupplier">
              <?php foreach ($supplierList as $supplier) {
                if ($data->supplier->id == $supplier->id) { ?>
                  <option value="<?php echo $supplier->id ?>" selected><?php echo $supplier->nama_supplier ?></option><?php
                } else { ?>
                  <option value="<?php echo $supplier->id ?>"><?php echo $supplier->nama_supplier ?></option><?php
                }
              }?>
            </select>
          </div>
          <div class="form-group">
            <label for="satuan">Satuan</label>
            <input type="text" class="form-control" name="satuan" id="satuan" value="<?php echo $data->satuan ?>" required>
          </div>
        </div>
        <!-- /.card-body -->
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
          <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
      </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
