<!-- modal Pengaturan -->
<div class="modal fade" id="modalUpdate<?php echo $data->id ?>">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header bg-info">
        <h4 class="modal-title">Ubah Perusahaan</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form id="formPerusashaan" method="post" action="<?php echo base_url('master/perusahaan/update') ?>">
        <div class="modal-body">
          <input type="text" name="id" id="id" value="<?php echo $data->id ?>" hidden>
          <div class="form-group">
            <label for="namaLengkap">Nama lengkap</label>
            <input type="text" class="form-control" id="namaLengkap" name="namaLengkap" value="<?php echo $data->nama ?>" placeholder="masukan nama lengkap" onkeypress="return event.charCode >= 65 && event.charCode <= 90 || event.charCode >= 97 && event.charCode <= 122 || event.charCode == 46 || event.charCode == 39 || event.charCode == 32 || event.charCode == 96" maxlength="50" required>
          </div>
          <div class="form-group">
            <label for="alamat">Alamat</label>
            <textarea class="form-control" rows="3" name="alamat" id="alamat" placeholder="Masukan alamat" required><?php echo $data->alamat ?></textarea>
          </div>
          <div class="form-group">
            <label for="telepon">Telepon</label>
            <input type="text" class="form-control" id="telepon" name="telepon" value="<?php echo $data->telepon ?>" placeholder="masukan nomor telepon" minlength="12" maxlength="15" onkeypress="return event.charCode >= 48 && event.charCode <=57" required>
          </div>
          <div class="form-group">
            <label for="level">Level</label>
            <select class="form-control" id="level" name="level">
              <?php if ($data->level == "PUSAT") {
                ?><option>Pusat</option><?php
              } else {
                ?><option>Cabang</option><?php
              } ?>
            </select>
          </div>
        </div>
        <!-- /.card-body -->
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
          <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
      </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->