<table id="dataTable" class="table table-bordered table-hover">
	<thead>
		<tr>
			<th>No</th>
			<th>Nama Lengkap</th>
			<th>Alamat</th>
			<th>Telepon</th>
			<th>Level</th>
			<th width="13%">Aksi</th>
		</tr>
	</thead>
	<tbody><?php $no = 1;
		if (count($perusahaanList) == 0) { ?>
			<tr>
				<td colspan="6" align="center">Data Masih Kosong ...</td>
			</tr><?php
		} else {
			foreach ($perusahaanList as $data) {
				$perusahaanOptional = array(
					'data' => $data
				); ?>
				<tr>
					<td><?php echo $no++; ?></td>
					<td><?php echo $data->nama ?></td>
					<td><?php echo $data->alamat ?></td>
					<td><?php echo $data->telepon ?></td>
					<td><?php echo $data->level ?></td>
					<td>
						<div class="row">
							<div class="col-6">
								<button type="button" class="btn btn-block bg-gradient-warning btn btn-sm" data-toggle="modal" data-target="#modalUpdate<?php echo $data->id ?>">Ubah</button>
							</div>
							<div class="col-6">
								<button type="button" class="btn btn-block bg-gradient-danger btn btn-sm" data-toggle="modal" data-target="#modalDelete<?php echo $data->id ?>">Hapus</button>
							</div>
						</div>
					</td>
				</tr>
				<?php $this->load->view('perusahaan/delete', $perusahaanOptional);
				$this->load->view('perusahaan/edit', $perusahaanOptional);
			}
		} ?>
	</tbody>
</table>
