<table id="dataTable" class="table table-bordered table-hover">
	<thead>
		<tr>
			<th style="text-align: center;">No</th>
			<th style="text-align: center;">Nama Tipe</th>
			<th style="width: 110px; text-align: center;">Aksi</th>
		</tr>
	</thead>
	<tbody><?php $no = 1;
		if (count($tipeList) == 0) { ?>
			<tr>
				<td colspan="3" style="text-align: center;">Data Masih Kosong ...</td>
			</tr><?php
		} else {
			foreach ($tipeList as $tipe) {
				$data = array(
					'data' => $tipe
				);?>
				<tr>
					<td><?php echo $no++; ?></td>
					<td><?php echo $tipe->nama_tipe ?></td>
					<td>
						<div class="row">
							<div class="col-6">
								<button type="button" class="btn btn-block bg-gradient-warning btn btn-sm" data-toggle="modal" data-target="#modalUpdate<?php echo $tipe->id ?>">Edit</button>
							</div>
							<div class="col-6">
								<button type="button" class="btn btn-block bg-gradient-danger btn btn-sm" data-toggle="modal" data-target="#modalDelete<?php echo $tipe->id ?>">Hapus</button>
							</div>
						</div>
					</td>
				</tr>
				<?php $this->load->view('tipe_barang/delete', $data);
				$this->load->view('tipe_barang/edit', $data);
			}
		} ?>
	</tbody>
</table>
