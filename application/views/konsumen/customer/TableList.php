<table id="dataTable" class="table table-bordered table-hover">
	<thead>
		<tr>
			<th style="text-align: center;">No</th>
			<th style="text-align: center;">Nama Customer</th>
			<th style="width: 110px; text-align: center;">Aksi</th>
		</tr>
	</thead>
	<tbody><?php $no = 1;
		if (count($customerList) == 0) { ?>
			<tr>
				<td colspan="3" style="text-align: center;">Data Masih Kosong ...</td>
			</tr><?php
		} else {
			foreach ($customerList as $customer) {
				$data = array(
					'data' => $customer
				);?>
				<tr>
					<td><?php echo $no++; ?></td>
					<td><?php echo $customer->nama_konsumen ?></td>
					<td>
						<div class="row">
							<div class="col-6">
								<button type="button" class="btn btn-block bg-gradient-warning btn btn-sm" data-toggle="modal" data-target="#modalUpdate<?php echo $customer->id ?>">Edit</button>
							</div>
							<div class="col-6">
								<button type="button" class="btn btn-block bg-gradient-danger btn btn-sm" data-toggle="modal" data-target="#modalDelete<?php echo $customer->id ?>">Hapus</button>
							</div>
						</div>
					</td>
				</tr>
				<?php $this->load->view('konsumen/customer/delete', $data);
				$this->load->view('konsumen/customer/edit', $data);
			}
		} ?>
	</tbody>
</table>
