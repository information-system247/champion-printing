<!-- modal Pengaturan -->
<div class="modal fade" id="modalUpdate<?php echo $data->id ?>">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header bg-info">
        <h4 class="modal-title">Ubah Perusahaan</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form id="formPerusahaan" method="post" action="<?php echo base_url('master/konsumen/perusahaanupdate') ?>">
        <div class="modal-body">
          <input type="text" name="id" id="id" value="<?php echo $data->id ?>" hidden>
          <div class="form-group">
            <label for="namaPerusahaan">Nama Perusahaan</label>
            <input type="text" class="form-control" id="namaPerusahaan" name="namaPerusahaan" value="<?php echo $data->nama_konsumen ?>" placeholder="masukan nama perusahaan" onkeypress="return event.charCode >= 65 && event.charCode <= 90 || event.charCode >= 97 && event.charCode <= 122 || event.charCode == 46 || event.charCode == 39 || event.charCode == 32 || event.charCode == 96" maxlength="50" required>
          </div>
          <div class="form-group">
            <label for="alamat">Alamat</label>
            <textarea class="form-control" name="alamat" id="alamat" rows="3" required><?php echo $data->alamat ?></textarea>
          </div>
          <div class="form-group">
            <label for="telepon">Telepon</label>
            <input type="text" class="form-control" name="telepon" id="telepon" value="<?php echo $data->telepon ?>" required>
          </div>
        </div>
        <!-- /.card-body -->
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
          <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
      </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
