<table id="dataTable" class="table table-bordered table-hover">
	<thead>
		<tr>
			<th style="text-align: center;">No</th>
			<th style="text-align: center;">Nama Perusahaan</th>
			<th style="text-align: center;">Alamat</th>
			<th style="text-align: center;">Telepon</th>
			<th style="width: 110px; text-align: center;">Aksi</th>
		</tr>
	</thead>
	<tbody><?php $no = 1;
		if (count($perusahaanList) == 0) { ?>
			<tr>
				<td colspan="5" style="text-align: center;">Data Masih Kosong ...</td>
			</tr><?php
		} else {
			foreach ($perusahaanList as $perusahaan) {
				$data = array(
					'data' => $perusahaan
				);?>
				<tr>
					<td><?php echo $no++; ?></td>
					<td><?php echo $perusahaan->nama_konsumen ?></td>
					<td><?php echo $perusahaan->alamat ?></td>
					<td><?php echo $perusahaan->telepon ?></td>
					<td>
						<div class="row">
							<div class="col-6">
								<button type="button" class="btn btn-block bg-gradient-warning btn btn-sm" data-toggle="modal" data-target="#modalUpdate<?php echo $perusahaan->id ?>">Edit</button>
							</div>
							<div class="col-6">
								<button type="button" class="btn btn-block bg-gradient-danger btn btn-sm" data-toggle="modal" data-target="#modalDelete<?php echo $perusahaan->id ?>">Hapus</button>
							</div>
						</div>
					</td>
				</tr>
				<?php $this->load->view('konsumen/perusahaan/delete', $data);
				$this->load->view('konsumen/perusahaan/edit', $data);
			}
		} ?>
	</tbody>
</table>
