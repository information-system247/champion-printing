<?php 
  $this->load->view('base_template/head');
  $this->load->view('base_template/sidebar', $authorization);
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<div class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1 class="m-0">Laporan Transaksi Masuk Supplier</h1>
				</div><!-- /.col -->
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="#">Laporan</a></li>
						<li class="breadcrumb-item active">Transaksi Masuk Supplier</li>
					</ol>
				</div><!-- /.col -->
			</div><!-- /.row -->
		</div><!-- /.container-fluid -->
	</div>
	<!-- /.content-header -->

	<!-- Main content -->
	<section class="content">
		<div class="container-fluid">
			<div class="row">
				<div class="col-12">
					<div class="card">
						<div class="card-header">
							<h3 class="card-title">Laporan Transaksi masuk Supplier</h3>
						</div>
						<div class="card-body">
							<div class="row">
								<div class="col-1">
									<div class="card">
										<a href="<?php echo base_url(BaseUrl::DEFAULT_EXPORT_PDF.'/transaksimasuksupplier') ?>" type="button" class="btn btn-block btn-primary">Cetak</a>
									</div>
								</div>
							</div>
						</div>
						<!-- /.card-header -->
						<div class="card-body">
							<?php $this->load->view("laporan/transaksi/masuk/supplier/tableList") ?>
						</div>
					</div>
				</div>
			</div>
		</div><!--/. container-fluid -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->
<?php 
$this->load->view('base_template/footer');
$this->load->view('base_template/script');
?>
<script type="text/javascript">
	$(document).ready(function(){
		getTransaksiMasukSupplierList();
		function getTransaksiMasukSupplierList() {
			$.ajax({
				type  : 'POST',
				url   : '<?php echo base_url('laporan/transaksi/masuk/gettransaksimasuksupplier')?>',
				async : true,
				dataType : 'json',
				success : function(data){
					var html = '';
					var i;
					if (data.length > 0) {
						var index = 1;
						for(i=0; i<data.length; i++){
							html += '<tr>'+
								'<td>'+ (index++) +'</td>'+
								'<td>'+ data[i].tgl_transaksi +'</td>'+
								'<td>'+ data[i].supplier.nama_supplier +'</td>'+
								'<td>'+ data[i].total +'</td>'+
								'<td>'+
									'<div class="row">'+
										'<a href="<?php echo base_url(BaseUrl::DEFAULT_TRANSAKSI_MASUK_SUPPLIER_URL.'/detailtransaksi').'/' ?>'+ data[i].id +'" type="button" class="btn btn-block bg-gradient-warning btn btn-sm">Detail</a>'+
									'</div>'+
								'</td>'+
							'</tr>';
						}
					}
					$('#showData').html(html);
				}
			});
		}

	});
</script>
