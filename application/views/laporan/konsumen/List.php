<?php 
  $this->load->view('base_template/head');
  $this->load->view('base_template/sidebar', $authorization);
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<div class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1 class="m-0">Laporan Konsumen</h1>
				</div><!-- /.col -->
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="#">Laporan</a></li>
						<li class="breadcrumb-item active">Konsumen</li>
					</ol>
				</div><!-- /.col -->
			</div><!-- /.row -->
		</div><!-- /.container-fluid -->
	</div>
	<!-- /.content-header -->

	<!-- Main content -->
	<section class="content">
		<div class="container-fluid">
			<div class="row">
				<div class="col-12">
					<div class="card">
						<div class="card-header">
							<h3 class="card-title">Laporan Konsumen</h3>
						</div>
						<div class="card-body">
							<div class="row">
								<div class="col-1">
									<div class="card">
										<a href="<?php echo base_url(BaseUrl::DEFAULT_EXPORT_PDF.'/konsumen') ?>" type="button" class="btn btn-block btn-primary">Cetak</a>
									</div>
								</div>
							</div>
						</div>
						<!-- /.card-header -->
						<div class="card-body">
							<?php $this->load->view("laporan/konsumen/tableList") ?>
						</div>
					</div>
				</div>
			</div>
		</div><!--/. container-fluid -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->
<?php 
$this->load->view('base_template/footer');
$this->load->view('base_template/script');
?>
<script type="text/javascript">
	$(document).ready(function(){
		getKonsumenList();
		function getKonsumenList() {
			$.ajax({
				url : "<?php echo base_url('laporan/konsumen/getkonsumenlist') ?>",
				method : "POST",
				async : true,
				dataType : 'json',
				success: function(data){
					var html = '';
					var i;
					if (data.length > 0) {
						var index = 1;
						for(i=0; i<data.length; i++){
							var alamat = "-";
							if (data[i].alamat != null) {
								alamat = data[i].alamat;
							}

							var telepon = "-";
							if (data[i].telepon != null) {
								telepon = data[i].telepon;
							}
							html += '<tr>'+
								'<td>'+ (index++) +'</td>'+
								'<td>'+ data[i].kategori +'</td>'+
								'<td>'+ data[i].nama_konsumen +'</td>'+
								'<td>'+ alamat +'</td>'+
								'<td>'+ telepon +'</td>'+
							'</tr>';
						}
					}
					
					$('#showData').html(html);
				}
			});
		}

	});
</script>
