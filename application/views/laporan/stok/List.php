<?php 
  $this->load->view('base_template/head');
  $this->load->view('base_template/sidebar', $authorization);
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<div class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1 class="m-0">Laporan Stok Barang</h1>
				</div><!-- /.col -->
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="#">Laporan</a></li>
						<li class="breadcrumb-item active">Stok Barang</li>
					</ol>
				</div><!-- /.col -->
			</div><!-- /.row -->
		</div><!-- /.container-fluid -->
	</div>
	<!-- /.content-header -->

	<!-- Main content -->
	<section class="content">
		<div class="container-fluid">
			<div class="row">
				<div class="col-12">
					<div class="card">
						<div class="card-header">
							<h3 class="card-title">Laporan Stok Barang</h3>
						</div>
						<div class="row">
							<div class="col-md-2">
								<div class="card-body">
									<label class="form-group">Nama Perusahaan</label>
									<select class="form-control" name="perusahaanId" id="perusahaanId"></select>
								</div>
							</div>
							<div class="col-1">
								<br><br><br>
								<div class="card">
									<a href="<?php echo base_url(BaseUrl::DEFAULT_EXPORT_PDF.'/stok') ?>" type="button" class="btn btn-block btn-primary">Cetak</a>
								</div>
							</div>
						</div>
						<!-- /.card-header -->
						<div class="card-body">
							<?php $this->load->view("laporan/stok/tableList") ?>
						</div>
					</div>
				</div>
			</div>
		</div><!--/. container-fluid -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->
<?php 
$this->load->view('base_template/footer');
$this->load->view('base_template/script');
?>
<script type="text/javascript">
	$(document).ready(function(){
		getPerusahaanList();
		function getPerusahaanList() {
			$.ajax({
				url : "<?php echo base_url('master/perusahaan/getperusahaanlist') ?>",
				method : "POST",
				async : true,
				dataType : 'json',
				success: function(data){
					var html = '<option value="all">Semua Perusahaan</option>';
					var i;
					for(i=0; i<data.length; i++){
						html += '<option value='+data[i].id+'>'+data[i].nama+'</option>';
					}
					$('#perusahaanId').html(html);
				}
			});
		}

		getStokList();
		function getStokList() {
			$.ajax({
				type  : 'POST',
				url   : '<?php echo base_url('laporan/stok/getstoklist')?>',
				async : true,
				dataType : 'json',
				data : {
					perusahaanId: null
				},
				success : function(data){
					var html = '';
					var i;
					if (data.length > 0) {
						var index = 1;
						for(i=0; i<data.length; i++){
							html += '<tr>'+
								'<td>'+ (index++) +'</td>'+
								'<td>'+ data[i].barang.nama_barang +'</td>'+
								'<td>'+ data[i].perusahaan.nama +'</td>'+
								'<td>'+ data[i].qty +'</td>'+
							'</tr>';
						}
					}
					
					$('#showData').html(html);
				}
			});
		}

		$('#perusahaanId').change(function(){ 
			var id = $(this).val();
			if (id == 'all') {
				id = null;
			}
			$.ajax({
				type  : 'POST',
				url   : '<?php echo base_url('laporan/stok/getstoklist')?>',
				async : true,
				dataType : 'json',
				data : {
					perusahaanId: id
				},
				success : function(data){
					var html = '';
					var i;
					if (data.length > 0) {
						var index = 1;
						for(i=0; i<data.length; i++){
							html += '<tr>'+
								'<td>'+ (index++) +'</td>'+
								'<td>'+ data[i].barang.nama_barang +'</td>'+
								'<td>'+ data[i].perusahaan.nama +'</td>'+
								'<td>'+ data[i].qty +'</td>'+
							'</tr>';
						}
					}
					
					$('#showData').html(html);
				}
			});
			return false;
		});

	});
</script>
