<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $file_pdf ?></title>
    <style>
      #table {
        font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
        border-collapse: collapse;
        width: 100%;
      }

      #table td, #table th {
        border: 1px solid #ddd;
        padding: 8px;
      }

      #table tr:nth-child(even){background-color: #f2f2f2;}

      #table tr:hover {background-color: #ddd;}

      #table th {
        padding-top: 10px;
        padding-bottom: 10px;
        text-align: left;
        background-color: #4CAF50;
        color: white;
      }
    </style>
  </head>
  <body>
    <div style="text-align:center">
        <h3> Laporan Data Stok </h3>
    </div>
    <table id="table">
      <thead>
				<tr>
					<th style="text-align: center;">No</th>
					<th style="text-align: center;">Nama Perusahaan</th>
					<th style="text-align: center;">Nama Barang</th>
					<th style="text-align: center;">QTY</th>
			</tr>
      </thead>
        <tbody>
					<?php if (!empty($stokList)) {
						$no = 1;
						foreach ($stokList as $stok) {
							?>
							<tr>
								<td><?php echo $no++ ?></td>
								<td><?php echo $stok->perusahaan->nama ?></td>
								<td><?php echo $stok->barang->nama_barang ?></td>
								<td><?php echo $stok->qty ?></td>
							</tr>
							<?php
						}
					} ?>
        </tbody>
    </table>
  </body>
</html>
