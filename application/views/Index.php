<?php 
  $this->load->view('base_template/head');
  $this->load->view('base_template/sidebar', $authorization);
	$this->load->view('modaldetail');
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Beranda</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Beranda</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Info boxes -->
        <div class="row">
          <div class="col-12 col-sm-6 col-md-3">
            <div class="info-box">
              <span class="info-box-icon bg-info elevation-1"><i class="fas fa-cog"></i></span>
              <div class="info-box-content">
                <span class="info-box-text">Transaksi masuk hari ini</span>
                <span class="info-box-number">10 Transaksi</span>
                <center><a href="#" class="small-box-footer">More info<i class="fas fa-arrow-circle-right"></i></a></center>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
          <div class="col-12 col-sm-6 col-md-3">
            <div class="info-box mb-3">
              <span class="info-box-icon bg-danger elevation-1"><i class="fas fa-thumbs-up"></i></span>
              <div class="info-box-content">
                <span class="info-box-text">transaksi keluar hari ini</span>
                <span class="info-box-number">10 Transaksi</span>
                <center><a href="#" class="small-box-footer">More info<i class="fas fa-arrow-circle-right"></i></a></center>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->

          <!-- fix for small devices only -->
          <div class="clearfix hidden-md-up"></div>

          <div class="col-12 col-sm-6 col-md-3">
            <div class="info-box mb-3">
              <span class="info-box-icon bg-success elevation-1"><i class="fas fa-shopping-cart"></i></span>
              <div class="info-box-content">
                <span class="info-box-text">Penjualan terlaris</span>
                <span class="info-box-number">760 Items</span>
                <center><a href="#" class="small-box-footer">More info<i class="fas fa-arrow-circle-right"></i></a></center>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
          <div class="col-12 col-sm-6 col-md-3">
            <div class="info-box mb-3">
              <span class="info-box-icon bg-warning elevation-1"><i class="fas fa-users"></i></span>
              <div class="info-box-content">
                <span class="info-box-text">Barang yang dihapus</span>
                <span class="info-box-number">2,000 Items</span>
                <center><a href="#" class="small-box-footer">More info<i class="fas fa-arrow-circle-right"></i></a></center>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Permintaan & Retur Produk</h3>
              </div>
							<input type="hidden" class="form-control" id="idPerusahaan" name="idPerusahaan" value="<?php echo $authorization->user->karyawan->perusahaan->id ?>" />
							<input type="hidden" class="form-control" id="levelPerusahaan" name="levelPerusahaan" value="<?php echo $authorization->user->karyawan->perusahaan->level ?>" />
							<input type="hidden" class="form-control" id="userId" name="userId" value="<?php echo $authorization->user->id ?>" />
              <!-- /.card-header -->
              <div class="card-body">
                <table id="dataTable" class="table table-bordered table-hover">
                  <thead>
                    <tr>
                      <th style="text-align: center;">No</th>
                      <th style="text-align: center;">Tanggal Transaksi</th>
                      <th style="text-align: center;">Keterangan</th>
                      <th style="text-align: center;">Status</th>
											<th style="width: 120px; text-align: center;">Aksi</th>
                    </tr>
                  </thead>
                  <tbody id="showData">
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <button type="submit" class="btn btn-primary swalDefaultSuccess" style="visibility: hidden">success</button>

<?php
  $this->load->view('base_template/footer');
  $this->load->view('base_template/script');

  if ($this->session->flashdata('success')) {
    echo $this->session->flashdata('success');
    unset($_SESSION['success']);
  }
?>
<script type="text/javascript">
	$(document).ready(function(){
		showTempList();
		function showTempList(){
			var perusahaanId = $('#idPerusahaan').val();
			var status = "<?php echo StatusTransaksiConstant::WAITING_APPROVED ?>";
			var level = $('#levelPerusahaan').val();
			var userId = $('#userId').val();
			$.ajax({
				type  : 'POST',
				url   : '<?php echo base_url('transaksi/histori/gethistoritransaksi')?>',
				async : true,
				dataType : 'json',
				data : {
					perusahaanId: perusahaanId,
					status: status,
					level: level,
					userId: userId,
					jenisTransaksi : null
				},
				success : function(data) {
					var html = '';
					var i;
					var transaksiList = data.historiTransaksiList;
					if (transaksiList.length > 0) {
						var index = 1;
						if (level == '<?php echo LevelPerusahaanConstant::PUSAT ?>') {
							for(i=0; i<transaksiList.length; i++){
								html += '<tr>'+
									'<td>'+ (index++) +'</td>'+
									'<td>'+ transaksiList[i].tgl_transaksi +'</td>'+
									'<td>'+ transaksiList[i].keterangan +'</td>'+
									'<td>'+ transaksiList[i].status +'</td>'+
									'<td>'+
										'<div class="row">'+
											'<div class="col-6">'+
												'<button type="button" class="btn btn-block bg-gradient-warning btn btn-sm" data-toggle="modal" data-id="'+ transaksiList[i].id +'" data-target="#modalDetail">Detail</button>'+
											'</div>'+
											'<div class="col-6">'+ 
												'<button type="button" class="btn btn-block bg-gradient-success btn btn-sm proses" data-id="'+ transaksiList[i].id +'">Proses</button>'+
											'</div>'+
										'</div>'+
									'</td>'+
								'</tr>';
							}
						} else {
							for(i=0; i<transaksiList.length; i++){
								html += '<tr>'+
									'<td>'+ (index++) +'</td>'+
									'<td>'+ transaksiList[i].tgl_transaksi +'</td>'+
									'<td>'+ transaksiList[i].keterangan +'</td>'+
									'<td>'+ transaksiList[i].status +'</td>'+
									'<td>'+
										'<div class="row">'+
											'<button type="button" class="btn btn-block bg-gradient-warning btn btn-sm" data-toggle="modal" data-target="#modalDetail'+ transaksiList[i].id +'">Detail</button>'+
										'</div>'+
									'</td>'+
								'</tr>';
							}
						}
					}
					
					$('#showData').html(html);
				}
			});
		}

		$('#dataTable').on('click','.proses',function(){
			var id = $(this).data('id');
			$.ajax({
				type : "POST",
				url  : "<?php echo base_url(BaseUrl::DEFAULT_TRANSAKSI_URL.'/update') ?>",
				data : {
					id: id
				},
				dataType : 'json',
				success: function(data){
					showTempList();
				}
			});
		});

	});

	$('#modalDetail').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var transaksiId = button.data('id');
        var modal = $(this);
        modal.find('#id').val(transaksiId);
    })
</script>
