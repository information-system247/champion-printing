<!-- modal Pengaturan -->
<div class="modal fade" id="modalAdd">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header bg-info">
        <h4 class="modal-title">Tambah Kategori Barang</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form id="formKategori" method="post" action="<?php echo base_url('master/kategoribarang/store') ?>">
        <div class="modal-body">
          <input type="text" id="id" name="id" hidden>
          <div class="form-group">
            <label for="namaKategori">Nama Kategori Barang</label>
            <input type="text" class="form-control" id="namaKategori" name="namaKategori" placeholder="masukan nama kategori barang" onkeypress="return event.charCode >= 65 && event.charCode <= 90 || event.charCode >= 97 && event.charCode <= 122 || event.charCode == 46 || event.charCode == 39 || event.charCode == 32 || event.charCode == 96" maxlength="50" required>
          </div>
        </div>
        <!-- /.card-body -->
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
          <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
      </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->