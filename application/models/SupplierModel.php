<?php 

class SupplierModel extends CI_Model
{
  public function findAll()
  {
    return $this->db->get('supplier')->result();
  }

  public function save($data)
  {
    return $this->db->insert('supplier', $data);
  }

  public function update($id, $namaSupplier, $alamat, $noHp, $username)
  {
    $this->db->set('nama_supplier', $namaSupplier);
		$this->db->set('alamat', $alamat);
		$this->db->set('no_hp', $noHp);
		$this->db->set('update_at', DateUtils::getDateNow());
		$this->db->set('update_by', $username);
		$this->db->where('id', $id);
    return $this->db->update('supplier');
  }

  public function delete($id)
  {
    return $this->db->delete('supplier', array('id' => $id));
  }

  public function findById($id)
  {
    return $this->db->get_where('supplier', array('id' => $id))->result();
  }

	public function findByIdIn($ids)
	{
		$this->db->from('supplier');
    return $this->db->where_in('id', $ids)->get()->result();
	}
}
