<?php 

class KonsumenModel extends CI_Model
{
  public function findAll()
  {
    return $this->db->get('konsumen')->result();
  }

  public function findById($id)
  {
    return $this->db->get_where('konsumen', array('id' => $id))->result();
  }

  public function save($data)
  {
    return $this->db->insert('konsumen', $data);
  }

  public function update($id, $kategori, $namaKonsumen, $username, $alamat = null, $telepon = null)
  {
		if ($alamat != null) {
			$this->db->set('alamat', $alamat);
		}
		
		if ($telepon != null) {
			$this->db->set('telepon', $telepon);
		}

    $this->db->set('kategori', $kategori);
		$this->db->set('nama_konsumen', $namaKonsumen);
		$this->db->set('update_at', DateUtils::getDateNow());
		$this->db->set('update_by', $username);
		$this->db->where('id', $id);
    return $this->db->update('konsumen');
  }

  public function delete($id)
  {
    return $this->db->delete('konsumen', array('id' => $id));
  }

  public function findByKategori($kategori)
  {
    return $this->db->get_where('konsumen', array('kategori' => $kategori))->result();
  }

	public function findByIdIn($ids)
	{
		$this->db->from('konsumen');
    return $this->db->where_in('id', $ids)->get()->result();
	}
}
