<?php

class Utils extends CI_Model
{
	public function validateSuperAdmin($user)
	{
		$result = false;
		$karyawan = null;
		if (!empty($user)) {
			$karyawan = $this->db->get_where('karyawan', array('id' => $user->id_karyawan))->result();
		}
		
		$perusahaan = null;
		if (!empty($karyawan)) {
      $perusahaan = $this->db->get_where('perusahaan', array('id' => $karyawan->id_perusahaan))->result();
		}

		if (LevelPerusahaanConstant::PUSAT == $perusahaan->level) {
			$result = true;
		}

		return $result;
	}

  public function getUserProfile()
  {
    if (LoginSessionConstant::ACTIVE == $this->session->userdata('status')) {
			$user = $this->db->get_where('user', array('id' => $this->session->userdata('id')))->result();
			$user = $this->getUserJoinKaryawan($user);
			$data = $this->getUserJoinPerusahaanAndPosisiKaryawan($user);
			return $data;
    }

    redirect('login');
  }

	public function getUserJoinPerusahaanAndPosisiKaryawan($user)
	{
		$result = array();
		if (!empty($user)) {
			$karyawanOld = $user->karyawan;
			$idPerusahaan = $karyawanOld->id_perusahaan;
			$perusahaan = $this->db->get_where("perusahaan", array("id" => $idPerusahaan))->result();
			$karyawanNew = array();
			if (!empty($perusahaan)) {
				$karyawanNew = array(
					"id" => $karyawanOld->id,
					"nama" => $karyawanOld->nama,
					"alamat" => $karyawanOld->alamat,
					"perusahaan" => $perusahaan[0],
					"telepon" => $karyawanOld->telepon
				);
			}

			$idPosisi = $karyawanOld->id_posisi;
			$posisi = $this->db->get_where("posisi_karyawan", array("id" => $idPosisi))->result();
			$posisiNew = array();
			if (!empty($posisi)) {
				$posisiNew = array(
					"posisi" => $posisi[0]
				);
			}

			$karyawanNew = array_merge($karyawanNew, $posisiNew);
			$result = array(
				"id" => $user->id,
				"karyawan" => $karyawanNew,
				"username" => $user->username,
				"password" => $user->password
			);
		}

		return $this->convertListToJson($result);
	}

	public function getUserJoinKaryawan($user)
	{
		$result = array();
		if (!empty($user)) {
			$user = $user[0];
			$karyawan = $this->db->get_where("karyawan", array("id" => $user->id_karyawan))->result();
			if (!empty($karyawan)) {
				$result = array(
					"id" => $user->id,
					"karyawan" => $karyawan[0],
					"username" => $user->username,
					"password" => $user->password
				);
			}
		}

		return $this->convertListToJson($result);
	}

	public function response($menu, $data = null)
	{
		$defaultData = array(
			"activeMenu" => $menu,
			"user" => $this->getUserProfile()
		);

		$defaultData = array(
			"authorization" => $defaultData
		);

		$result = $defaultData;
		if (!empty($data)) {
			$data = (array) $data;
			$result = array_merge($defaultData, $data);
		}

		return $this->convertListToJson($result);
	}

	public function convertListToJson($data)
	{
		$result = array();
		if (!empty($data)) {
			$result = json_decode(json_encode($data), FALSE);
		}

		return $result;
	}

	public function storeDataValidation($result, $isDeleted = false)
	{
		$key = "failed";
		$message = null;
		if ($isDeleted == true) {
			if ($result) {
				$key = "success";
				$message = "Berhasil hapus data";
			} else {
				$message = "Gagal hapus data";
			}
		} else {
			if ($result) {
				$key = "success";
        $message = "Berhasil simpan data";
			} else {
				$message = "Gagal simpan data";
			}
		}

		if ("success" == $key) {
			$this->session->set_flashdata($key, FlashData::success($message));
		} else {
			$this->session->set_flashdata($key, FlashData::generalError($message));
		}
	}
}
