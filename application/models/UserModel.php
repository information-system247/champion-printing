<?php
class UserModel extends CI_Model
{
  public function findByIdKaryawan($idKaryawan)
  {
    return $this->db->get_where('user', array('id_karyawan' => $idKaryawan))->result();
  }

  public function findByLevel($level)
  {
    return $this->db->get_where('user', array('level' => $level))->result();
  }

  public function findAll()
  {
    return $this->db->get('user')->result();
  }

  public function findByUsernameAndPassword($username, $password)
  {
    $data = array(
      'username' => $username,
      'password' => $password
    );

    return $this->db->get_where('user', $data)->result();
  }

  public function findByUsername($username)
  {
    return $this->db->get_where('user', array('username' => $username))->result();
  }

  public function findById($id)
  {
    return $this->db->get_where('user', array('id' => $id))->result();
  }

	public function findByIdIn($ids)
	{
		$this->db->from('user');
    return $this->db->where_in('id', $ids)->get()->result();
	}

  public function findByIdKaryawanIn($karyawanIdList)
  {
    $this->db->from('user');
    return $this->db->where_in('id_karyawan', $karyawanIdList)->get()->result();
  }

  public function findByIdAndKaryawanId($id, $karyawanId)
  {
    $data = array(
      'id' => $id,
      'id_karyawan' => $karyawanId
    );
    return $this->db->get_where('user', $data)->result();
  }

  public function delete($id)
  {
    return $this->db->delete('user', array('id' => $id));
  }
	
	public function save($data)
	{
		return $this->db->insert('user', $data);
	}

	public function update($data)
	{
		return $this->db->replace('user', $data);
	}
}
