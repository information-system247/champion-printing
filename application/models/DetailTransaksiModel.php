<?php

class DetailTransaksiModel extends CI_Model
{
	public function save($data)
	{
		return $this->db->insert('detail_transaksi', $data);
	}

	public function findByTransaksiId($transaksiId)
	{
		return $this->db->get_where('detail_transaksi', array('id_transaksi' => $transaksiId))->result();
	}

	public function findByTransaksiIdIn($transaksiIds)
	{
		$this->db->from('detail_transaksi');
    return $this->db->where_in('id_transaksi', $transaksiIds)->get()->result();
	}
}
