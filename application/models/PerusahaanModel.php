<?php
class PerusahaanModel extends CI_Model
{
  public function findAll()
  {
    return $this->db->get("perusahaan")->result();
  }

  public function findById($id)
  {
    return $this->db->get_where('perusahaan', array('id' => $id))->result();
  }

  public function save($data)
  {
    return $this->db->insert('perusahaan', $data);
  }

  public function update($id, $namaPerusahaan, $alamat, $telepon, $level)
  {
		$this->db->set('nama', $namaPerusahaan);
		$this->db->set('alamat', $alamat);
		$this->db->set('telepon', $telepon);
		$this->db->set('level', $level);
		$this->db->where('id', $id);
    return $this->db->update('perusahaan');
  }

  public function delete($id)
  {
    return $this->db->delete('perusahaan', array('id' => $id));
  }

  public function findByLevel($level)
  {
    return $this->db->get_where('perusahaan', array('level' => $level))->result();
  }

  public function findByIdIn($idList)
  {
    $this->db->from('perusahaan');
    return $this->db->where_in('id', $idList)->get()->result();
  }

	public function findByIdNot($id)
  {
    return $this->db->get_where('perusahaan', array('id !=' => $id))->result();
  }
}
