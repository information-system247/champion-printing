<?php

class TransactionHistoryModel extends CI_Model
{
	public function findByCategoryTransaction($categoryTransaction)
	{
		return $this->db->get_where('histori_transaksi', array('kategori_transaksi' => $categoryTransaction))->result();
	}

	public function findByCategoryTransactionAndTransactionType($categoryTransaction, $transactionType)
	{
		$data = array(
			'kategori_transaksi' => $categoryTransaction,
			'tipe_transaksi' => $transactionType
		);

		return $this->db->get_where('histori_transaksi', $data)->result();
	}

	public function findByCategoryTransactionAndTransactionTypeAndUserId($categoryTransaction, $transactionType, $userId)
	{
		$data = array(
			'kategori_transaksi' => $categoryTransaction,
			'tipe_transaksi' => $transactionType,
			'id_user' => $userId
		);

		return $this->db->get_where('histori_transaksi', $data)->result();
	}

	public function findByCategoryTransactionAndTransactionTypeNot($categoryTransaction, $transactionType)
	{
		$data = array(
			'kategori_transaksi' => $categoryTransaction,
			'tipe_transaksi !=' => $transactionType
		);

		return $this->db->get_where('histori_transaksi', $data)->result();
	}

	public function findByCategoryTransactionAndTransactionTypeAndUserIdAndSupplierIdLimit1($categoryTransaction, $transactionType, $userId, $supplierId)
	{
		$this->db->from('histori_transaksi');
		$this->db->where('kategori_transaksi', $categoryTransaction);
		$this->db->where('tipe_transaksi', $transactionType);
		$this->db->where('id_user', $userId);
		$this->db->where('id_supplier', $supplierId);
		$this->db->order_by('id', 'DESC');
		$this->db->limit(1);
		return $this->db->get()->result();
	}

	public function findByCategoryTransactionAndTransactionTypeAndUserIdAndPerusahaanIdLimit1($categoryTransaction, $transactionType, $userId, $perusahaanId)
	{
		$this->db->from('histori_transaksi');
		$this->db->where('kategori_transaksi', $categoryTransaction);
		$this->db->where('tipe_transaksi', $transactionType);
		$this->db->where('id_user', $userId);
		$this->db->where('id_perusahaan', $perusahaanId);
		$this->db->order_by('id', 'DESC');
		$this->db->limit(1);
		return $this->db->get()->result();
	}

	public function findByCategoryTransactionAndTransactionTypeAndUserIdAndKonsumenIdLimit1($categoryTransaction, $transactionType, $userId, $konsumenId)
	{
		$this->db->from('histori_transaksi');
		$this->db->where('kategori_transaksi', $categoryTransaction);
		$this->db->where('tipe_transaksi', $transactionType);
		$this->db->where('id_user', $userId);
		$this->db->where('id_konsumen', $konsumenId);
		$this->db->order_by('id', 'DESC');
		$this->db->limit(1);
		return $this->db->get()->result();
	}

	public function findByPerusahaanIdAndStatus($perusahaanId, $status)
	{
		$this->db->from('histori_transaksi');
		$this->db->where('id_perusahaan', $perusahaanId);
		$this->db->where('status', $status);
		return $this->db->get()->result();
	}

	public function findByStatus($status)
	{
		$this->db->from('histori_transaksi');
		$this->db->where('status', $status);
		return $this->db->get()->result();
	}

	public function findByPerusahaanId($perusahaanId)
	{
		$this->db->from('histori_transaksi');
		$this->db->where('id_perusahaan', $perusahaanId);
		return $this->db->get()->result();
	}

	public function save($data)
	{
		return $this->db->insert('histori_transaksi', $data);
	}

	public function findById($id)
	{
		return $this->db->get_where('histori_transaksi', array('id' => $id))->result();
	}

	public function update($id)
	{
		$this->db->set('status', StatusTransaksiConstant::APPROVED);
		$this->db->where('id', $id);
		return $this->db->update('histori_transaksi');
	}

	public function findByUserIdAndStatus($userId, $status)
	{
		$this->db->from('histori_transaksi');
		$this->db->where('id_user', $userId);
		$this->db->where('status', $status);
		return $this->db->get()->result();
	}

	public function findByUserId($userId)
	{
		$this->db->from('histori_transaksi');
		$this->db->where('id_user', $userId);
		return $this->db->get()->result();
	}
}
