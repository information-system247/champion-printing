<?php 

class BarangModel extends CI_Model
{
  public function findByIdSupplier($supplierId)
  {
    return $this->db->get_where('barang', array('id_supplier' => $supplierId))->result();
  }

  public function findById($id)
  {
    return $this->db->get_where('barang', array('id' => $id))->result();
  }

  public function findAll()
  {
    return $this->db->get_where('barang', array('is_deleted' => false))->result();
  }

  public function save($data)
  {
    return $this->db->insert('barang', $data);
  }

  public function update($id, $idKategori, $idTipe, $namaBarang, $skDiskon, $diskon, $hargaJual, $idSupplier, $satuan, $isDeleted)
  {
    $this->db->set('id_kategori', $idKategori);
		$this->db->set('id_tipe', $idTipe);
		$this->db->set('nama_barang', $namaBarang);
		$this->db->set('sk_diskon', $skDiskon);
		$this->db->set('diskon', $diskon);
		$this->db->set('harga_jual', $hargaJual);
		$this->db->set('id_supplier', $idSupplier);
		$this->db->set('satuan', $satuan);
		$this->db->set('is_deleted', $isDeleted);
		$this->db->where('id', $id);
    return $this->db->update('barang');
  }

  public function delete($id)
  {
    return $this->db->delete('barang', array('id' => $id))->result();
  }

	public function findByIdIn($idList)
	{
		$this->db->from('barang');
    return $this->db->where_in('id', $idList)->get()->result();
	}
}
