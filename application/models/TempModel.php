<?php 

class TempModel extends CI_Model
{
  public function findAll()
  {
    return $this->db->get('temp')->result();
  }

  public function save($data)
  {
    return $this->db->insert('temp', $data);
  }

  public function update($data)
  {
    return $this->db->replace('temp', $data);
  }

  public function delete($id)
  {
    return $this->db->delete('temp', array('id' => $id));
  }

	public function deleteByUserIdAndKategoriTransaksiAndTipeTransaksi($userId, $kategoriTransaksi, $tipeTransaksi)
	{
		$data = array(
			'id_user' => $userId,
			'tipe_transaksi' => $tipeTransaksi,
			'kategori_transaksi' => $kategoriTransaksi
		);
		return $this->db->delete('temp', $data);
	}

  public function findById($id)
  {
    return $this->db->get_where('temp', array('id' => $id))->result();
  }

	public function deleteByUserId($userId)
	{
		return $this->db->delete('temp', array('id_user' => $userId));
	}

	public function findByUserId($userId)
	{
		return $this->db->get_where('temp', array('id_user' => $userId))->result();
	}

	public function findByUserIdAndKategoriTransaksiAndTipeTransaksi($userId, $kategoriTransaksi, $tipeTransaksi)
	{
		$data = array(
			'id_user' => $userId,
			'tipe_transaksi' => $tipeTransaksi,
			'kategori_transaksi' => $kategoriTransaksi
		);

		return $this->db->get_where('temp', $data)->result();
	}
}
