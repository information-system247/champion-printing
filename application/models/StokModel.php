<?php

class StokModel extends CI_Model
{
	public function findByPerusahaanId($perusahaanId)
	{
		return $this->db->get_where('stok', array('id_perusahaan' => $perusahaanId))->result();
	}

	public function save($data)
	{
		return $this->db->insert('stok', $data);
	}

	public function findByPerusahaanIdAndBarangId($perusahaanId, $barangId)
	{
		$data = array(
			'id_perusahaan' => $perusahaanId,
			'id_barang' => $barangId
		);

		return $this->db->get_where('stok', $data)->result();
	}

	public function update($data)
  {
    return $this->db->replace('stok', $data);
  }

	public function findAll()
	{
		return $this->db->get('stok')->result();
	}
}
