<?php
class KaryawanModel extends CI_Model
{
  public function findByPerusahaanId($perusahaanId)
  {
    return $this->db->get_where('karyawan', array('id_perusahaan' => $perusahaanId))->result();
  }

  public function findAll()
  {
    return $this->db->get('karyawan')->result();
  }

  public function save($data)
  {
    return $this->db->insert('karyawan', $data);
  }

  public function update($id, $nama, $alamat, $idPerusahaan, $telepon, $idPosisi)
  {
		$this->db->set('nama', $nama);
		$this->db->set('alamat', $alamat);
		$this->db->set('id_perusahaan', $idPerusahaan);
		$this->db->set('telepon', $telepon);
		$this->db->set('id_posisi', $idPosisi);
		$this->db->where('id', $id);
    return $this->db->update('karyawan');
  }

  public function delete($id)
  {
    return $this->db->delete('karyawan', array('id' => $id));
  }

  public function findByidPerusahaan($idPerusahaan)
  {
    return $this->db->get_where('karyawan', array('id_perusahaan' => $idPerusahaan))->result();
  }

  public function findByIdIn($idList)
  {
    $this->db->from('karyawan');
    return $this->db->where_in('id', $idList)->get()->result();
  }

  public function findById($id)
  {
    return $this->db->get_where('karyawan', array('id' => $id))->result();
  }

  public function findAllGroupByIdPerusahaan()
  {
    $this->db->from('karyawan');
    return $this->db->group_by('id_perusahaan')->result();
  }

  public function findByIdNotIn($idList)
  {
    $this->db->from('karyawan');
    return $this->db->where_not_in('id', $idList)->get()->result();
  }

  public function findByPerusahaanIdAndPosisiId($perusahaanId, $posisiId)
  {
    $data = array(
      'id_perusahaan' => $perusahaanId,
      'id_posisi' => $posisiId
    );

    return $this->db->get_where('karyawan', $data)->result();
  }
}
