<?php 

class PosisiKaryawanModel extends CI_Model
{
  public function findAll()
  {
    return $this->db->get('posisi_karyawan')->result();
  }

  public function findById($id)
  {
    return $this->db->get_where('posisi_karyawan', array('id' => $id))->result();
  }

  public function findByNama($nama)
  {
		$this->db->from('karyawan');
    return $this->db->like('nama', $nama)->get()->result();
  }
}
