<?php 

class TipeBarangModel extends CI_Model
{
  public function findAll()
  {
    return $this->db->get('tipe_barang')->result();
  }

  public function findById($id)
  {
    return $this->db->get_where('tipe_barang', array('id' => $id))->result();
  }

  public function save($data)
  {
    return $this->db->insert('tipe_barang', $data);
  }

  public function update($id, $namaTipe)
  {
    $this->db->set('nama_tipe', $namaTipe);
		$this->db->where('id', $id);
    return $this->db->update('tipe_barang');
  }

  public function delete($id)
  {
    return $this->db->delete('tipe_barang', array('id' => $id));
  }
}
