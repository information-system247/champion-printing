<?php 

class KategoriBarangModel extends CI_Model
{
  public function findAll()
  {
    return $this->db->get('kategori_barang')->result();
  }

  public function findById($id)
  {
    return $this->db->get_where('kategori_barang', array('id' => $id))->result();
  }

  public function save($data)
  {
    return $this->db->insert('kategori_barang', $data);
  }

  public function update($id, $namaKategori)
  {
    $this->db->set('nama_kategori', $namaKategori);
		$this->db->where('id', $id);
    return $this->db->update('kategori_barang');
  }

  public function delete($id)
  {
    return $this->db->delete('kategori_barang', array('id' => $id));
  }
}
