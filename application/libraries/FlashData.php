<?php

class FlashData
{
  public static function success($message)
  {
    return "
      <script>
        $(function() {
            var Toast = Swal.mixin({
              toast: true,
              position: 'top-end',
              showConfirmButton: false,
              timer: 5000
            });

            $('.swalDefaultSuccess').click(function() {
                Toast.fire({
                  icon: 'success',
                  title: '". $message ."'
                })
            });
            $('.swalDefaultSuccess').trigger('click');
        });
      </script>";
  }

  public static function generalError($message)
  {
    return "
      <script>
        $(function() {
            var Toast = Swal.mixin({
              toast: true,
              position: 'top-end',
              showConfirmButton: false,
              timer: 5000
            });

            $('.swalDefaultError').click(function() {
                Toast.fire({
                  icon: 'error',
                  title: '". $message ."'
                })
            });
            $('.swalDefaultError').trigger('click');
        });
      </script>";
  }

  public static function warning($message)
  {
    return "
      <script>
        $(function() {
            var Toast = Swal.mixin({
              toast: true,
              position: 'top-end',
              showConfirmButton: false,
              timer: 5000
            });

            $('.swalDefaultWarning').click(function() {
                Toast.fire({
                  icon: 'warning',
                  title: '". $message ."'
                })
            });
            $('.swalDefaultWarning').trigger('click');
        });
      </script>";
  }
}