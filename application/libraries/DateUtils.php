<?php 

class DateUtils
{
  public static function getDateNow()
  {
    return date("Y-m-d h:i:s");
  }
}
