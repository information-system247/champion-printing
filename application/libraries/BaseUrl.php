<?php
class BaseUrl {
	const ASSERT_URL = "assert/template";
	const DEFAULT_BARANG_URL = "master/barang";
	const DEFAULT_KARYAWAN_URL = "master/karyawan";
	const DEFAULT_KATEGORI_BARANG_URL = "master/kategoribarang";
	const DEFAULT_KONSUMEN_PERUSAHAAN_URL = "master/konsumen/perusahaan";
	const DEFAULT_KONSUMEN_CUSTOMER_URL = "master/konsumen/customer";
	const DEFAULT_PERUSAHAAN_URL = "master/perusahaan";
	const DEFAULT_SUPPLIER_URL = "master/supplier";
	const DEFAULT_TIPE_BARANG_URL = "master/tipebarang";
	const DEFAULT_USER_URL = "master/user";
	const DEFAULT_TRANSAKSI_MASUK_SUPPLIER_URL = "transaksi/masuk/supplier";
	const DEFAULT_TRANSAKSI_MASUK_RETUR_URL = "transaksi/masuk/retur";
	const DEFAULT_TRANSAKSI_MASUK_CABANG_URL = "transaksi/masuk/cabang";
	const DEFAULT_TRANSAKSI_KELUAR_CABANG_URL = "transaksi/keluar/cabang";
	const DEFAULT_TRANSAKSI_KELUAR_KONSUMEN_URL = "transaksi/keluar/konsumen";
	const DEFAULT_TRANSAKSI_KELUAR_RETUR_URL = "transaksi/keluar/retur";
	const DEFAULT_TRANSAKSI_URL = "transaksi/histori";
	const DEFAULT_STOK_URL = "master/stok";
	const DEFAULT_LAPORAN_KARYAWAN = "laporan/karyawan";
	const DEFAULT_LAPORAN_SUPPLIER = "laporan/supplier";
	const DEFAULT_LAPORAN_KONSUMEN = "laporan/konsumen";
	const DEFAULT_LAPORAN_STOK = "laporan/stok";
	const DEFAULT_LAPORAN_TRANSAKSI_MASUK = "laporan/transaksi/masuk";
	const DEFAULT_LAPORAN_TRANSAKSI_KELUAR = "laporan/transaksi/keluar";
	const DEFAULT_EXPORT_PDF = "export/pdf";
}
