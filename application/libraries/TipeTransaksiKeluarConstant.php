<?php

class tipeTransaksiKeluarConstant {
	const TRANS_KELUAR_PUSAT_ALL = "all";
	const TRANS_KELUAR_PUSAT_KE_CABANG = "cabang";
	const TRANS_KELUAR_PUSAT_ECER = "ecer";
	const TRANS_KELUAR_PUSAT_PAK = "pak";
	const TRANS_KELUAR_PUSAT_KE_KONSUMEN = "konsumen";

	const TRANS_KELUAR_CABANG_ALL = "all";
	const TRANS_KELUAR_ECER = "ecer";
	const TRANS_KELUAR_CABANG_PAK = "pak";
	const TRANS_KELUAR_CABANG_RETUR = "retur";
}
