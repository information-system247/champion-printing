<?php

class TipeTransaksiMasukConstant {
	const TRANS_MASUK_PUSAT_ALL = "all";
	const TRANS_MASUK_PUSAT_DARI_SUPPLIER = "supplier";
	const TRANS_MASUK_PUSAT_DARI_RETUR = "retur";

	const TRANS_MASUK_CABANG_ALL = "cabang";
	const TRANS_MASUK_CABANG_DARI_PUSAT = "pusat";
}
