<?php
class MenuConstant
{
  const BERANDA = "Beranda";
  const PERUSAHAAN = "Perusahaan";
  const KARYAWAN = "Karyawan";
  const USER = "User";
  const SUPPLIER = "Supplier";
  const KONSUMEN_CUSTOMER = "Konsumen Customer";
  const KONSUMEN_PERUSAHAAN = "Konsumen Perusahaan";
  const KATEGORI_BARANG = "Kategori Barang";
  const TIPE_BARANG = "Tipe Barang";
  const BARANG = "Barang";
	const TRANSAKSI_MASUK_SUPPLIER = "Transaksi Masuk supplier";
	const TRANSAKSI_MASUK_RETUR = "Transaksi Masuk retur";
	const TRANSAKSI_MASUK_CABANG = "Transaksi Masuk cabang";
	const TRANSAKSI_KELUAR_PUSAT_CABANG = "Transaksi Keluar cabang";
	const TRANSAKSI_KELUAR_KONSUMEN = "Transaksi Keluar konsumen";
	const TRANSAKSI_KELUAR_RETUR = "Transaksi Keluar retur";
	const STOK = "Stok";
	const LAPORAN_KARYAWAN = "Laporan Karyawan";
	const LAPORAN_SUPPLIER = "Laporan Supplier";
	const LAPORAN_KONSUMEN = "Laporan Konsumen";
	const LAPORAN_STOK = "Laporan Stok";
	const LAPORAN_TRANSAKSI_MASUK_SUPPLIER = "Laporan Transaksi Masuk Supplier";
	const LAPORAN_TRANSAKSI_MASUK_RETUR = "Laporan Transaksi Masuk Retur";
	const LAPORAN_TRANSAKSI_MASUK_PUSAT = "Laporan Transaksi Masuk Pusat";
	const LAPORAN_TRANSAKSI_KELUAR_CABANG = "Laporan Transaksi Masuk Cabang";
	const LAPORAN_TRANSAKSI_KELUAR_KONSUMEN = "Laporan Transaksi Masuk Konsumen";
}
