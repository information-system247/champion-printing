<?php 

class Stok extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('UserModel','user');
		$this->load->model('Utils', 'utils');
		$this->load->model('StokModel','stok');
		$this->load->model('BarangModel','barang');
		$this->load->model('PerusahaanModel','perusahaan');
  }

	public function index()
	{
		$userOptional = $this->utils->getUserProfile();
		$data = $this->utils->response(MenuConstant::LAPORAN_STOK);
    $this->load->view('laporan/stok/list', $data);
	}

	public function getStokList()
	{
		$perusahaanId = $this->input->post('perusahaanId');
		$stokList = array();
		if (empty($perusahaanId)) {
			$stokList = $this->stok->findAll();
		} else {
			$stokList = $this->stok->findByPerusahaanId($perusahaanId);
		}

		$stokList = $this->getStokJoinPerusahaan($stokList);
		$stokList = $this->getStokJoinBarang($stokList);
		echo json_encode($stokList);
	}

	public function getStokJoinPerusahaan($stokList)
	{
		$perusahaanIds = [];
		if (!empty($stokList)) {
			foreach ($stokList as $stok) {
				$perusahaanIds = $stok->id_perusahaan;
			}
		}

		$perusahaanList = array();
		if (!empty($perusahaanIds)) {
			$perusahaanList = $this->perusahaan->findByIdIn($perusahaanIds);
		}

		$result = [];
		if (!empty($perusahaanList)) {
			foreach ($stokList as $stok) {
				foreach ($perusahaanList as $perusahaan) {
					if ($stok->id_perusahaan == $perusahaan->id) {
						$data = array(
							'id' => $stok->id,
							'perusahaan' => $perusahaan,
							'id_barang' => $stok->id_barang,
							'qty' => $stok->qty
						);

						$result[] = $data;
					}
				}
			}
		}

		return $this->utils->convertListToJson($result);
	}

	public function getStokJoinBarang($stokList)
	{
		$barangIds = [];
		if (!empty($stokList)) {
			foreach ($stokList as $stok) {
				$barangIds[] = $stok->id_barang;
			}
		}

		$barangList = array();
		if (!empty($barangIds)) {
			$barangList = $this->barang->findByIdIn($barangIds);
		}

		$result = [];
		if (!empty($barangList)) {
			foreach ($stokList as $stok) {
				foreach ($barangList as $barang) {
					if ($stok->id_barang == $barang->id) {
						$data = array(
							'id' => $stok->id,
							'perusahaan' => $stok->perusahaan,
							'barang' => $barang,
							'qty' => $stok->qty
						);

						$result[] = $data;
					}
				}
			}
		}

		return $this->utils->convertListToJson($result);
	}
}
