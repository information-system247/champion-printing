<?php 

class Karyawan extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('UserModel','user');
		$this->load->model('Utils', 'utils');
		$this->load->model('KaryawanModel','Karyawan');
		$this->load->model('PerusahaanModel','perusahaan');
		$this->load->model('PosisiKaryawanModel','posisi');
  }

	public function index()
	{
		$userOptional = $this->utils->getUserProfile();
		$data = $this->utils->response(MenuConstant::LAPORAN_KARYAWAN);
    $this->load->view('laporan/karyawan/list', $data);
	}

	public function getKaryawanList()
	{
		$perusahaanId = $this->input->post('perusahaanId');
		$karyawanList = array();
		if (empty($perusahaanId)) {
			$karyawanList = $this->Karyawan->findAll();
		} else {
			$karyawanList = $this->Karyawan->findByidPerusahaan($perusahaanId);
		}

		if (!empty($karyawanList)) {
			$perusahaanList = array();
			if (empty($perusahaanId)) {
				$perusahaanList = $this->perusahaan->findAll();
			} else {
				$perusahaanList = $this->perusahaan->findById($perusahaanId);
			}
			$karyawanList = $this->getKaryawanJoinPerusahaan($karyawanList, $perusahaanList);
		}
		
		if (!empty($karyawanList)) {
			$posisiList = $this->posisi->findAll();
			$karyawanList = $this->getKaryawanJoinPosisi($karyawanList, $posisiList);
		}

		echo json_encode($karyawanList);
	}

	public function getKaryawanJoinPerusahaan($karyawanList, $perusahaanList)
  {
    $response = array();
    if (!empty($karyawanList)) {
      foreach ($karyawanList as $karyawan) {
        foreach ($perusahaanList as $perusahaan) {
          if ($karyawan->id_perusahaan == $perusahaan->id) {
            $data = array(
              'id' => $karyawan->id,
              'nama' => $karyawan->nama,
              'alamat' => $karyawan->alamat,
              'perusahaan' => $perusahaan,
              'telepon' => $karyawan->telepon,
              'id_posisi' => $karyawan->id_posisi
            );
            $response[] = $data;
          }
        }
      }
    }

		return $this->utils->convertListToJson($response);
  }

	public function getKaryawanJoinPosisi($karyawanList, $posisiList)
  {
    $response = array();
    if (!empty($karyawanList)) {
      foreach ($karyawanList as $karyawan) {
        foreach ($posisiList as $posisi) {
          if ($karyawan->id_posisi == $posisi->id) {
            $data = array(
              'id' => $karyawan->id,
              'nama' => $karyawan->nama,
              'alamat' => $karyawan->alamat,
              'perusahaan' => $karyawan->perusahaan,
              'telepon' => $karyawan->telepon,
              'posisi' => $posisi
            );
            $response[] = $data;
          }
        }
      }
    }

    return $this->utils->convertListToJson($response);
  }
}
