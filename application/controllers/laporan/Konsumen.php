<?php 

class Konsumen extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('UserModel','user');
		$this->load->model('Utils', 'utils');
		$this->load->model('KonsumenModel','konsumen');
  }

	public function index()
	{
		$userOptional = $this->utils->getUserProfile();
		$data = $this->utils->response(MenuConstant::LAPORAN_KONSUMEN);
    $this->load->view('laporan/konsumen/list', $data);
	}

	public function getKonsumenList()
	{
		$konsumenList = $this->konsumen->findAll();
		echo json_encode($konsumenList);
	}
}
