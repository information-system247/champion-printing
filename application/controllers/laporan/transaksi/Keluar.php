<?php 

class Keluar extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
		$this->load->model('Utils', 'utils');
		$this->load->model('TransactionHistoryModel','transaksi');
		$this->load->model('SupplierModel','supplier');
		$this->load->model('UserModel','user');
		$this->load->model('KaryawanModel','karyawan');
		$this->load->model('PerusahaanModel','perusahaan');
		$this->load->model('KonsumenModel','konsumen');
  }

	public function cabang()
	{
		$data = $this->utils->response(MenuConstant::LAPORAN_TRANSAKSI_KELUAR_CABANG);
    $this->load->view('laporan/transaksi/keluar/cabang/list', $data);
	}

	public function getTransaksiKeluarCabang()
	{
		$transaksiList = $this->transaksi->findByCategoryTransactionAndTransactionType(KategoriTransaksiConstant::MASUK, TipeTransaksiMasukConstant::TRANS_MASUK_CABANG_DARI_PUSAT);
		$userList = $this->getUserList($transaksiList);
		$karyawanList = $this->getKaryawanList($userList);
		$perusahaanList = $this->getPerusahaanList($karyawanList);
		$karyawanList = $this->getKaryawanJoinPerusahaan($karyawanList, $perusahaanList);
		$userList = $this->getUserJoinKaryawan($userList, $karyawanList);
		$transaksiList = $this->getTransaksiJoinUser($transaksiList, $userList);
		echo json_encode($transaksiList);
	}

	private function getTransaksiJoinUser($transaksiList, $userList)
	{
		$result = [];
		if (!empty($transaksiList)) {
			foreach ($transaksiList as $transaksi) {
				foreach ($userList as $user) {
					if ($transaksi->id_user == $user->id) {
						$data = array(
							'id' => $transaksi->id,
							'tgl_transaksi' => $transaksi->tgl_transaksi,
							'kategori_transaksi' => $transaksi->kategori_transaksi,
							'tipe_transaksi' => $transaksi->tipe_transaksi,
							'id_supplier' => $transaksi->id_supplier,
							'id_perusahaan' => $transaksi->id_perusahaan,
							'id_konsumen' => $transaksi->id_konsumen,
							'total' => $transaksi->total,
							'keterangan' => $transaksi->keterangan,
							'user' => $user,
							'status' => $transaksi->status,
						);

						$result[] = $data;
					}
				}
			}
		}

		return $this->utils->convertListToJson($result);
	}

	private function getUserJoinKaryawan($userList, $karyawanList)
	{
		$result = [];
		if (!empty($userList)) {
			foreach ($userList as $user) {
				foreach ($karyawanList as $karyawan) {
					if ($user->id_karyawan == $karyawan->id) {
						$data = array(
							'id' => $user->id,
							'karyawan' => $karyawan,
							'username' => $user->username,
							'password' => $user->password
						);

						$result[] = $data;
					}
				}
			}
		}

		return $this->utils->convertListToJson($result);
	}

	private function getKaryawanJoinPerusahaan($karyawanList, $perusahaanList)
	{
		$result = [];
		if (!empty($karyawanList)) {
			foreach ($karyawanList as $karyawan) {
				foreach ($perusahaanList as $perusahaan) {
					if ($karyawan->id_perusahaan == $perusahaan->id) {
						$data = array(
							'id' => $perusahaan->id,
							'nama' => $perusahaan->nama,
							'alamat' => $perusahaan->alamat,
							'perusahaan' => $perusahaan,
							'telepon' => $karyawan->telepon,
							'id_posisi' => $karyawan->id_posisi
						);

						$result[] = $data;
					}
				}
			}
		}

		return $this->utils->convertListToJson($result);
	}

	private function getUserList($transaksiList)
	{
		$userIds = [];
		if (!empty($transaksiList)) {
			foreach ($transaksiList as $transaksi) {
				$userIds = $transaksi->id_user;
			}
		}

		$userList = array();
		if (!empty($userIds)) {
			$userList = $this->user->findByIdIn($userIds);
		}

		return $this->utils->convertListToJson($userList);
	}

	private function getKaryawanList($userList)
	{
		$karyawanIds = [];
		if (!empty($userList)) {
			foreach ($userList as $user) {
				$karyawanIds[] = $user->id_karyawan;
			}
		}

		$karyawanList = array();
		if (!empty($karyawanIds)) {
			$karyawanList = $this->karyawan->findByIdIn($karyawanIds);
		}

		return $this->utils->convertListToJson($karyawanList);
	}

	private function getPerusahaanList($karyawanList)
	{
		$perusahaanIds = [];
		if (!empty($karyawanList)) {
			foreach ($karyawanList as $karyawan) {
				$perusahaanIds[] = $karyawan->id_perusahaan;
			}
		}

		$perusahaanList = array();
		if (!empty($perusahaanIds)) {
			$perusahaanList = $this->perusahaan->findByIdIn($perusahaanIds);
		}

		return $this->utils->convertListToJson($perusahaanList);
	}

	public function konsumen()
	{
		$data = $this->utils->response(MenuConstant::LAPORAN_TRANSAKSI_KELUAR_KONSUMEN);
    $this->load->view('laporan/transaksi/keluar/konsumen/list', $data);
	}

	public function getTransaksiKeluarKonsumen()
	{
		$userId = $this->input->post('userId');
		$transaksiList = $this->transaksi->findByCategoryTransactionAndTransactionTypeAndUserId(KategoriTransaksiConstant::KELUAR, tipeTransaksiKeluarConstant::TRANS_KELUAR_PUSAT_KE_KONSUMEN, $userId);
		$transaksiList = $this->getTransaksiJoinKonsumen($transaksiList);
		echo json_encode($transaksiList);
	}

	private function getTransaksiJoinKonsumen($transaksiList)
	{
		$konsumenIds = [];
		if (!empty($transaksiList)) {
			foreach ($transaksiList as $transaksi) {
				$konsumenIds[] = $transaksi->id_konsumen;
			}
		}

		$konsumenList = array();
		if (!empty($konsumenIds)) {
			$konsumenList = $this->konsumen->findByIdIn($konsumenIds);
		}

		$result = [];
		if (!empty($konsumenList)) {
			foreach ($transaksiList as $transaksi) {
				foreach ($konsumenList as $konsumen) {
					if ($transaksi->id_konsumen == $konsumen->id) {
						$data = array(
							'id' => $transaksi->id,
							'tgl_transaksi' => $transaksi->tgl_transaksi,
							'kategori_transaksi' => $transaksi->kategori_transaksi,
							'tipe_transaksi' => $transaksi->tipe_transaksi,
							'id_supplier' => $transaksi->id_supplier,
							'id_perusahaan' => $transaksi->id_perusahaan,
							'konsumen' => $konsumen,
							'total' => $transaksi->total,
							'keterangan' => $transaksi->keterangan,
							'id_user' => $transaksi->id_user,
							'status' => $transaksi->status,
						);

						$result[] = $data;
					}
				}
			}
		}

		return $this->utils->convertListToJson($result);
	}

	public function detailTransaksiKeluarCabang($id)
	{
		$historiTransaksi = $this->transaksi->findById($id);
		$data = array(
			'transaksiOptional' => $historiTransaksi,
		);
		$data = $this->utils->response(MenuConstant::LAPORAN_TRANSAKSI_KELUAR_CABANG, $data);

    $this->load->view('laporan/transaksi/keluar/cabang/detail', $data);
	}
}
