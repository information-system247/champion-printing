<?php 

class Masuk extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
		$this->load->model('Utils', 'utils');
		$this->load->model('TransactionHistoryModel','transaksi');
		$this->load->model('SupplierModel','supplier');
		$this->load->model('UserModel','user');
		$this->load->model('KaryawanModel','karyawan');
		$this->load->model('PerusahaanModel','perusahaan');
  }

	public function supplier()
	{
		$data = $this->utils->response(MenuConstant::LAPORAN_TRANSAKSI_MASUK_SUPPLIER);
    $this->load->view('laporan/transaksi/masuk/supplier/list', $data);
	}

	public function getTransaksiMasukSupplier()
	{
		$transaksiList = $this->transaksi->findByCategoryTransactionAndTransactionType(KategoriTransaksiConstant::MASUK, TipeTransaksiMasukConstant::TRANS_MASUK_PUSAT_DARI_SUPPLIER);
		$transaksiList = $this->getTransaksiJoinSupplier($transaksiList);
		echo json_encode($transaksiList);
	}

	public function getTransaksiJoinSupplier($transaksiList)
	{
		$supplierIds = [];
		if (!empty($transaksiList)) {
			foreach ($transaksiList as $transaksi) {
				$supplierIds[] = $transaksi->id_supplier;
			}
		}

		$supplierList = array();
		if (!empty($supplierIds)) {
			$supplierList = $this->supplier->findByIdIn($supplierIds);
		}

		$result = [];
		if (!empty($supplierList)) {
			foreach ($transaksiList as $transaksi) {
				foreach ($supplierList as $supplier) {
					if ($transaksi->id_supplier == $supplier->id) {
						$data = array(
							'id' => $transaksi->id,
							'tgl_transaksi' => $transaksi->tgl_transaksi,
							'kategori_transaksi' => $transaksi->kategori_transaksi,
							'tipe_transaksi' => $transaksi->tipe_transaksi,
							'supplier' => $supplier,
							'id_perusahaan' => $transaksi->id_perusahaan,
							'id_konsumen' => $transaksi->id_konsumen,
							'total' => $transaksi->total,
							'keterangan' => $transaksi->keterangan,
							'id_user' => $transaksi->id_user,
							'status' => $transaksi->status
						);

						$result[] = $data;
					}
				}
			}
		}

		return $this->utils->convertListToJson($result);
	}

	public function retur()
	{
		$data = $this->utils->response(MenuConstant::LAPORAN_TRANSAKSI_MASUK_RETUR);
    $this->load->view('laporan/transaksi/masuk/retur/list', $data);
	}

	public function getTransaksiMasukRetur()
	{
		$transaksiList = $this->transaksi->findByCategoryTransactionAndTransactionType(KategoriTransaksiConstant::KELUAR, TipeTransaksiMasukConstant::TRANS_MASUK_PUSAT_DARI_RETUR);
		$userList = $this->getUserList($transaksiList);
		$karyawanList = $this->getKaryawanList($userList);
		$perusahaanList = $this->getPerusahaanList($karyawanList);
		$karyawanList = $this->getKaryawanJoinPerusahaan($karyawanList, $perusahaanList);
		$userList = $this->getUserJoinKaryawan($userList, $karyawanList);
		$transaksiList = $this->getTransaksiJoinUser($transaksiList, $userList);
		echo json_encode($transaksiList);
	}

	private function getTransaksiJoinUser($transaksiList, $userList)
	{
		$result = [];
		if (!empty($transaksiList)) {
			foreach ($transaksiList as $transaksi) {
				foreach ($userList as $user) {
					if ($transaksi->id_user == $user->id) {
						$data = array(
							'id' => $transaksi->id,
							'tgl_transaksi' => $transaksi->tgl_transaksi,
							'kategori_transaksi' => $transaksi->kategori_transaksi,
							'tipe_transaksi' => $transaksi->tipe_transaksi,
							'id_supplier' => $transaksi->id_supplier,
							'id_perusahaan' => $transaksi->id_perusahaan,
							'id_konsumen' => $transaksi->id_konsumen,
							'total' => $transaksi->total,
							'keterangan' => $transaksi->keterangan,
							'user' => $user,
							'status' => $transaksi->status,
						);

						$result[] = $data;
					}
				}
			}
		}

		return $this->utils->convertListToJson($result);
	}

	private function getUserJoinKaryawan($userList, $karyawanList)
	{
		$result = [];
		if (!empty($userList)) {
			foreach ($userList as $user) {
				foreach ($karyawanList as $karyawan) {
					if ($user->id_karyawan == $karyawan->id) {
						$data = array(
							'id' => $user->id,
							'karyawan' => $karyawan,
							'username' => $user->username,
							'password' => $user->password
						);

						$result[] = $data;
					}
				}
			}
		}

		return $this->utils->convertListToJson($result);
	}

	private function getKaryawanJoinPerusahaan($karyawanList, $perusahaanList)
	{
		$result = [];
		if (!empty($karyawanList)) {
			foreach ($karyawanList as $karyawan) {
				foreach ($perusahaanList as $perusahaan) {
					if ($karyawan->id_perusahaan == $perusahaan->id) {
						$data = array(
							'id' => $perusahaan->id,
							'nama' => $perusahaan->nama,
							'alamat' => $perusahaan->alamat,
							'perusahaan' => $perusahaan,
							'telepon' => $karyawan->telepon,
							'id_posisi' => $karyawan->id_posisi
						);

						$result[] = $data;
					}
				}
			}
		}

		return $this->utils->convertListToJson($result);
	}

	private function getUserList($transaksiList)
	{
		$userIds = [];
		if (!empty($transaksiList)) {
			foreach ($transaksiList as $transaksi) {
				$userIds = $transaksi->id_user;
			}
		}

		$userList = array();
		if (!empty($userIds)) {
			$userList = $this->user->findByIdIn($userIds);
		}

		return $this->utils->convertListToJson($userList);
	}

	private function getKaryawanList($userList)
	{
		$karyawanIds = [];
		if (!empty($userList)) {
			foreach ($userList as $user) {
				$karyawanIds[] = $user->id_karyawan;
			}
		}

		$karyawanList = array();
		if (!empty($karyawanIds)) {
			$karyawanList = $this->karyawan->findByIdIn($karyawanIds);
		}

		return $this->utils->convertListToJson($karyawanList);
	}

	private function getPerusahaanList($karyawanList)
	{
		$perusahaanIds = [];
		if (!empty($karyawanList)) {
			foreach ($karyawanList as $karyawan) {
				$perusahaanIds[] = $karyawan->id_perusahaan;
			}
		}

		$perusahaanList = array();
		if (!empty($perusahaanIds)) {
			$perusahaanList = $this->perusahaan->findByIdIn($perusahaanIds);
		}

		return $this->utils->convertListToJson($perusahaanList);
	}

	public function pusat()
	{
		$data = $this->utils->response(MenuConstant::LAPORAN_TRANSAKSI_MASUK_PUSAT);
    $this->load->view('laporan/transaksi/masuk/pusat/list', $data);
	}

	public function getTransaksiMasukPusat()
	{
		$userId = $this->input->post('userId');
		$transaksiList = $this->transaksi->findByCategoryTransactionAndTransactionTypeAndUserId(KategoriTransaksiConstant::MASUK, TipeTransaksiMasukConstant::TRANS_MASUK_CABANG_DARI_PUSAT, $userId);
		$userList = $this->getUserList($transaksiList);
		$karyawanList = $this->getKaryawanList($userList);
		$perusahaanList = $this->getPerusahaanList($karyawanList);
		$karyawanList = $this->getKaryawanJoinPerusahaan($karyawanList, $perusahaanList);
		$userList = $this->getUserJoinKaryawan($userList, $karyawanList);
		$transaksiList = $this->getTransaksiJoinUser($transaksiList, $userList);
		echo json_encode($transaksiList);
	}
}
