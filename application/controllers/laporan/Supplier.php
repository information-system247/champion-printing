<?php 

class Supplier extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('UserModel','user');
		$this->load->model('Utils', 'utils');
		$this->load->model('SupplierModel','supplier');
  }

	public function index()
	{
		$userOptional = $this->utils->getUserProfile();
		$data = $this->utils->response(MenuConstant::LAPORAN_SUPPLIER);
    $this->load->view('laporan/supplier/list', $data);
	}

	public function getSupplierList()
	{
		$supplierList = $this->supplier->findAll();
		echo json_encode($supplierList);
	}
}
