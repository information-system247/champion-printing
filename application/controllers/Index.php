<?php 

class Index extends CI_Controller
{
  public function __construct() {
    parent::__construct();
    $this->load->model('UserModel','user');
		$this->load->model('Utils','utils');
  }

  public function index()
  {
    $data = $this->utils->getUserProfile();
		$data = $this->utils->response(MenuConstant::BERANDA);

    $this->load->view('index', $data);
  }
}
