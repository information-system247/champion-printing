<?php 

class Pdf extends CI_Controller
{
  function __construct(){
		parent::__construct();
		$this->load->library('pdfgenerator');
		$this->load->model('SupplierModel', 'supplier');
		$this->load->model('KonsumenModel', 'konsumen');
		$this->load->model('KaryawanModel', 'karyawan');
		$this->load->model('PerusahaanModel', 'perusahaan');
		$this->load->model('PosisiKaryawanModel', 'posisi');
		$this->load->model('StokModel', 'stok');
		$this->load->model('Utils', 'utils');
		$this->load->model('DetailTransaksiModel', 'detail');
		$this->load->model('TransactionHistoryModel', 'transactionHistory');
		$this->load->model('BarangModel', 'barang');
	}

  public function karyawan()
  {
		$karyawanList = $this->karyawan->findAll();
		$perusahaanList = $this->perusahaan->findAll();
		$karyawanList = $this->getKaryawanJoinPerusahaan($karyawanList, $perusahaanList);
		$posisiList = $this->posisi->findAll();
		$karyawanList = $this->getKaryawanJoinPosisi($karyawanList, $posisiList);
    $file_pdf = 'Laporan';
    $data = array(
      'file_pdf' => $file_pdf,
			'karyawanList' => $karyawanList
    );

    $html = $this->load->view('laporan/karyawan/download', $data, true);
    $this->pdfgenerator->generate($html, $file_pdf);
  }

	public function supplier()
	{
		$supplierList = $this->supplier->findAll();
		$file_pdf = 'Laporan';
    $data = array(
      'file_pdf' => $file_pdf,
			'supplierList' => $supplierList
    );

    $html = $this->load->view('laporan/supplier/download', $data, true);
    $this->pdfgenerator->generate($html, $file_pdf);
	}

	private function getTransaksiJoinDetail($transactionHistoryList)
	{
		$result = [];
		if (!empty($transactionHistoryList)) {
			foreach ($transactionHistoryList as $transaksi) {
				$detailList = $this->detail->findByTransaksiId($transaksi->id);
				$detailList = $this->getDetailHistoriTransaksiJoinBarang($detailList);
				$data = array(
					'id' => $transaksi->id,
					'tgl_transaksi' => $transaksi->tgl_transaksi,
					'kategori_transaksi' => $transaksi->kategori_transaksi,
					'tipe_transaksi' => $transaksi->tipe_transaksi,
					'id_supplier' => $transaksi->id_supplier,
					'id_perusahaan' => $transaksi->id_perusahaan,
					'id_konsumen' => $transaksi->id_konsumen,
					'total' => $transaksi->total,
					'keterangan' => $transaksi->keterangan,
					'id_user' => $transaksi->id_user,
					'status' => $transaksi->status,
					'detailList' => $detailList
				);

				$result[] = $data;
			}
		}

		return $this->utils->convertListToJson($result);
	}

	private function getDetailHistoriTransaksiJoinBarang($detailList)
	{
		$barangIds = [];
		if (!empty($detailList)) {
			foreach ($detailList as $detail) {
				$barangIds[] = $detail->id_barang;
			}
		}

		$barangList = array();
		if (!empty($barangIds)) {
			$barangList = $this->barang->findByIdIn($barangIds);
		}

		$result = [];
		if (!empty($barangList)) {
			foreach ($detailList as $detail) {
				foreach ($barangList as $barang) {
					if ($detail->id_barang == $barang->id) {
						$data = array(
							'id' => $detail->id,
							'barang' => $barang,
							'id_transaksi' => $detail->id_transaksi,
							'qty' => $detail->qty,
							'sub_total' => $detail->sub_total
						);

						$result [] = $data;
					}
				}
			}
		}

		return $this->utils->convertListToJson($result);
	}

	private function transactionHistoryJoinPerusahaan($transactionHistoryList)
	{
		$perusahaanIds = [];
		if (!empty($transactionHistoryList)) {
			foreach ($transactionHistoryList as $transactionHistory) {
				$perusahaanIds[] = $transactionHistory->id_perusahaan;
			}
		}

		$perusahaanList = array();
		if (!empty($perusahaanIds)) {
			$perusahaanList = $this->perusahaan->findByIdIn($perusahaanIds);
		}

		$result = [];
		if (!empty($perusahaanList)) {
			foreach ($transactionHistoryList as $transactionHistory) {
				foreach ($perusahaanList as $perusahaan) {
					if ($transactionHistory->id_perusahaan == $perusahaan->id) {
						$data = array(
							'id' => $transactionHistory->id,
							'tgl_transaksi' => $transactionHistory->tgl_transaksi,
							'kategori_transaksi' => $transactionHistory->kategori_transaksi,
							'tipe_transaksi' => $transactionHistory->tipe_transaksi,
							'id_supplier' => $transactionHistory->id_supplier,
							'perusahaan' => $perusahaan,
							'id_konsumen' => $transactionHistory->id_konsumen,
							'total' => $transactionHistory->total,
							'keterangan' => $transactionHistory->keterangan,
							'id_user' => $transactionHistory->id_user,
							'status' => $transactionHistory->status,
							'detailList' => $transactionHistory->detailList
						);

						$result[] = $data;
					}
				}
			}
		}

		return $this->utils->convertListToJson($result);
	}

	private function transactionHistoryJoinSupplier($transactionHistoryList)
	{
		$supplierIds = [];
		if (!empty($transactionHistoryList)) {
			foreach ($transactionHistoryList as $transactionHistory) {
				$supplierIds[] = $transactionHistory->id_supplier;
			}
		}

		$supplierList = array();
		if (!empty($supplierIds)) {
			$supplierList = $this->supplier->findByIdIn($supplierIds);
		}

		$result = [];
		if (!empty($supplierList)) {
			foreach ($transactionHistoryList as $transactionHistory) {
				foreach ($supplierList as $supplier) {
					if ($transactionHistory->id_supplier == $supplier->id) {
						$data = array(
							'id' => $transactionHistory->id,
							'tgl_transaksi' => $transactionHistory->tgl_transaksi,
							'kategori_transaksi' => $transactionHistory->kategori_transaksi,
							'tipe_transaksi' => $transactionHistory->tipe_transaksi,
							'supplier' => $supplier,
							'id_perusahaan' => $transactionHistory->id_perusahaan,
							'id_konsumen' => $transactionHistory->id_konsumen,
							'total' => $transactionHistory->total,
							'keterangan' => $transactionHistory->keterangan,
							'id_user' => $transactionHistory->id_user,
							'status' => $transactionHistory->status,
							'detailList' => $transactionHistory->detailList
						);

						$result[] = $data;
					}
				}
			}
		}

		return $this->utils->convertListToJson($result);
	}

	public function konsumen()
	{
		$konsumenList = $this->konsumen->findAll();
		$file_pdf = 'Laporan';
    $data = array(
      'file_pdf' => $file_pdf,
			'konsumenList' => $konsumenList
    );

    $html = $this->load->view('laporan/konsumen/download', $data, true);
    $this->pdfgenerator->generate($html, $file_pdf);
	}

	public function stok()
	{
		$stokList = $this->stok->findAll();
		$stokList = $this->getStokJoinPerusahaan($stokList);
		$stokList = $this->getStokJoinBarang($stokList);
		$file_pdf = 'Laporan';
    $data = array(
      'file_pdf' => $file_pdf,
			'stokList' => $stokList
    );

    $html = $this->load->view('laporan/stok/download', $data, true);
    $this->pdfgenerator->generate($html, $file_pdf);
	}

	private function getStokJoinPerusahaan($stokList)
	{
		$perusahaanIds = [];
		if (!empty($stokList)) {
			foreach ($stokList as $stok) {
				$perusahaanIds = $stok->id_perusahaan;
			}
		}

		$perusahaanList = array();
		if (!empty($perusahaanIds)) {
			$perusahaanList = $this->perusahaan->findByIdIn($perusahaanIds);
		}

		$result = [];
		if (!empty($perusahaanList)) {
			foreach ($stokList as $stok) {
				foreach ($perusahaanList as $perusahaan) {
					if ($stok->id_perusahaan == $perusahaan->id) {
						$data = array(
							'id' => $stok->id,
							'perusahaan' => $perusahaan,
							'id_barang' => $stok->id_barang,
							'qty' => $stok->qty
						);

						$result[] = $data;
					}
				}
			}
		}

		return $this->utils->convertListToJson($result);
	}

	private function getStokJoinBarang($stokList)
	{
		$barangIds = [];
		if (!empty($stokList)) {
			foreach ($stokList as $stok) {
				$barangIds[] = $stok->id_barang;
			}
		}

		$barangList = array();
		if (!empty($barangIds)) {
			$barangList = $this->barang->findByIdIn($barangIds);
		}

		$result = [];
		if (!empty($barangList)) {
			foreach ($stokList as $stok) {
				foreach ($barangList as $barang) {
					if ($stok->id_barang == $barang->id) {
						$data = array(
							'id' => $stok->id,
							'perusahaan' => $stok->perusahaan,
							'barang' => $barang,
							'qty' => $stok->qty
						);

						$result[] = $data;
					}
				}
			}
		}

		return $this->utils->convertListToJson($result);
	}

	public function transaksiMasukSupplier()
	{
		$transactionHistoryList = $this->transactionHistory->findByCategoryTransactionAndTransactionType(KategoriTransaksiConstant::MASUK, TipeTransaksiMasukConstant::TRANS_MASUK_PUSAT_DARI_SUPPLIER);
		$transactionHistoryList = $this->getTransaksiJoinDetail($transactionHistoryList);
		$transactionHistoryList = $this->transactionHistoryJoinSupplier($transactionHistoryList);
		$file_pdf = 'Laporan';
    $data = array(
      'file_pdf' => $file_pdf,
			'transaksiList' => $transactionHistoryList
    );

    $html = $this->load->view('laporan/transaksi/masuk/supplier/download', $data, true);
    $this->pdfgenerator->generate($html, $file_pdf);
	}

	public function transaksiMasukRetur()
	{
		$file_pdf = 'Laporan';
    $data = array(
      'file_pdf' => $file_pdf
    );

    $html = $this->load->view('laporan/transaksi/masuk/retur/download', $data, true);
    $this->pdfgenerator->generate($html, $file_pdf);
	}

	public function transaksiKeluarCabang()
	{
		$transactionHistoryList = $this->transactionHistory->findByCategoryTransactionAndTransactionType(KategoriTransaksiConstant::MASUK, TipeTransaksiMasukConstant::TRANS_MASUK_CABANG_DARI_PUSAT);
		$transactionHistoryList = $this->getTransaksiJoinDetail($transactionHistoryList);
		$transactionHistoryList = $this->transactionHistoryJoinPerusahaan($transactionHistoryList);
		$file_pdf = 'Laporan';
    $data = array(
      'file_pdf' => $file_pdf,
			'transaksiList' => $transactionHistoryList
    );

    $html = $this->load->view('laporan/transaksi/keluar/cabang/download', $data, true);
    $this->pdfgenerator->generate($html, $file_pdf);
	}

	public function transaksiKeluarPusat()
	{
		$file_pdf = 'Laporan';
    $data = array(
      'file_pdf' => $file_pdf
    );

    $html = $this->load->view('laporan/transaksi/keluar/pusat/download', $data, true);
    $this->pdfgenerator->generate($html, $file_pdf);
	}

	public function transaksiKeluarKonsumen()
	{
		$file_pdf = 'Laporan';
    $data = array(
      'file_pdf' => $file_pdf
    );

    $html = $this->load->view('laporan/transaksi/keluar/konsumen/download', $data, true);
    $this->pdfgenerator->generate($html, $file_pdf);
	}

	public function getKaryawanJoinPerusahaan($karyawanList, $perusahaanList)
  {
    $response = array();
    if (!empty($karyawanList)) {
      foreach ($karyawanList as $karyawan) {
        foreach ($perusahaanList as $perusahaan) {
          if ($karyawan->id_perusahaan == $perusahaan->id) {
            $data = array(
              'id' => $karyawan->id,
              'nama' => $karyawan->nama,
              'alamat' => $karyawan->alamat,
              'perusahaan' => $perusahaan,
              'telepon' => $karyawan->telepon,
              'id_posisi' => $karyawan->id_posisi
            );
            $response[] = $data;
          }
        }
      }
    }

		return $this->utils->convertListToJson($response);
  }

	public function getKaryawanJoinPosisi($karyawanList, $posisiList)
  {
    $response = array();
    if (!empty($karyawanList)) {
      foreach ($karyawanList as $karyawan) {
        foreach ($posisiList as $posisi) {
          if ($karyawan->id_posisi == $posisi->id) {
            $data = array(
              'id' => $karyawan->id,
              'nama' => $karyawan->nama,
              'alamat' => $karyawan->alamat,
              'perusahaan' => $karyawan->perusahaan,
              'telepon' => $karyawan->telepon,
              'posisi' => $posisi
            );
            $response[] = $data;
          }
        }
      }
    }

    return $this->utils->convertListToJson($response);
  }
}
