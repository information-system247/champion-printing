<?php

class Konsumen extends CI_Controller
{
	public function __construct()
  {
    parent::__construct();
    $this->load->model('UserModel','user');
		$this->load->model('Utils', 'utils');
		$this->load->model('TransactionHistoryModel','transactionHistory');
		$this->load->model('KonsumenModel','konsumen');
		$this->load->model('TempModel','temp');
		$this->load->model('BarangModel','barang');
		$this->load->model('DetailTransaksiModel', 'detailTransaksi');
		$this->load->model('StokModel','stok');
  }

	public function index()
	{
		$userOptional = $this->utils->getUserProfile();
		$transactionHistoryList = $this->transactionHistory->findByCategoryTransactionAndTransactionTypeNot(KategoriTransaksiConstant::KELUAR, tipeTransaksiKeluarConstant::TRANS_KELUAR_PUSAT_KE_CABANG);
		$transactionHistoryList = $this->transactionHistoryJoinKonsumen($transactionHistoryList);

		$data = array(
			'transactionHistoryList' => $transactionHistoryList
		);
		$data = $this->utils->response(MenuConstant::TRANSAKSI_KELUAR_KONSUMEN, $data);

    $this->load->view('transaksi/keluar/konsumen/list', $data);
	}

	private function transactionHistoryJoinKonsumen($transactionHistoryList)
	{
		$konsumenIds = [];
		if (count($transactionHistoryList) > 0) {
			foreach ($transactionHistoryList as $transactionHistory) {
				$konsumenIds[] = $transactionHistory->id_konsumen;
			}
		}

		$konsumenList = array();
		if (!empty($konsumenIds)) {
			$konsumenList = $this->konsumen->findByIdIn($konsumenIds);
		}

		$result = array();
		if (count($konsumenList) > 0) {
			foreach ($transactionHistoryList as $transactionHistory) {
				foreach ($konsumenList as $konsumen) {
					if ($transactionHistory->id_konsumen == $konsumen->id) {
						$data = array(
							'id' => $transactionHistory->id,
							'tgl_transaksi' => $transactionHistory->tgl_transaksi,
							'kategori_transaksi' => $transactionHistory->kategori_transaksi,
							'tipe_transaksi' => $transactionHistory->tipe_transaksi,
							'id_supplier' => $transactionHistory->id_supplier,
							'id_perusahaan' => $transactionHistory->id_perusahaan,
							'konsumen' => $konsumen,
							'total' => $transactionHistory->total,
							'keterangan' => $transactionHistory->keterangan,
							'id_user' => $transactionHistory->id_user,
							'status' => $transactionHistory->status
						);

						$result[] = $data;
					}
				}
			}
		}

		return $this->utils->convertListToJson($result);
	}

	public function add()
	{
		$userOptional = $this->utils->getUserProfile();
		$tempList = $this->temp->findByUserIdAndKategoriTransaksiAndTipeTransaksi($userOptional->id, KategoriTransaksiConstant::KELUAR, tipeTransaksiKeluarConstant::TRANS_KELUAR_PUSAT_KE_KONSUMEN);
		$konsumenList = array();
		$barangList = $this->barang->findAll();
		$total = 0;
		if (!empty($tempList)) {
			$konsumenList = $this->konsumen->findById($tempList[0]->id_konsumen);
			foreach ($tempList as $temp) {
				$total = $total + $temp->sub_total;
		}
		} else {
			$konsumenList = $this->konsumen->findAll();
		}
		
		$data = array(
			'konsumenList' => $konsumenList,
			'tempList' => $tempList,
			'barangList' => $barangList,
			'total' => $total
		);

		$data = $this->utils->response(MenuConstant::TRANSAKSI_KELUAR_KONSUMEN, $data);

    $this->load->view('transaksi/keluar/konsumen/add', $data);
	}

	public function store()
	{
		$userOptional = $this->utils->getUserProfile();
		$userId = $userOptional->id;
		$tempList = $this->temp->findByUserIdAndKategoriTransaksiAndTipeTransaksi($userId, KategoriTransaksiConstant::KELUAR, tipeTransaksiKeluarConstant::TRANS_KELUAR_PUSAT_KE_KONSUMEN);
		if (!empty($tempList)) {
			$this->storeHistoriTransaksi($tempList, $userId, $userOptional);
		}
		redirect(BaseUrl::DEFAULT_TRANSAKSI_KELUAR_KONSUMEN_URL);
	}

	private function storeHistoriTransaksi($tempList, $userId, $userOptional)
	{
		$konsumenId = null;
		$total = 0;
		foreach ($tempList as $temp) {
				$total = $total + $temp->sub_total;
				$konsumenId = $temp->id_konsumen;
		}

		$data = array(
			'kategori_transaksi' => $temp->kategori_transaksi,
			'tipe_transaksi' => $temp->tipe_transaksi,
			'id_konsumen' => $temp->id_konsumen,
			'total' => $total,
			'id_user' => $userId,
			'status' => StatusTransaksiConstant::APPROVED
		);

		$save = $this->transactionHistory->save($data);
		if ($save) {
			$this->storeDetailTransaksi($tempList, $userId, $konsumenId, $userOptional);
		}
	}

	private function storeDetailTransaksi($tempList, $userId, $konsumenId, $userOptional)
	{
		$historiTransaksi = $this->transactionHistory->findByCategoryTransactionAndTransactionTypeAndUserIdAndKonsumenIdLimit1(KategoriTransaksiConstant::KELUAR,
			tipeTransaksiKeluarConstant::TRANS_KELUAR_PUSAT_KE_KONSUMEN, $userId, $konsumenId);
		$perusahaanId = $userOptional->karyawan->perusahaan->id;
		if (!empty($historiTransaksi)) {
			$transaksiId = $historiTransaksi[0]->id;
			foreach ($tempList as $temp) {
				$barangId = $temp->id_barang;
				$qty = $temp->qty;
				$data = array(
					'id_transaksi' => $transaksiId,
					'id_barang' => $barangId,
					'qty' => $qty,
					'sub_total' => $temp->sub_total
				);
				$save = $this->detailTransaksi->save($data);
				if ($save) {
					$this->getStokByPerusahaanAndBarang($perusahaanId, $barangId, $qty);
				}
			}
			$this->temp->deleteByUserIdAndKategoriTransaksiAndTipeTransaksi($userId, KategoriTransaksiConstant::KELUAR, tipeTransaksiKeluarConstant::TRANS_KELUAR_PUSAT_KE_KONSUMEN);
		}
	}

	public function getStokByPerusahaanAndBarang($perusahaanId, $barangId, $qty)
	{
		$stokOptional = $this->stok->findByPerusahaanIdAndBarangId($perusahaanId, $barangId);
		if (empty($stokOptional)) {
			$this->storeStok($perusahaanId, $barangId, $qty);
		} else {
			$stokOptional = $stokOptional[0];
			$stokAwal = (int) $stokOptional->qty;
			$qty = (int) $qty;
			$qty = $stokAwal - $qty;
			$this->updateStok($stokOptional->id, $perusahaanId, $barangId, $qty);
		}
	}

	public function storeStok($perusahaanId, $barangId, $qty)
	{
		$data = array(
			'id_perusahaan' => $perusahaanId,
			'id_barang' => $barangId,
			'qty' => $qty
		);

		$this->stok->save($data);
	}

	public function updateStok($id, $perusahaanId, $barangId, $qty)
	{
		$data = array(
			'id' => $id,
			'id_perusahaan' => $perusahaanId,
			'id_barang' => $barangId,
			'qty' => $qty
		);

		$this->stok->update($data);
	}

	public function detailTransaksi($id)
	{
		$historiTransaksi = $this->transactionHistory->findById($id);
		$data = array(
			'transaksiOptional' => $historiTransaksi,
		);
		$data = $this->utils->response(MenuConstant::TRANSAKSI_KELUAR_KONSUMEN, $data);

    $this->load->view('transaksi/keluar/konsumen/detail', $data);
	}
}
