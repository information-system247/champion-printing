<?php

class Retur extends CI_Controller
{
	public function __construct()
  {
    parent::__construct();
    $this->load->model('UserModel','user');
		$this->load->model('Utils', 'utils');
		$this->load->model('TransactionHistoryModel','transactionHistory');
		$this->load->model('PerusahaanModel','perusahaan');
		$this->load->model('TempModel','temp');
		$this->load->model('BarangModel','barang');
		$this->load->model('DetailTransaksiModel', 'detailTransaksi');
		$this->load->model('StokModel','stok');
  }

	public function index()
	{
		$data = $this->utils->response(MenuConstant::TRANSAKSI_KELUAR_RETUR);
    $this->load->view('transaksi/keluar/retur/list', $data);
	}

	public function add()
	{
		$data = $this->utils->response(MenuConstant::TRANSAKSI_KELUAR_RETUR);

    $this->load->view('transaksi/keluar/retur/add', $data);
	}

	public function store()
	{
		$userOptional = $this->utils->getUserProfile();
		$userId = $userOptional->id;
		$tempList = $this->temp->findByUserIdAndKategoriTransaksiAndTipeTransaksi($userId, KategoriTransaksiConstant::KELUAR, tipeTransaksiKeluarConstant::TRANS_KELUAR_CABANG_RETUR);
		if (!empty($tempList)) {
			$this->storeHistoriTransaksi($tempList, $userId, $userOptional);
		}
		redirect(BaseUrl::DEFAULT_TRANSAKSI_KELUAR_RETUR_URL);
	}

	private function storeHistoriTransaksi($tempList, $userId, $userOptional)
	{
		$perusahaanId = null;
		$total = 0;
		foreach ($tempList as $temp) {
				$total = $total + $temp->sub_total;
				$perusahaanId = $temp->id_perusahaan;
		}

		$additionalData = array(
			'status' => JenisTransaksiConstant::RETUR
		);

		$data = array(
			'kategori_transaksi' => $temp->kategori_transaksi,
			'tipe_transaksi' => $temp->tipe_transaksi,
			'id_perusahaan' => $temp->id_perusahaan,
			'total' => $total,
			'id_user' => $userId,
			'status' => StatusTransaksiConstant::WAITING_APPROVED,
			'keterangan' => json_encode($additionalData)
		);

		$save = $this->transactionHistory->save($data);
		if ($save) {
			$this->storeDetailTransaksi($tempList, $userId, $perusahaanId, $userOptional);
		}
	}

	private function storeDetailTransaksi($tempList, $userId, $perusahaanId, $userOptional)
	{
		$historiTransaksi = $this->transactionHistory->findByCategoryTransactionAndTransactionTypeAndUserIdAndPerusahaanIdLimit1(KategoriTransaksiConstant::KELUAR,
			tipeTransaksiKeluarConstant::TRANS_KELUAR_CABANG_RETUR, $userId, $perusahaanId);
		if (!empty($historiTransaksi)) {
			$transaksiId = $historiTransaksi[0]->id;
			$perusahaanId = $userOptional->karyawan->perusahaan->id;
			foreach ($tempList as $temp) {
				$barangId = $temp->id_barang;
				$qty = $temp->qty;
				$data = array(
					'id_transaksi' => $transaksiId,
					'id_barang' => $barangId,
					'qty' => $qty
				);
				$save = $this->detailTransaksi->save($data);
				if ($save) {
					$this->getStokByPerusahaanAndBarang($perusahaanId, $barangId, $qty);
				}
			}
			$this->temp->deleteByUserIdAndKategoriTransaksiAndTipeTransaksi($userId, KategoriTransaksiConstant::KELUAR, tipeTransaksiKeluarConstant::TRANS_KELUAR_CABANG_RETUR);
		}
	}

	public function getStokByPerusahaanAndBarang($perusahaanId, $barangId, $qty)
	{
		$stokOptional = $this->stok->findByPerusahaanIdAndBarangId($perusahaanId, $barangId);
		if (empty($stokOptional)) {
			$this->storeStok($perusahaanId, $barangId, $qty);
		} else {
			$stokOptional = $stokOptional[0];
			$stokAwal = (int) $stokOptional->qty;
			$qty = (int) $qty;
			$qty = $stokAwal - $qty;
			$this->updateStok($stokOptional->id, $perusahaanId, $barangId, $qty);
		}
	}

	public function storeStok($perusahaanId, $barangId, $qty)
	{
		$data = array(
			'id_perusahaan' => $perusahaanId,
			'id_barang' => $barangId,
			'qty' => $qty
		);

		$this->stok->save($data);
	}

	public function updateStok($id, $perusahaanId, $barangId, $qty)
	{
		$data = array(
			'id' => $id,
			'id_perusahaan' => $perusahaanId,
			'id_barang' => $barangId,
			'qty' => $qty
		);

		$this->stok->update($data);
	}

	public function detailTransaksi($id)
	{
		$historiTransaksi = $this->transactionHistory->findById($id);
		$data = array(
			'transaksiOptional' => $historiTransaksi,
		);
		$data = $this->utils->response(MenuConstant::TRANSAKSI_KELUAR_RETUR, $data);

    $this->load->view('transaksi/keluar/retur/detail', $data);
	}
}
