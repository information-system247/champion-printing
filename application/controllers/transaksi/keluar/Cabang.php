<?php

class Cabang extends CI_Controller
{
	public function __construct()
  {
    parent::__construct();
    $this->load->model('UserModel','user');
		$this->load->model('Utils', 'utils');
		$this->load->model('TransactionHistoryModel','transactionHistory');
		$this->load->model('PerusahaanModel','perusahaan');
		$this->load->model('TempModel','temp');
		$this->load->model('BarangModel','barang');
		$this->load->model('DetailTransaksiModel', 'detailTransaksi');
  }

	public function index()
	{
		$userOptional = $this->utils->getUserProfile();
		$transactionHistoryList = $this->transactionHistory->findByCategoryTransactionAndTransactionType(KategoriTransaksiConstant::KELUAR, tipeTransaksiKeluarConstant::TRANS_KELUAR_PUSAT_KE_CABANG);
		$transactionHistoryList = $this->transactionHistoryJoinPerusahaan($transactionHistoryList);

		$data = array(
			'transactionHistoryList' => $transactionHistoryList
		);
		$data = $this->utils->response(MenuConstant::TRANSAKSI_KELUAR_PUSAT_CABANG, $data);

    $this->load->view('transaksi/keluar/pusat/cabang/list', $data);
	}

	private function transactionHistoryJoinPerusahaan($transactionHistoryList)
	{
		$perusahaanIds = [];
		if (count($transactionHistoryList) > 0) {
			foreach ($transactionHistoryList as $transactionHistory) {
				$perusahaanIds[] = $transactionHistory->id_perusahaan;
			}
		}

		$perusahaanList = array();
		if (!empty($perusahaanIds)) {
			$perusahaanList = $this->perusahaan->findByIdIn($perusahaanIds);
		}

		$result = array();
		if (count($perusahaanList) > 0) {
			foreach ($transactionHistoryList as $transactionHistory) {
				foreach ($perusahaanList as $perusahaan) {
					if ($transactionHistory->id_perusahaan == $perusahaan->id) {
						$data = array(
							'id' => $transactionHistory->id,
							'tgl_transaksi' => $transactionHistory->tgl_transaksi,
							'kategori_transaksi' => $transactionHistory->kategori_transaksi,
							'tipe_transaksi' => $transactionHistory->tipe_transaksi,
							'id_supplier' => $transactionHistory->id_supplier,
							'perusahaan' => $perusahaan,
							'id_konsumen' => $transactionHistory->id_konsumen,
							'total' => $transactionHistory->total,
							'keterangan' => $transactionHistory->keterangan,
							'id_user' => $transactionHistory->id_user,
							'status' => $transactionHistory->status
						);

						$result[] = $data;
					}
				}
			}
		}

		return $this->utils->convertListToJson($result);
	}

	public function add()
	{
		$userOptional = $this->utils->getUserProfile();
		$tempList = $this->temp->findByUserIdAndKategoriTransaksiAndTipeTransaksi($userOptional->id, KategoriTransaksiConstant::KELUAR, tipeTransaksiKeluarConstant::TRANS_KELUAR_PUSAT_KE_CABANG);
		$perusahaanList = array();
		$barangList = $this->barang->findAll();
		if (!empty($tempList)) {
			$perusahaanList = $this->perusahaan->findById($tempList[0]->id_perusahaan);
		} else {
			$perusahaanList = $this->perusahaan->findByIdNot($userOptional->karyawan->perusahaan->id);
		}
		
		$data = array(
			'perusahaanList' => $perusahaanList,
			'tempList' => $tempList,
			'barangList' => $barangList
		);

		$data = $this->utils->response(MenuConstant::TRANSAKSI_KELUAR_PUSAT_CABANG, $data);

    $this->load->view('transaksi/keluar/pusat/cabang/add', $data);
	}

	public function store()
	{
		$userOptional = $this->utils->getUserProfile();
		$userId = $userOptional->id;
		$tempList = $this->temp->findByUserIdAndKategoriTransaksiAndTipeTransaksi($userId, KategoriTransaksiConstant::KELUAR, tipeTransaksiKeluarConstant::TRANS_KELUAR_PUSAT_KE_CABANG);
		if (!empty($tempList)) {
			$this->storeHistoriTransaksi($tempList, $userId);
		}
		redirect(BaseUrl::DEFAULT_TRANSAKSI_KELUAR_CABANG_URL);
	}

	private function storeHistoriTransaksi($tempList, $userId)
	{
		$perusahaanId = null;
		$total = 0;
		foreach ($tempList as $temp) {
				$total = $total + $temp->sub_total;
				$perusahaanId = $temp->id_perusahaan;
		}

		$additionalData = array(
			'status' => JenisTransaksiConstant::PERMINTAAN
		);

		$data = array(
			'kategori_transaksi' => $temp->kategori_transaksi,
			'tipe_transaksi' => $temp->tipe_transaksi,
			'id_perusahaan' => $temp->id_perusahaan,
			'total' => $total,
			'id_user' => $userId,
			'status' => StatusTransaksiConstant::WAITING_APPROVED,
			'keterangan' => json_encode($additionalData)
		);

		$save = $this->transactionHistory->save($data);
		if ($save) {
			$this->storeDetailTransaksi($tempList, $userId, $perusahaanId);
		}
	}

	private function storeDetailTransaksi($tempList, $userId, $perusahaanId)
	{
		$historiTransaksi = $this->transactionHistory->findByCategoryTransactionAndTransactionTypeAndUserIdAndPerusahaanIdLimit1(KategoriTransaksiConstant::KELUAR,
			tipeTransaksiKeluarConstant::TRANS_KELUAR_PUSAT_KE_CABANG, $userId, $perusahaanId);
		if (!empty($historiTransaksi)) {
			$transaksiId = $historiTransaksi[0]->id;
			foreach ($tempList as $temp) {
				$data = array(
					'id_transaksi' => $transaksiId,
					'id_barang' => $temp->id_barang,
					'qty' => $temp->qty
				);
				$this->detailTransaksi->save($data);
			}
			$this->temp->deleteByUserIdAndKategoriTransaksiAndTipeTransaksi($userId, KategoriTransaksiConstant::KELUAR, tipeTransaksiKeluarConstant::TRANS_KELUAR_PUSAT_KE_CABANG);
		}
	}

	public function detailTransaksi($id)
	{
		$historiTransaksi = $this->transactionHistory->findById($id);
		$data = array(
			'transaksiOptional' => $historiTransaksi,
		);
		$data = $this->utils->response(MenuConstant::TRANSAKSI_KELUAR_PUSAT_CABANG, $data);

    $this->load->view('transaksi/keluar/pusat/cabang/detail', $data);
	}
}
