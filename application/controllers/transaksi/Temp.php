<?php

class Temp extends CI_Controller
{
	public function __construct()
  {
    parent::__construct();
    $this->load->model('UserModel','user');
		$this->load->model('Utils', 'utils');
		$this->load->model('TransactionHistoryModel','transactionHistory');
		$this->load->model('SupplierModel', 'supplier');
		$this->load->model('TempModel','temp');
		$this->load->model('BarangModel', 'barang');
  }

	public function getTempByUserId()
	{
		$userId = $this->input->post('userId');
		$kategoriTransaksi = $this->input->post('kategoriTransaksi');
		$tipeTransaksi = $this->input->post('tipeTransaksi');
		$tempList = $this->temp->findByUserIdAndKategoriTransaksiAndTipeTransaksi($userId, $kategoriTransaksi, $tipeTransaksi);
		$tempList = $this->getTempJoinBarang($tempList);
		$total = 0;
		if (!empty($tempList)) {
			foreach ($tempList as $temp) {
				$total += $temp->sub_total;
			}
		}

		$data = array(
			'total' => $total,
			'tempList' => $tempList
		);
		echo json_encode($data);
	}

	private function getTempJoinBarang($tempList)
	{
		$idBarangList = [];
		if (count($tempList) != 0) {
			foreach ($tempList as $temp) {
				$idBarangList[] = $temp->id_barang;
			}
		}

		$barangList = array();
		if (count($idBarangList) != null) {
			$barangList = $this->barang->findByIdIn($idBarangList);
		}

		$result = [];
		if (!empty($barangList)) {
			foreach ($tempList as $temp) {
				foreach ($barangList as $barang) {
					if ($temp->id_barang == $barang->id) {
						$data = array(
							'id' => $temp->id,
							'barang' => $barang,
							'qty' => $temp->qty,
							'sub_total' => $temp->sub_total,
							'keterangan' => $temp->keterangan,
							'id_user' => $temp->id_user,
							'id_supplier' => $temp->id_supplier,
							'tipe_transaksi' => $temp->tipe_transaksi,
							'kategori_transaksi' => $temp->kategori_transaksi
						);

						$result[] = $data;
					}
				}
			}
		}

		return $this->utils->convertListToJson($result);
	}

	public function delete()
	{
		$id = $this->input->post('id');
		$success = $this->temp->delete($id);
		$message = array(
			'message' => 'false'
		);
		if ($success) {
			$message = array(
				'message' => 'true'
			);
		}

		echo json_encode($message);
	}

	public function storeTemp()
	{
		$userOptional = $this->utils->getUserProfile();
		$idBarang = $this->input->post('idBarang');
		$qty = $this->input->post('qty');
		$subTotal = $this->input->post('subTotal');
		$keterangan = $this->input->post('keterangan');
		$idUser = $userOptional->id;
		$idSupplier = $this->input->post('idSupplier');
		$tipeTransaksi = $this->input->post('tipeTransaksi');
		$kategoriTransaksi = $this->input->post('kategoriTransaksi');
		$idPerusahaan = $this->input->post('idPerusahaan');
		$idKonsumen = $this->input->post('idKonsumen');

		$barang = $this->barang->findById($idBarang);
		$barang = $barang[0];
		$message = array(
			'message' => 'false'
		);
		if (empty($barang)) {
			$this->session->set_flashdata('warning', FlashData::warning('Data not found'));
			$tempList = $this->temp->findByUserId($userOptional->id);
		}

		if ($barang->id_supplier != $idSupplier) {
			$this->session->set_flashdata('warning', FlashData::warning('Data barang tidak terkait dengan supplier yang di inputkan'));
			$tempList = $this->temp->findByUserId($userOptional->id);
		}

		$data = array(
			'kategori_transaksi' => $kategoriTransaksi,
			'tipe_transaksi' => $tipeTransaksi,
			'id_barang' => $idBarang,
			'qty' => $qty,
			'sub_total' => $subTotal,
			'keterangan' => $keterangan,
			'id_user' => $idUser,
			'id_supplier' => $idSupplier,
			'id_perusahaan' => $idPerusahaan,
			'id_konsumen' => $idKonsumen
		);

		$save = $this->temp->save($data);
		if ($save) {
			$message = array(
				'message' => 'true'
			);
		}

		echo json_encode($message);
	}
}
