<?php

class Histori extends CI_Controller
{
	public function __construct()
  {
    parent::__construct();
    $this->load->model('UserModel','user');
		$this->load->model('Utils', 'utils');
		$this->load->model('TransactionHistoryModel','transactionHistory');
		$this->load->model('SupplierModel', 'supplier');
		$this->load->model('PerusahaanModel','perusahaan');
		$this->load->model('BarangModel', 'barang');
		$this->load->model('TempModel','temp');
		$this->load->model('DetailTransaksiModel', 'detailTransaksi');
		$this->load->model('StokModel', 'stok');
  }

	public function getHistoriTransaksi()
	{
		$perusahaanId = $this->input->post('perusahaanId');
		$status = $this->input->post('status');
		$level = $this->input->post('level');
		$userId = $this->input->post('userId');
		$jenisTransaksiReq = $this->input->post('jenisTransaksi');

		$historiTransaksiList = array();
		if ($level == LevelPerusahaanConstant::PUSAT) {
			if (empty($status)) {
				$historiTransaksiList = $this->transactionHistory->findByPerusahaanId($perusahaanId);
			} else {
				$historiTransaksiList = $this->transactionHistory->findByPerusahaanIdAndStatus($perusahaanId, $status);
			}
		} else {
			if (empty($status)) {
				$historiTransaksiList = $this->transactionHistory->findByUserId($userId);
			} else {
				$historiTransaksiList = $this->transactionHistory->findByUserIdAndStatus($userId, $status);
			}
		}
		
		$hisTransList = [];

		if (empty($jenisTransaksiReq)) {
			$hisTransList = $historiTransaksiList;
		} else {
			if (!empty($historiTransaksiList)) {
				foreach ($historiTransaksiList as $historiTransaksi) {
					$additionalData = json_decode(json_encode($historiTransaksi->keterangan));
					$additionalData = json_decode($additionalData);
					$jenisTransaksi = $additionalData->status;
					if ($jenisTransaksi == $jenisTransaksiReq) {
						$hisTransList[] = $historiTransaksi;
					}
				}
			}
		}

		$data = array(
			'historiTransaksiList' => $hisTransList,
			'jenisTransaksi' => $jenisTransaksiReq
		);

		echo json_encode($data);
	}

	public function addPermintaan()
	{
		$userOptional = $this->utils->getUserProfile();
		$data = $this->utils->response(MenuConstant::TRANSAKSI_MASUK_CABANG);

    $this->load->view('transaksi/masuk/cabang/add', $data);
	}

	public function store()
	{
		$userOptional = $this->utils->getUserProfile();
		$userId = $userOptional->id;
		$tempList = $this->temp->findByUserIdAndKategoriTransaksiAndTipeTransaksi($userId, KategoriTransaksiConstant::MASUK, TipeTransaksiMasukConstant::TRANS_MASUK_CABANG_DARI_PUSAT);
		if (!empty($tempList)) {
			$this->storeHistoriTransaksi($tempList, $userId);
		}
		redirect(BaseUrl::DEFAULT_TRANSAKSI_MASUK_CABANG_URL);
	}

	private function storeHistoriTransaksi($tempList, $userId)
	{
		$perusahaanId = null;
		$total = 0;
		foreach ($tempList as $temp) {
				$total = $total + $temp->sub_total;
				$perusahaanId = $temp->id_perusahaan;
		}

		$additionalData = array(
			'status' => JenisTransaksiConstant::PERMINTAAN
		);

		$data = array(
			'kategori_transaksi' => $temp->kategori_transaksi,
			'tipe_transaksi' => $temp->tipe_transaksi,
			'id_perusahaan' => $temp->id_perusahaan,
			'total' => $total,
			'id_user' => $userId,
			'status' => StatusTransaksiConstant::WAITING_APPROVED,
			'keterangan' => json_encode($additionalData)
		);

		$save = $this->transactionHistory->save($data);
		if ($save) {
			$this->storeDetailTransaksi($tempList, $userId, $perusahaanId);
		}
	}

	private function storeDetailTransaksi($tempList, $userId, $perusahaanId)
	{
		$historiTransaksi = $this->transactionHistory->findByCategoryTransactionAndTransactionTypeAndUserIdAndPerusahaanIdLimit1(KategoriTransaksiConstant::MASUK,
			TipeTransaksiMasukConstant::TRANS_MASUK_CABANG_DARI_PUSAT, $userId, $perusahaanId);
		if (!empty($historiTransaksi)) {
			$transaksiId = $historiTransaksi[0]->id;
			foreach ($tempList as $temp) {
				$data = array(
					'id_transaksi' => $transaksiId,
					'id_barang' => $temp->id_barang,
					'qty' => $temp->qty
				);
				$this->detailTransaksi->save($data);
			}
			$this->temp->deleteByUserIdAndKategoriTransaksiAndTipeTransaksi($userId, KategoriTransaksiConstant::MASUK, TipeTransaksiMasukConstant::TRANS_MASUK_CABANG_DARI_PUSAT);
		}
	}

	public function update()
	{
		$message = false;
		$idTransaksi = $this->input->post('id');
		$historiTransaksiOptional = $this->transactionHistory->findById($idTransaksi);
		if (!empty($historiTransaksiOptional)) {
			$update = $this->transactionHistory->update($idTransaksi);
			if ($update) {
				$historiTransaksiOptional = $historiTransaksiOptional[0];
				$perusahaanId = $historiTransaksiOptional->id_perusahaan;
				$additionalData = json_decode(json_encode($historiTransaksiOptional->keterangan));
				$additionalData = json_decode($additionalData);
				$jenisTransaksi = $additionalData->status;
				$this->getDetailTransaksi($idTransaksi, $perusahaanId, $jenisTransaksi);
				$message = true;
			}
		}

		$data = array(
			'status' => $message
		);
		echo json_encode($data);
	}

	public function getDetailTransaksi($transaksiId, $perusahaanId, $jenisTransaksi)
	{
		$detailList = $this->detailTransaksi->findByTransaksiId($transaksiId);
		if (!empty($detailList)) {
			foreach ($detailList as $detail) {
				$barangId = $detail->id_barang;
				$qty = $detail->qty;
				$this->getStokByPerusahaanAndBarang($perusahaanId, $barangId, $qty, $jenisTransaksi);
			}
		}
	}

	public function getStokByPerusahaanAndBarang($perusahaanId, $barangId, $qty, $jenisTransaksi)
	{
		$stokOptional = $this->stok->findByPerusahaanIdAndBarangId($perusahaanId, $barangId);
		if (empty($stokOptional)) {
			$this->storeStok($perusahaanId, $barangId, $qty);
		} else {
			$stokOptional = $stokOptional[0];
			$stokAwal = (int) $stokOptional->qty;
			$qty = (int) $qty;
			if ($jenisTransaksi == JenisTransaksiConstant::RETUR) {
				$qty = $stokAwal + $qty;
			} else if ($jenisTransaksi == JenisTransaksiConstant::PERMINTAAN) {
				$qty = $stokAwal - $qty;
			}
			
			$this->updateStok($stokOptional->id, $perusahaanId, $barangId, $qty);
		}
	}

	public function storeStok($perusahaanId, $barangId, $qty)
	{
		$data = array(
			'id_perusahaan' => $perusahaanId,
			'id_barang' => $barangId,
			'qty' => $qty
		);

		$this->stok->save($data);
	}

	public function updateStok($id, $perusahaanId, $barangId, $qty)
	{
		$data = array(
			'id' => $id,
			'id_perusahaan' => $perusahaanId,
			'id_barang' => $barangId,
			'qty' => $qty
		);

		$this->stok->update($data);
	}

	public function getDetailHistoriTransaksi()
	{
		$transaksiId = $this->input->post('transaksiId');
		$detailList = $this->detailTransaksi->findByTransaksiId($transaksiId);
		$detailList = $this->getDetailHistoriTransaksiJoinBarang($detailList);
		echo json_encode($detailList);
	}

	private function getDetailHistoriTransaksiJoinBarang($detailList)
	{
		$barangIds = [];
		if (!empty($detailList)) {
			foreach ($detailList as $detail) {
				$barangIds[] = $detail->id_barang;
			}
		}

		$barangList = array();
		if (!empty($barangIds)) {
			$barangList = $this->barang->findByIdIn($barangIds);
		}

		$result = [];
		if (!empty($barangList)) {
			foreach ($detailList as $detail) {
				foreach ($barangList as $barang) {
					if ($detail->id_barang == $barang->id) {
						$data = array(
							'id' => $detail->id,
							'barang' => $barang,
							'id_transaksi' => $detail->id_transaksi,
							'qty' => $detail->qty,
							'sub_total' => $detail->sub_total
						);

						$result [] = $data;
					}
				}
			}
		}

		return $this->utils->convertListToJson($result);
	}

	public function prosesApproved()
	{
		$idTransaksi = $this->input->post('transaksiId');
		$tipeTransaksi = $this->input->post('tipeTransaksi');
		$historiTransaksiOptional = $this->transactionHistory->findById($idTransaksi);
		if (!empty($historiTransaksiOptional)) {
			$update = $this->transactionHistory->update($idTransaksi);
			if ($update) {
				$historiTransaksiOptional = $historiTransaksiOptional[0];
				$perusahaanId = $historiTransaksiOptional->id_perusahaan;
				$additionalData = json_decode(json_encode($historiTransaksiOptional->keterangan));
				$additionalData = json_decode($additionalData);
				$jenisTransaksi = $additionalData->status;
				$this->getDetailTransaksi($idTransaksi, $perusahaanId, $jenisTransaksi);
			}
		}

		$baseUrl = "";
		if ($tipeTransaksi == TipeTransaksiMasukConstant::TRANS_MASUK_PUSAT_DARI_RETUR) {
			$baseUrl = BaseUrl::DEFAULT_TRANSAKSI_MASUK_RETUR_URL;
		} else if ($tipeTransaksi == tipeTransaksiKeluarConstant::TRANS_KELUAR_PUSAT_KE_CABANG) {
			$baseUrl = BaseUrl::DEFAULT_TRANSAKSI_KELUAR_CABANG_URL;
		}
		
		redirect($baseUrl);
	}
}
