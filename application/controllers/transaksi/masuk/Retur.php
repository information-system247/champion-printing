<?php

class Retur extends CI_Controller
{
	public function __construct()
  {
    parent::__construct();
    $this->load->model('UserModel','user');
		$this->load->model('Utils', 'utils');
		$this->load->model('TransactionHistoryModel','transactionHistory');
		$this->load->model('PerusahaanModel','perusahaan');
  }

	public function index()
	{
		$data = $this->utils->response(MenuConstant::TRANSAKSI_MASUK_RETUR);

    $this->load->view('transaksi/masuk/pusat/retur/list', $data);
	}

	private function transactionHistoryJoinPerusahaan($transactionHistoryList)
	{
		$perusahaanIds = [];
		if (count($transactionHistoryList) > 0) {
			foreach ($transactionHistoryList as $transactionHistory) {
				$perusahaanIds[] = $transactionHistory->id_perusahaan;
			}
		}

		$perusahaanList = array();
		if (!empty($perusahaanIds)) {
			$perusahaanList = $this->perusahaan->findByIdIn($perusahaanIds);
		}

		$result = array();
		if (count($perusahaanList) > 0) {
			foreach ($transactionHistoryList as $transactionHistory) {
				foreach ($perusahaanList as $perusahaan) {
					if ($transactionHistoryList->id_perusahaan == $perusahaan->id) {
						$data = array(
							'id' => $transactionHistory->id,
							'tgl_transaksi' => $transactionHistory->tgl_transaksi,
							'kategori_transaksi' => $transactionHistory->kategori_transaksi,
							'tipe_transaksi' => $transactionHistory->tipe_transaksi,
							'id_supplier' => $transactionHistory->id_supplier,
							'perusahaan' => $perusahaan,
							'id_konsumen' => $transactionHistory->id_konsumen,
							'total' => $transactionHistory->total,
							'ketentuan' => $transactionHistory->ketentuan,
							'id_user' => $transactionHistory->id_user
						);

						$result[] = $data;
					}
				}
			}
		}

		return $this->utils->convertListToJson($result);
	}

	public function detailTransaksi($id)
	{
		$historiTransaksi = $this->transactionHistory->findById($id);
		$data = array(
			'transaksiOptional' => $historiTransaksi,
		);
		$data = $this->utils->response(MenuConstant::TRANSAKSI_MASUK_RETUR, $data);

    $this->load->view('transaksi/masuk/pusat/retur/detail', $data);
	}
}
