<?php

class Cabang extends CI_Controller
{
	public function __construct()
  {
    parent::__construct();
    $this->load->model('UserModel','user');
		$this->load->model('Utils', 'utils');
		$this->load->model('TransactionHistoryModel','transactionHistory');
		$this->load->model('PerusahaanModel','perusahaan');
  }

	public function index()
	{
		$userOptional = $this->utils->getUserProfile();
		$transactionHistoryList = $this->transactionHistory->findByCategoryTransactionAndTransactionType(KategoriTransaksiConstant::MASUK, TipeTransaksiMasukConstant::TRANS_MASUK_PUSAT_DARI_RETUR);
		$transactionHistoryList = $this->transactionHistoryJoinPerusahaan($transactionHistoryList);

		$data = array(
			'transactionHistoryList' => $transactionHistoryList
		);
		$data = $this->utils->response(MenuConstant::TRANSAKSI_MASUK_CABANG, $data);

    $this->load->view('transaksi/masuk/cabang/list', $data);
	}

	private function transactionHistoryJoinPerusahaan($transactionHistoryList)
	{
		$perusahaanIds = [];
		if (count($transactionHistoryList) > 0) {
			foreach ($transactionHistoryList as $transactionHistory) {
				$perusahaanIds[] = $transactionHistory->id_perusahaan;
			}
		}

		$perusahaanList = array();
		if (!empty($perusahaanIds)) {
			$perusahaanList = $this->perusahaan->findByIdIn($perusahaanIds);
		}

		$result = array();
		if (count($perusahaanList) > 0) {
			foreach ($transactionHistoryList as $transactionHistory) {
				foreach ($perusahaanList as $perusahaan) {
					if ($transactionHistoryList->id_perusahaan == $perusahaan->id) {
						$data = array(
							'id' => $transactionHistory->id,
							'tgl_transaksi' => $transactionHistory->tgl_transaksi,
							'kategori_transaksi' => $transactionHistory->kategori_transaksi,
							'tipe_transaksi' => $transactionHistory->tipe_transaksi,
							'id_supplier' => $transactionHistory->id_supplier,
							'perusahaan' => $perusahaan,
							'id_konsumen' => $transactionHistory->id_konsumen,
							'total' => $transactionHistory->total,
							'ketentuan' => $transactionHistory->ketentuan,
							'id_user' => $transactionHistory->id_user
						);

						$result[] = $data;
					}
				}
			}
		}

		return $this->utils->convertListToJson($result);
	}

	public function detailTransaksi($id)
	{
		$historiTransaksi = $this->transactionHistory->findById($id);
		$data = array(
			'transaksiOptional' => $historiTransaksi,
		);
		$data = $this->utils->response(MenuConstant::TRANSAKSI_MASUK_CABANG, $data);

    $this->load->view('transaksi/masuk/cabang/detail', $data);
	}
}
