<?php

class Supplier extends CI_Controller
{
	public function __construct()
  {
    parent::__construct();
    $this->load->model('UserModel','user');
		$this->load->model('Utils', 'utils');
		$this->load->model('TransactionHistoryModel','transactionHistory');
		$this->load->model('SupplierModel', 'supplier');
		$this->load->model('TempModel','temp');
		$this->load->model('BarangModel', 'barang');
		$this->load->model('DetailTransaksiModel', 'detailTransaksi');
		$this->load->model('StokModel', 'stok');
  }

	public function index()
	{
		$userOptional = $this->utils->getUserProfile();
		$transactionHistoryList = $this->transactionHistory->findByCategoryTransactionAndTransactionType(KategoriTransaksiConstant::MASUK, TipeTransaksiMasukConstant::TRANS_MASUK_PUSAT_DARI_SUPPLIER);
		$transactionHistoryList = $this->transactionHistoryJoinSupplier($transactionHistoryList);

		$data = array(
			'transactionHistoryList' => $transactionHistoryList
		);
		$data = $this->utils->response(MenuConstant::TRANSAKSI_MASUK_SUPPLIER, $data);

    $this->load->view('transaksi/masuk/pusat/supplier/list', $data);
	}

	private function transactionHistoryJoinSupplier($transactionHistoryList)
	{
		$supplierIds = [];
		if (count($transactionHistoryList) > 0) {
			foreach ($transactionHistoryList as $transactionHistory) {
				$supplierIds[] = $transactionHistory->id_supplier;
			}
		}

		$supplierList = array();
		if (!empty($supplierIds)) {
			$supplierList = $this->supplier->findByIdIn($supplierIds);
		}

		$result = array();
		if (count($supplierList) > 0) {
			if (count($transactionHistoryList) == 1) {
				$transactionHistoryList = $transactionHistoryList[0];
				foreach ($supplierList as $supplier) {
					if ($transactionHistoryList->id_supplier == $supplier->id) {
						$data = array(
							'id' => $transactionHistoryList->id,
							'tgl_transaksi' => $transactionHistoryList->tgl_transaksi,
							'kategori_transaksi' => $transactionHistoryList->kategori_transaksi,
							'tipe_transaksi' => $transactionHistoryList->tipe_transaksi,
							'supplier' => $supplier,
							'id_perusahaan' => $transactionHistoryList->id_perusahaan,
							'id_konsumen' => $transactionHistoryList->id_konsumen,
							'total' => $transactionHistoryList->total,
							'keterangan' => $transactionHistoryList->keterangan,
							'id_user' => $transactionHistoryList->id_user,
							'status' => $transactionHistoryList->status
						);

						$result[] = $data;
					}
				}
			} else {
				foreach ($transactionHistoryList as $transactionHistory) {
					foreach ($supplierList as $supplier) {
						if ($transactionHistory->id_supplier == $supplier->id) {
							$data = array(
								'id' => $transactionHistory->id,
								'tgl_transaksi' => $transactionHistory->tgl_transaksi,
								'kategori_transaksi' => $transactionHistory->kategori_transaksi,
								'tipe_transaksi' => $transactionHistory->tipe_transaksi,
								'supplier' => $supplier,
								'id_perusahaan' => $transactionHistory->id_perusahaan,
								'id_konsumen' => $transactionHistory->id_konsumen,
								'total' => $transactionHistory->total,
								'keterangan' => $transactionHistory->keterangan,
								'id_user' => $transactionHistory->id_user,
								'status' => $transactionHistory->status
							);
	
							$result[] = $data;
						}
					}
				}
			}
		}

		return $this->utils->convertListToJson($result);
	}

	public function add()
	{
		$userOptional = $this->utils->getUserProfile();
		$tempList = $this->temp->findByUserIdAndKategoriTransaksiAndTipeTransaksi($userOptional->id, KategoriTransaksiConstant::MASUK, TipeTransaksiMasukConstant::TRANS_MASUK_PUSAT_DARI_SUPPLIER);
		$supplierList = array();
		$barangList = array();
		$total = 0;
		if (!empty($tempList)) {
			foreach ($tempList as $temp) {
				$total += $temp->sub_total;
			}

			$barangList = $this->barang->findByIdSupplier($temp->id_supplier);
			$supplierList = $this->supplier->findById($temp->id_supplier);
		} else {
			$supplierList = $this->supplier->findAll();
		}
		
		$data = array(
			'supplierList' => $supplierList,
			'tempList' => $tempList,
			'total' => $total,
			'barangList' => $barangList
		);

		$data = $this->utils->response(MenuConstant::TRANSAKSI_MASUK_SUPPLIER, $data);

    $this->load->view('transaksi/masuk/pusat/supplier/add', $data);
	}

	public function store()
	{
		$userOptional = $this->utils->getUserProfile();
		$userId = $userOptional->id;
		$tempList = $this->temp->findByUserIdAndKategoriTransaksiAndTipeTransaksi($userId, KategoriTransaksiConstant::MASUK, TipeTransaksiMasukConstant::TRANS_MASUK_PUSAT_DARI_SUPPLIER);
		if (!empty($tempList)) {
			$this->storeHistoriTransaksi($tempList, $userOptional);
		}
		redirect(BaseUrl::DEFAULT_TRANSAKSI_MASUK_SUPPLIER_URL);
	}

	private function storeHistoriTransaksi($tempList, $userOptional)
	{
		$supplierId = null;
		$total = 0;
		foreach ($tempList as $temp) {
				$total = $total + $temp->sub_total;
				$supplierId = $temp->id_supplier;
		}

		$userId = $userOptional->id;
		$data = array(
			'kategori_transaksi' => $temp->kategori_transaksi,
			'tipe_transaksi' => $temp->tipe_transaksi,
			'id_supplier' => $temp->id_supplier,
			'total' => $total,
			'id_user' => $userId,
			'status' => StatusTransaksiConstant::APPROVED
		);

		$save = $this->transactionHistory->save($data);
		if ($save) {
			$this->storeDetailTransaksi($tempList, $userId, $supplierId, $userOptional);
		}
	}

	private function storeDetailTransaksi($tempList, $userId, $supplierId, $userOptional)
	{
		$historiTransaksi = $this->transactionHistory->findByCategoryTransactionAndTransactionTypeAndUserIdAndSupplierIdLimit1(KategoriTransaksiConstant::MASUK,
			TipeTransaksiMasukConstant::TRANS_MASUK_PUSAT_DARI_SUPPLIER, $userId, $supplierId);
		if (!empty($historiTransaksi)) {
			$transaksiId = $historiTransaksi[0]->id;
			$perusahaanId = $userOptional->karyawan->perusahaan->id;
			foreach ($tempList as $temp) {
				$barangId = $temp->id_barang;
				$qty = $temp->qty;
				$data = array(
					'id_transaksi' => $transaksiId,
					'id_barang' => $barangId,
					'qty' => $qty,
					'sub_total' => $temp->sub_total
				);

				$save = $this->detailTransaksi->save($data);
				if ($save) {
					$this->getStokByPerusahaanAndBarang($perusahaanId, $barangId, $qty);
				}
			}
			$this->temp->deleteByUserIdAndKategoriTransaksiAndTipeTransaksi($userId, KategoriTransaksiConstant::MASUK, TipeTransaksiMasukConstant::TRANS_MASUK_PUSAT_DARI_SUPPLIER);
		}
	}

	public function getStokByPerusahaanAndBarang($perusahaanId, $barangId, $qty)
	{
		$stokOptional = $this->stok->findByPerusahaanIdAndBarangId($perusahaanId, $barangId);
		if (empty($stokOptional)) {
			$this->storeStok($perusahaanId, $barangId, $qty);
		} else {
			$stokOptional = $stokOptional[0];
			$stokAwal = (int) $stokOptional->qty;
			$qty = (int) $qty + $stokAwal;
			$this->updateStok($stokOptional->id, $perusahaanId, $barangId, $qty);
		}
	}

	public function storeStok($perusahaanId, $barangId, $qty)
	{
		$data = array(
			'id_perusahaan' => $perusahaanId,
			'id_barang' => $barangId,
			'qty' => $qty
		);

		$this->stok->save($data);
	}

	public function updateStok($id, $perusahaanId, $barangId, $qty)
	{
		$data = array(
			'id' => $id,
			'id_perusahaan' => $perusahaanId,
			'id_barang' => $barangId,
			'qty' => $qty
		);

		$this->stok->update($data);
	}

	public function detailTransaksi($id)
	{
		$historiTransaksi = $this->transactionHistory->findById($id);
		$data = array(
			'transaksiOptional' => $historiTransaksi,
		);
		$data = $this->utils->response(MenuConstant::TRANSAKSI_MASUK_SUPPLIER, $data);

    $this->load->view('transaksi/masuk/pusat/supplier/detail', $data);
	}
}
