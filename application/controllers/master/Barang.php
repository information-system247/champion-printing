<?php 

class Barang extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('UserModel','user');
    $this->load->model('BarangModel','barang');
    $this->load->model('KategoriBarangModel', 'kategori');
    $this->load->model('TipeBarangModel', 'tipe');
    $this->load->model('SupplierModel', 'supplier');
		$this->load->model('Utils', 'utils');
  }

  public function index()
  {
    $userOptional = $this->utils->getUserProfile();
    $barangList = $this->barang->findAll();
    $kategoriList = $this->kategori->findAll();
    $tipeList = $this->tipe->findAll();
    $supplierList = $this->supplier->findAll();
    $barangList = $this->getBarangJoinKategori($barangList, $kategoriList);
    $barangList = $this->getBarangJoinTipe($barangList, $tipeList);
    $barangList = $this->getBarangJoinSupplier($barangList, $supplierList);

    $data = array(
      'barangList' => $barangList,
      'kategoriList' => $kategoriList,
      'tipeList' => $tipeList,
      'supplierList' => $supplierList
    );
		$data = $this->utils->response(MenuConstant::BARANG, $data);

    $this->load->view('barang/list', $data);
  }

  public function getBarangJoinKategori($barangList, $kategoriList)
  {
    $barangs = array();
    if (!empty($kategoriList)) {
      if (!empty($barangList)) {
        foreach ($barangList as $barang) {
          foreach ($kategoriList as $kategori) {
            if ($barang->id_kategori == $kategori->id) {
              $barangs[] = array(
                'id' => $barang->id,
                'kategori' => $kategori,
                'id_tipe' => $barang->id_tipe,
                'nama_barang' => $barang->nama_barang,
                'sk_diskon' => $barang->sk_diskon,
                'diskon' => $barang->diskon,
                'harga_jual' => $barang->harga_jual,
                'id_supplier' => $barang->id_supplier,
                'satuan' => $barang->satuan,
                'is_deleted' => $barang->is_deleted
              );
            }
          }
        }
      }
    }

    return $this->utils->convertListToJson($barangs);
  }

  public function getBarangJoinTipe($barangList, $tipeList)
  {
    $barangs = array();
    if (!empty($tipeList)) {
      if (!empty($barangList)) {
        foreach ($barangList as $barang) {
          foreach ($tipeList as $tipe) {
            if ($barang->id_tipe == $tipe->id) {
              $barangs[] = array(
                'id' => $barang->id,
                'kategori' => $barang->kategori,
                'tipe' => $tipe,
                'nama_barang' => $barang->nama_barang,
                'sk_diskon' => $barang->sk_diskon,
                'diskon' => $barang->diskon,
                'harga_jual' => $barang->harga_jual,
                'id_supplier' => $barang->id_supplier,
                'satuan' => $barang->satuan,
                'is_deleted' => $barang->is_deleted
              );
            }
          }
        }
      }
    }

    return $this->utils->convertListToJson($barangs);
  }

  public function getBarangJoinSupplier($barangList, $supplierList)
  {
    $barangs = array();
    if (!empty($supplierList)) {
      if (!empty($barangList)) {
        foreach ($barangList as $barang) {
          foreach ($supplierList as $supplier) {
            if ($barang->id_supplier == $supplier->id) {
              $barangs[] = array(
                'id' => $barang->id,
                'kategori' => $barang->kategori,
                'tipe' => $barang->tipe,
                'nama_barang' => $barang->nama_barang,
                'sk_diskon' => $barang->sk_diskon,
                'diskon' => $barang->diskon,
                'harga_jual' => $barang->harga_jual,
                'supplier' => $supplier,
                'satuan' => $barang->satuan,
                'is_deleted' => $barang->is_deleted
              );
            }
          }
        }
      }
    }

    return $this->utils->convertListToJson($barangs);
  }

  public function store()
  {
    $idKategori = $this->input->post('idKategori');
    $idTipe = $this->input->post('idTipe');
    $namaBarang = $this->input->post('namaBarang');
    $skDiskon = $this->input->post('skDiskon');
    $diskon = $this->input->post('diskon');
    $hargaJual = $this->input->post('hargaJual');
    $idSupplier = $this->input->post('idSupplier');
    $satuan = $this->input->post('satuan');
    
    $data = array(
      'id_kategori' => $idKategori,
      'id_tipe' => $idTipe,
      'nama_barang' => $namaBarang,
      'sk_diskon' => $skDiskon,
      'diskon' => $diskon,
      'harga_jual' => $hargaJual,
      'id_supplier' => $idSupplier,
      'satuan' => $satuan,
      'is_deleted' => false
    );

		$result = $this->barang->save($data);
		$this->utils->storeDataValidation($result);

    redirect(BaseUrl::DEFAULT_BARANG_URL);
  }

  public function update()
  {
    $id = $this->input->post('id');
    $idKategori = $this->input->post('idKategori');
    $idTipe = $this->input->post('idTipe');
    $namaBarang = $this->input->post('namaBarang');
    $skDiskon = $this->input->post('skDiskon');
    $diskon = $this->input->post('diskon');
    $hargaJual = $this->input->post('hargaJual');
    $idSupplier = $this->input->post('idSupplier');
    $satuan = $this->input->post('satuan');

		$result = $this->barang->update($id, $idKategori, $idTipe, $namaBarang, $skDiskon, $diskon, $hargaJual, $idSupplier, $satuan, false);
		$this->utils->storeDataValidation($result);

    redirect(BaseUrl::DEFAULT_BARANG_URL);
  }

  public function delete()
  {
    $id = $this->input->post('id');
    $barangOptional = $this->barang->findById($id);
    if (empty($barangOptional)) {
      $this->session->set_flashdata('warning', FlashData::warning('Data not found'));
      redirect(BaseUrl::DEFAULT_BARANG_URL);
    }

		$result = $this->barang->update($id, $barangOptional[0]->id_kategori, $barangOptional[0]->id_tipe, $barangOptional[0]->nama_barang, $barangOptional[0]->sk_diskon, 
			$barangOptional[0]->diskon, $barangOptional[0]->harga_jual, $barangOptional[0]->id_supplier, $barangOptional[0]->satuan, true);
		$this->utils->storeDataValidation($result, true);

    redirect(BaseUrl::DEFAULT_BARANG_URL);
  }

	public function getBarangBySupplier()
	{
		$idSupplier = $this->input->post('id');
		$barangList = $this->barang->findByIdSupplier($idSupplier);
		echo json_encode($barangList);
	}

	public function getBarangById()
	{
		$idBarang = $this->input->post('id');
		$barangOptional = $this->barang->findById($idBarang);

		echo json_encode($barangOptional[0]);
	}

	public function getBarangList()
	{
		$barangList = $this->barang->findAll();
		$data = array(
			'barangList' => $barangList
		);
		echo json_encode($data);
	}
}
