<?php 

class TipeBarang extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('TipeBarangModel','tipe');
    $this->load->model('UserModel','user');
		$this->load->model('Utils', 'utils');
  }

  public function index()
  {
    $userOptional = $this->utils->getUserProfile();
    $tipeList = $this->tipe->findAll();
    $data = array(
      'tipeList' => $tipeList
    );
		$data = $this->utils->response(MenuConstant::TIPE_BARANG, $data);

    $this->load->view('tipe_barang/list', $data);
  }

  public function store()
  {
    $this->utils->getUserProfile();
    $namaTipe = $this->input->post('namaTipe');
    $data = array(
      'nama_tipe' => $namaTipe
    );

		$result = $this->tipe->save($data);
    $this->utils->storeDataValidation($result);

    redirect(BaseUrl::DEFAULT_TIPE_BARANG_URL);
  }

  public function update()
  {
    $id = $this->input->post('id');
    $namaTipe = $this->input->post('namaTipe');
    $tipeOptional = $this->tipe->findById($id);
    if (empty($tipeOptional)) {
      $this->session->set_flashdata('warning', FlashData::warning('Data not found'));
      redirect(BaseUrl::DEFAULT_TIPE_BARANG_URL);
    }
		
		$result = $this->tipe->update($id, $namaTipe);
    $this->utils->storeDataValidation($result);

    redirect(BaseUrl::DEFAULT_TIPE_BARANG_URL);
  }

  public function delete()
  {
    $id = $this->input->post('id');
    $tipeOptional = $this->tipe->findById($id);
    if (empty($tipeOptional)) {
      $this->session->set_flashdata('warning', FlashData::warning('Data not found'));
      redirect(BaseUrl::DEFAULT_TIPE_BARANG_URL);
    }

		$result = $this->tipe->delete($id);
    $this->utils->storeDataValidation($result, true);

    redirect(BaseUrl::DEFAULT_TIPE_BARANG_URL);
  }
}
