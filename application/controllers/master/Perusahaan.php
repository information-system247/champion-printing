<?php
class Perusahaan extends CI_Controller
{
  public function __construct() {
    parent::__construct();
    $this->load->model("PerusahaanModel",'perusahaan');
    $this->load->model("KaryawanModel",'karyawan');
    $this->load->model('UserModel','user');
		$this->load->model('Utils', 'utils');
  }

  public function index() {
    $userOptional = $this->utils->getUserProfile();
    $perusahaanList = $this->perusahaan->findAll();
    
    $data = array(
      'perusahaanList' => $perusahaanList
    );
		$data = $this->utils->response(MenuConstant::PERUSAHAAN,$data);

    $this->load->view('perusahaan/list', $data);
  }

  public function store()
  {
    $this->utils->getUserProfile();
    $nama = $this->input->post('namaLengkap');
    $alamat = $this->input->post('alamat');
    $telepon = $this->input->post('telepon');
    $level = $this->input->post('level');

    if (LevelPerusahaanConstant::PUSAT == $level) {
      $perusahaanOptional = $this->perusahaan->findByLevel(LevelPerusahaanConstant::PUSAT);
      if (!empty($perusahaanOptional)) {
        $this->session->set_flashdata('warning', FlashData::warning('Perusahaan dengan level pusat sudah ada'));
        redirect(BaseUrl::DEFAULT_PERUSAHAAN_URL);
      }
    }
    
    $data = array(
      'nama' => $nama,
      'alamat' => $alamat,
      'telepon' => $telepon,
      'level' => $level
    );

    $store = $this->perusahaan->save($data);
    $this->utils->storeDataValidation($store);

    redirect(BaseUrl::DEFAULT_PERUSAHAAN_URL);
  }

  public function update()
  {
    $this->utils->getUserProfile();
    $id = $this->input->post('id');
    $nama = $this->input->post('namaLengkap');
    $alamat = $this->input->post('alamat');
    $telepon = $this->input->post('telepon');
    $level = $this->input->post('level');

    if (LevelPerusahaanConstant::PUSAT == $level) {
      $perusahaanOptional = $this->perusahaan->findByLevel(LevelPerusahaanConstant::PUSAT);
      if (!empty($perusahaanOptional) && $perusahaanOptional[0]->level != $level) {
        $this->session->set_flashdata('warning', FlashData::warning('Perusahaan dengan level pusat sudah ada'));
        redirect(BaseUrl::DEFAULT_PERUSAHAAN_URL);
      }
    }

    $store = $this->perusahaan->update($id, $nama, $alamat, $telepon, $level);
    $this->utils->storeDataValidation($store);
    
    redirect(BaseUrl::DEFAULT_PERUSAHAAN_URL);
  }

  public function delete()
  {
		$result = false;
    $this->utils->getUserProfile();
    $id = $this->input->post('id');
    $karyawanList = $this->karyawan->findByPerusahaanId($id);
    if (count($karyawanList) == 0) {
			$result = $this->perusahaan->delete($id);
    }

		$this->utils->storeDataValidation($result, true);
		redirect(BaseUrl::DEFAULT_PERUSAHAAN_URL);
  }

	public function getPerusahaan()
	{
		$perusahaanId = $this->input->post('perusahaanId');
		$perusahaanList = array();
		if (empty($perusahaanId)) {
			$perusahaanList = $this->perusahaan->findAll();
		} else {
			$perusahaanList = $this->perusahaan->findById($perusahaanId);
		}

		$data = array(
			'perusahaanList' => $perusahaanList
		);
		echo json_encode($data);
	}

	public function getPerusahaanPusat()
	{
		$perusahaanOptional = $this->perusahaan->findByLevel(LevelPerusahaanConstant::PUSAT);
		echo json_encode($perusahaanOptional);
	}

	public function getPerusahaanList()
	{
		$perusahaanList = $this->perusahaan->findAll();
		echo json_encode($perusahaanList);
	}
}
