<?php
class Stok extends CI_Controller
{
  public function __construct() {
    parent::__construct();
    $this->load->model("PerusahaanModel",'perusahaan');
		$this->load->model('Utils', 'utils');
		$this->load->model('StokModel', 'stok');
		$this->load->model('BarangModel', 'barang');
  }

  public function index() {
    $userOptional = $this->utils->getUserProfile();
		$data = $this->utils->response(MenuConstant::STOK);

    $this->load->view('stok/list', $data);
  }

	public function getStok()
	{
		$idPerusahaan = $this->input->post('idPerusahaan');
		$stokList = $this->stok->findByPerusahaanId($idPerusahaan);
		$stokList = $this->getStokJoinBarang($stokList);
		$data = array(
			'stokList' => $stokList
		);
		echo json_encode($data);
	}

	private function getStokJoinBarang($stokList)
	{
		$barangIds = [];
		if (!empty($stokList)) {
			foreach ($stokList as $stok) {
				$barangIds[] = $stok->id_barang;
			}
		}

		$barangList = array();
		if (!empty($barangIds)) {
			$barangList = $this->barang->findByIdIn($barangIds);
		}

		$result = [];
		if (!empty($barangList)) {
			foreach ($stokList as $stok) {
				foreach ($barangList as $barang) {
					if ($stok->id_barang == $barang->id) {
						$data = array(
							'id' => $stok->id,
							'id_perusahaan' => $stok->id_perusahaan,
							'barang' => $barang,
							'qty' => $stok->qty
						);

						$result[] = $data;
					}
				}
			}
		}

		return $this->utils->convertListToJson($result);
	}
}
