<?php

class Karyawan extends CI_Controller
{
  public function __construct() {
    parent::__construct();
    $this->load->model("PerusahaanModel",'perusahaan');
    $this->load->model("KaryawanModel",'karyawan');
    $this->load->model('UserModel','user');
    $this->load->model('PosisiKaryawanModel', 'posisi');
		$this->load->model('Utils', 'utils');
  }

  public function index()
  {
    $userOptional = $this->utils->getUserProfile();
		$karyawanIdList = array($userOptional->karyawan->id);
    $karyawanList = $this->karyawan->findByIdNotIn($karyawanIdList);
    $perusahaanList = $this->perusahaan->findAll();
    $posisiList = $this->posisi->findAll();
    $karyawanList = $this->getKaryawanJoinPerusahaan($karyawanList, $perusahaanList);
    $karyawanList = $this->getKaryawanJoinPosisi($karyawanList, $posisiList);

    $data = array(
      'karyawanList' => $karyawanList,
      'perusahaanList' => $perusahaanList,
      'posisiList' => $posisiList
    );
		$data = $this->utils->response(MenuConstant::KARYAWAN, $data);

    $this->load->view('karyawan/list', $data);
  }

  public function getKaryawanJoinPerusahaan($karyawanList, $perusahaanList)
  {
    $response = array();
    if (!empty($karyawanList)) {
      foreach ($karyawanList as $karyawan) {
        foreach ($perusahaanList as $perusahaan) {
          if ($karyawan->id_perusahaan == $perusahaan->id) {
            $data = array(
              'id' => $karyawan->id,
              'nama' => $karyawan->nama,
              'alamat' => $karyawan->alamat,
              'perusahaan' => $perusahaan,
              'telepon' => $karyawan->telepon,
              'id_posisi' => $karyawan->id_posisi
            );
            $response[] = $data;
          }
        }
      }
    }

		return $this->utils->convertListToJson($response);
  }

  public function getKaryawanJoinPosisi($karyawanList, $posisiList)
  {
    $response = array();
    if (!empty($karyawanList)) {
      foreach ($karyawanList as $karyawan) {
        foreach ($posisiList as $posisi) {
          if ($karyawan->id_posisi == $posisi->id) {
            $data = array(
              'id' => $karyawan->id,
              'nama' => $karyawan->nama,
              'alamat' => $karyawan->alamat,
              'perusahaan' => $karyawan->perusahaan,
              'telepon' => $karyawan->telepon,
              'posisi' => $posisi
            );
            $response[] = $data;
          }
        }
      }
    }

    return $this->utils->convertListToJson($response);
  }

  public function store()
  {
    $this->utils->getUserProfile();
    $namaLengkap = $this->input->post('namaLengkap');
    $alamat = $this->input->post('alamat');
    $telepon = $this->input->post('telepon');
    $idPerusahaan = $this->input->post('idPerusahaan');
    $idPosisi = $this->input->post('idPosisi');
    $posisiOptional = $this->posisi->findByNama('admin');

    if (!empty($posisiOptional) && $posisiOptional[0]->id == $idPosisi) {
      $karyawanList = $this->karyawan->findByPerusahaanIdAndPosisiId($idPerusahaan);
      if (!empty($karyawanList)) {
        $this->session->set_flashdata('warning', 'Posisi admin sudah tersedia');
        redirect(BaseUrl::DEFAULT_KARYAWAN_URL);
      }
    }

    $data = array(
      "nama" => $namaLengkap,
      "alamat" => $alamat,
      "id_perusahaan" => $idPerusahaan,
      "telepon" => $telepon,
      'id_posisi' => $idPosisi
    );

    $store = $this->karyawan->save($data);
    $this->utils->storeDataValidation($store);

    redirect(BaseUrl::DEFAULT_KARYAWAN_URL);
  }

  public function update()
  {
    $this->utils->getUserProfile();
    $id = $this->input->post('id');
    $namaLengkap = $this->input->post('namaLengkap');
    $alamat = $this->input->post('alamat');
    $telepon = $this->input->post('telepon');
    $idPerusahaan = $this->input->post('idPerusahaan');
    $idPosisi = $this->input->post('idPosisi');
    $karyawanOptional = $this->karyawan->findById($id);
    $posisiOptional = $this->posisi->findByNama('admin');

    if (!empty($posisiOptional) && $posisiOptional[0]->id == $idPosisi && $karyawanOptional[0]->id_posisi != $idPosisi) {
      $karyawanList = $this->karyawan->findByPerusahaanIdAndPosisiId($idPerusahaan);
      if (!empty($karyawanList)) {
        $this->session->set_flashdata('warning', 'Posisi admin sudah tersedia');
        redirect(BaseUrl::DEFAULT_KARYAWAN_URL);
      }
    }

    $store = $this->karyawan->update($id, $namaLengkap, $alamat, $idPerusahaan, $telepon, $idPosisi);
    $this->utils->storeDataValidation($store);

    redirect(BaseUrl::DEFAULT_KARYAWAN_URL);
  }

  public function delete()
  {
		$result = false;
    $this->utils->getUserProfile();
    $id = $this->input->post('id');
    $karyawanOptional = $this->karyawan->findById($id);
    if (!empty($karyawanOptional)) {
      $userOptional = $this->user->findByIdKaryawan($karyawanOptional[0]->id_karyawan);
      if (count($userOptional) == 0) {
				$result = $this->karyawan->delete($id);
      }
    }
    
		$this->utils->storeDataValidation($result, true);
    redirect(BaseUrl::DEFAULT_KARYAWAN_URL);
  }
}
