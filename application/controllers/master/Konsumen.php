<?php 

class Konsumen extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('KonsumenModel','konsumen');
    $this->load->model('UserModel','user');
		$this->load->model('Utils', 'utils');
  }

	// ================================== CUSTOMER ==================================

  public function customer()
  {
    $userOptional = $this->utils->getUserProfile();
    $customerList = $this->konsumen->findByKategori(KonsumenKategoriConstant::CUSTOMER);

    $data = array(
      'customerList' => $customerList
    );
		$data = $this->utils->response(MenuConstant::KONSUMEN_CUSTOMER, $data);

    $this->load->view('konsumen/customer/list', $data);
  }

  public function customerStore()
  {
    $this->utils->getUserProfile();
    $namaCustomer = $this->input->post('namaCustomer');

    $data = array(
      'kategori' => KonsumenKategoriConstant::CUSTOMER,
      'nama_konsumen' => $namaCustomer,
      'create_at' => DateUtils::getDateNow()
    );

    $store = $this->konsumen->save($data);
    $this->utils->storeDataValidation($store);

    redirect(BaseUrl::DEFAULT_KONSUMEN_CUSTOMER_URL);
  }

  public function customerUpdate()
  {
    $userOptional = $this->utils->getUserProfile();
    $id = $this->input->post('id');
    $namaCustomer = $this->input->post('namaCustomer');

    $customerOptional = $this->konsumen->findById($id);
    if (empty($customerOptional)) {
      $this->session->set_flashdata('warning', FlashData::warning('Data not found'));
      redirect(BaseUrl::DEFAULT_KONSUMEN_CUSTOMER_URL);
    }

    $store = $this->konsumen->update($id, $customerOptional[0]->kategori, $namaCustomer, $userOptional->username);
    $this->utils->storeDataValidation($store);

    redirect(BaseUrl::DEFAULT_KONSUMEN_CUSTOMER_URL);
  }

  public function customerDelete()
  {
    $this->utils->getUserProfile();
    $id = $this->input->post('id');
    $customerOptional = $this->konsumen->findById($id);
    if (empty($customerOptional)) {
      $this->session->set_flashdata('warning', FlashData::warning('Data not found'));
      redirect(BaseUrl::DEFAULT_KONSUMEN_CUSTOMER_URL);
    }

		$result = $this->konsumen->delete($id);
    $this->utils->storeDataValidation($result, true);

    redirect(BaseUrl::DEFAULT_KONSUMEN_CUSTOMER_URL);
  }

  // ================================== PERUSAHAAN ==================================

  public function perusahaan()
  {
    $userOptional = $this->utils->getUserProfile();
    $perusahaanList = $this->konsumen->findByKategori(KonsumenKategoriConstant::PERUSAHAAN);
    $data = array(
      'perusahaanList' => $perusahaanList
    );
		$data = $this->utils->response(MenuConstant::KONSUMEN_PERUSAHAAN, $data);

    $this->load->view('konsumen/perusahaan/list', $data);
  }

  public function perusahaanStore()
  {
    $this->utils->getUserProfile();
    $namaPerusahaan = $this->input->post('namaPerusahaan');
    $alamat = $this->input->post('alamat');
    $telepon = $this->input->post('telepon');

    $data = array(
      'kategori' => KonsumenKategoriConstant::PERUSAHAAN,
      'nama_konsumen' => $namaPerusahaan,
      'alamat' => $alamat,
      'telepon' => $telepon,
      'create_at' => DateUtils::getDateNow()
    );

    $store = $this->konsumen->save($data);
    if ($store) {
      $this->session->set_flashdata('success', FlashData::success('Berhasil simpan data'));
    } else {
      $this->session->set_flashdata('failed', FlashData::generalError('Gagal simpan data'));
    }

    redirect(BaseUrl::DEFAULT_KONSUMEN_PERUSAHAAN_URL);
  }

  public function perusahaanupdate()
  {
    $userOptional = $this->utils->getUserProfile();
    $id = $this->input->post('id');
    $namaPerusahaan = $this->input->post('namaPerusahaan');
    $alamat = $this->input->post('alamat');
    $telepon = $this->input->post('telepon');
    $perusahaanOptional = $this->konsumen->findById($id);
    if (empty($perusahaanOptional)) {
      $this->session->set_flashdata('warning', FlashData::warning('Data not found'));
      redirect(BaseUrl::DEFAULT_KONSUMEN_PERUSAHAAN_URL);
    }
		
    $store = $this->konsumen->update($id, $perusahaanOptional[0]->kategori, $namaPerusahaan, $userOptional->username, $alamat, $telepon);
    $this->utils->storeDataValidation($store);

    redirect(BaseUrl::DEFAULT_KONSUMEN_PERUSAHAAN_URL);
  }

  public function perusahaanDelete()
  {
    $this->utils->getUserProfile();
    $id = $this->input->post('id');
    $perusahaanOptional = $this->konsumen->findById($id);
    if (empty($perusahaanOptional)) {
      $this->session->set_flashdata('warning', FlashData::warning('Data not found'));
      redirect(BaseUrl::DEFAULT_KONSUMEN_PERUSAHAAN_URL);
    }

		$result = $this->konsumen->delete($id);
		$this->utils->storeDataValidation($result, true);

    redirect(BaseUrl::DEFAULT_KONSUMEN_PERUSAHAAN_URL);
  }
}
