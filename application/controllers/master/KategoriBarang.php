<?php 

class KategoriBarang extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('KategoriBarangModel','kategori');
    $this->load->model('UserModel','user');
		$this->load->model('Utils', 'utils');
  }

  public function index()
  {
    $userOptional = $this->utils->getUserProfile();
    $kategoriList = $this->kategori->findAll();
    $data = array(
      'kategoriList' => $kategoriList
    );
		$data = $this->utils->response(MenuConstant::KATEGORI_BARANG, $data);

    $this->load->view('kategori_barang/list', $data);
  }

  public function store()
  {
    $this->utils->getUserProfile();
    $namaKategori = $this->input->post('namaKategori');
    $data = array(
      'nama_kategori' => $namaKategori
    );

		$result = $this->kategori->save($data);
    $this->utils->storeDataValidation($result);

    redirect(BaseUrl::DEFAULT_KATEGORI_BARANG_URL);
  }

  public function update()
  {
    $this->utils->getUserProfile();
    $id = $this->input->post('id');
    $namaKategori = $this->input->post('namaKategori');
    $kategoriOptional = $this->kategori->findById($id);
    if (empty($kategoriOptional)) {
      $this->session->set_flashdata('warning', FlashData::warning('Data not found'));
      redirect(BaseUrl::DEFAULT_KATEGORI_BARANG_URL);
    }
		
		$result = $this->kategori->update($id, $namaKategori);
    $this->utils->storeDataValidation($result);

    redirect(BaseUrl::DEFAULT_KATEGORI_BARANG_URL);
  }

  public function delete()
  {
    $this->utils->getUserProfile();
    $id = $this->input->post('id');
    $kategoriOptional = $this->kategori->findById($id);
    if (empty($kategoriOptional)) {
      $this->session->set_flashdata('warning', FlashData::warning('Data not found'));
      redirect(BaseUrl::DEFAULT_KATEGORI_BARANG_URL);
    }

		$result = $this->kategori->delete($id);
    $this->utils->storeDataValidation($result, true);

    redirect(BaseUrl::DEFAULT_KATEGORI_BARANG_URL);
  }
}
