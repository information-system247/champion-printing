<?php 

class User extends CI_Controller
{
  public function __construct() {
    parent::__construct();
    $this->load->model("UserModel", 'user');
    $this->load->model("KaryawanModel", 'karyawan');
    $this->load->model("PerusahaanModel", 'perusahaan');
    $this->load->model('PosisiKaryawanModel', 'posisi');
		$this->load->model('Utils', 'utils');
  }
  
  public function index()
  {
    $userOptional = $this->utils->getUserProfile();
    $userList = $this->user->findAll();
		$karyawanIdList = array($userOptional->karyawan->id);
    $karyawanList = $this->karyawan->findByIdNotIn($karyawanIdList);
    $perusahaanList = $this->perusahaan->findAll();
    $posisiList = $this->posisi->findAll();
    $karyawanList = $this->getKaryawanJoinPerusahaan($karyawanList, $perusahaanList);
    $karyawanList = $this->getKaryawanJoinPosisi($karyawanList, $posisiList);
    $userList = $this->getUserJoinKaryawan($userList, $karyawanList);
		$userList = $this->getUserNotLoginList($userList, $userOptional);

    $data = array(
      "userList" => $userList,
      'karyawanList' => $karyawanList
    );
		$data = $this->utils->response(MenuConstant::USER, $data);

    $this->load->view('user/list', $data);
  }

	public function getUserNotLoginList($userList, $userOptional)
	{
		$result = array();
    if (!empty($userList)) {
      foreach ($userList as $user) {
        if ($userOptional->id != $user->id) {
					$result[] = $user;
				}
      }
    }

		return $this->utils->convertListToJson($result);
	}

  public function getUserJoinKaryawan($userList, $karyawanList)
  {
    $response = array();
    if (!empty($userList)) {
      foreach ($userList as $user) {
        foreach ($karyawanList as $karyawan) {
          if ($user->id_karyawan == $karyawan->id) {
            $data = array(
              'id' => $user->id,
              'karyawan' => $karyawan,
              'username' => $user->username,
              'password' => $user->password
            );
            $response[] = $data;
          }
        }
      }
    }

    return $this->utils->convertListToJson($response);
  }

  public function getKaryawanJoinPerusahaan($karyawanList, $perusahaanList)
  {
    $response = array();
    if (!empty($karyawanList)) {
      foreach ($karyawanList as $karyawan) {
        foreach ($perusahaanList as $perusahaan) {
          if ($karyawan->id_perusahaan == $perusahaan->id) {
            $data = array(
              'id' => $karyawan->id,
              'nama' => $karyawan->nama,
              'alamat' => $karyawan->alamat,
              'perusahaan' => $perusahaan,
              'telepon' => $karyawan->telepon,
              'id_posisi' => $karyawan->id_posisi
            );
            $response[] = $data;
          }
        }
      }
    }

    return $this->utils->convertListToJson($response);
  }

  public function getKaryawanJoinPosisi($karyawanList, $posisiList)
  {
    $response = array();
    if (!empty($karyawanList)) {
      foreach ($karyawanList as $karyawan) {
        foreach ($posisiList as $posisi) {
          if ($karyawan->id_posisi == $posisi->id) {
            $data = array(
              'id' => $karyawan->id,
              'nama' => $karyawan->nama,
              'alamat' => $karyawan->alamat,
              'perusahaan' => $karyawan->perusahaan,
              'telepon' => $karyawan->telepon,
              'posisi' => $posisi
            );
            $response[] = $data;
          }
        }
      }
    }

    return $this->utils->convertListToJson($response);
  }

  public function store()
  {
    $idKaryawan = $this->input->post('idKaryawan');
    $username = $this->input->post('username');
    $password = $this->input->post('password');

		$karyawan = $this->user->findByIdKaryawan($idKaryawan);
		if (!empty($karyawan)) {
			$this->session->set_flashdata('failed', FlashData::generalError('Karyawan sudah memiliki akun'));
      redirect(BaseUrl::DEFAULT_USER_URL);
		}

    $user = $this->user->findByUsername($username);
    if (!empty($user)) {
      $this->session->set_flashdata('failed', FlashData::generalError('Username sudah tersedia'));
      redirect(BaseUrl::DEFAULT_USER_URL);
    }

    $data = array(
      'id_karyawan' => $idKaryawan,
      'username' => $username,
      'password' => $this->encryption->encrypt($password)
    );

    $store = $this->user->save($data);
    $this->utils->storeDataValidation($store);

    redirect(BaseUrl::DEFAULT_USER_URL);
  }

  public function update()
  {
    $id = $this->input->post('id');
    $idKaryawan = $this->input->post('idKaryawan');
    $username = $this->input->post('username');
    $password = $this->input->post('password');

    $userOptional = $this->user->findById($id);
    $user = $this->user->findByUsername($username);
    if (!empty($user) && $userOptional[0]->username != $username) {
      $this->session->set_flashdata('failed', FlashData::generalError('Username sudah digunakan'));
      redirect(BaseUrl::DEFAULT_USER_URL);
    }

    $karyawanOptional = $this->user->findByIdAndKaryawanId($id, $idKaryawan);
    if (!empty($karyawanOptional) && $karyawanOptional[0]->id_karyawan != $idKaryawan) {
      $this->session->set_flashdata('warning', 'Data karyawan sudah memiliki akun');
      redirect(BaseUrl::DEFAULT_USER_URL);
    }

    $data = array(
      "id" => $id,
      "id_karyawan" => $idKaryawan,
      "username" => $username,
      "password" => $password
    );

    $store = $this->user->update($data);
    $this->utils->storeDataValidation($store);
    redirect(BaseUrl::DEFAULT_USER_URL);
  }

  public function delete()
  {
    $id = $this->input->post('id');
		$result = $this->user->delete($id);
    $this->utils->storeDataValidation($result, true);
    redirect(BaseUrl::DEFAULT_USER_URL);
  }
}
