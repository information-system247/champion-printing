<?php 

class Supplier extends CI_Controller
{
  public function __construct() {
    parent::__construct();
    $this->load->model('SupplierModel','supplier');
    $this->load->model('UserModel','user');
    $this->load->model('BarangModel','barang');
		$this->load->model('Utils', 'utils');
  }

  public function index()
  {
    $userOptional = $this->utils->getUserProfile();
    $supplierList = $this->supplier->findAll();
    $data = array(
      'supplierList' => $supplierList
    );
		$data = $this->utils->response(MenuConstant::SUPPLIER, $data);

    $this->load->view('supplier/list', $data);
  }

  public function store()
  {
    $this->utils->getUserProfile();
    $namaSupplier = $this->input->post('namaSupplier');
    $alamat = $this->input->post('alamat');
    $noHp = $this->input->post('noHp');

    $data = array(
      'nama_supplier' => $namaSupplier,
      'alamat' => $alamat,
      'no_hp' => $noHp,
      'create_at' => DateUtils::getDateNow()
    );

    $store = $this->supplier->save($data);
    $this->utils->storeDataValidation($store);

    redirect(BaseUrl::DEFAULT_SUPPLIER_URL);
  }

  public function update()
  {
    $userOptional = $this->utils->getUserProfile();
    $id = $this->input->post('id');
    $namaSupplier = $this->input->post('namaSupplier');
    $alamat = $this->input->post('alamat');
    $noHp = $this->input->post('noHp');

    $supplierOptional = $this->supplier->findById($id);

    $store = $this->supplier->update($id, $namaSupplier, $alamat, $noHp, $userOptional->username);
    $this->utils->storeDataValidation($store);

    redirect(BaseUrl::DEFAULT_SUPPLIER_URL);
  }

  public function delete()
  {
		$result = false;
    $id = $this->input->post('id');
    $barangList = $this->barang->findByIdSupplier($id);
    if (empty($barangList)) {
			$result = $this->supplier->delete($id);
    } else {
      $this->session->set_flashdata('failed', FlashData::generalError('Gagal hapus data, data sudah terelasi dengan tabel lain'));
    }

		$this->utils->storeDataValidation($result, true);
    redirect(BaseUrl::DEFAULT_SUPPLIER_URL);
  }
}
