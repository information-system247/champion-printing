<?php 

class Login extends CI_Controller
{
  public function __construct() {
    parent::__construct();
    $this->load->model('UserModel','user');
		$this->load->model('Utils','utils');
  }

  public function index()
  {
    $username = $this->input->post('username');
    $password = $this->input->post('password');
    $userOptional = $this->user->findByUsername($username);
    if (empty($userOptional)) {
      $this->session->set_flashdata('failed', FlashData::generalError('Username salah'));
      $this->load->view('login');
    } else {
      $passDecrypt = $this->encryption->decrypt($userOptional[0]->password);
      if ($password != $passDecrypt) {
        $this->session->set_flashdata('failed', FlashData::generalError('Password salah'));
        $this->load->view('login');
      } else {
        $dataSession = array(
          'id' => $userOptional[0]->id,
          'id_karyawan' => $userOptional[0]->id_karyawan,
          'username' => $userOptional[0]->username,
          'password' => $userOptional[0]->password,
          'status' => LoginSessionConstant::ACTIVE
        );
        
        $this->session->set_userdata($dataSession);
        $this->session->set_flashdata('success', FlashData::success('Berhasil Masuk'));
				$data = $this->utils->response(MenuConstant::BERANDA);
  
        $this->load->view('index', $data);
      }
    }
  }
}
